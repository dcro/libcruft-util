
#include "endian.hpp"

#include "tap.hpp"
#include "platform.hpp"

#include <cstdlib>
#if defined(PLATFORM_WIN32)
    #include <winsock2.h>
#else
    #include <arpa/inet.h>
    #include <netinet/in.h>
#endif


int
main (void)
{
    cruft::TAP::logger tap;

    uint16_t val16 = 0x1358;
    uint32_t val32 = 0x12345678;

    tap.expect_eq (htons (val16), cruft::hton (val16), "htons");
    tap.expect_eq (htonl (val32), cruft::hton (val32), "htonl");

    tap.expect_eq (ntohs (val16), cruft::hton (val16), "ntohs");
    tap.expect_eq (ntohl (val32), cruft::hton (val32), "ntohl");

    return tap.status ();
}
