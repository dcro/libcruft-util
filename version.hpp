/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_VERSION_HPP
#define __UTIL_VERSION_HPP

#include <array>
#include <string>
#include <iosfwd>
#include <limits>

namespace cruft {
    struct version {
        // underlying component value type
        using value_t = unsigned;

        static_assert (std::numeric_limits<value_t>::max () >= 2100'00'00,
                       "value_t must be sufficient to store a reasonable future year in YYYYMMDD format");

        /// type of release.
        ///
        /// * must be sorted from earliest to latest
        /// * specific values aren't currently guaranteed.
        enum release_t {
            ALPHA,
            BETA,
            GAMMA,
            PRODUCTION
        };

        version ();
        version (unsigned _major);
        version (unsigned _major, unsigned _minor);
        version (unsigned _major, unsigned _minor, unsigned _point);
        version (unsigned _major, unsigned _minor, unsigned _point, unsigned _build);
        explicit version (const std::string& str);
        explicit version (const char *str);

        void sanity (void) const;

        // named component indices
        //
        // * must start from zero and be monotonically increasing
        enum offset_t {
            MAJOR = 0,
            MINOR = 1,
            POINT = 2,
            BUILD = 3
        };

// fuck you too freebsd
#ifdef major
#undef major
#endif
#ifdef minor
#undef minor
#endif

        unsigned major (void) const;
        unsigned minor (void) const;
        unsigned point (void) const;
        unsigned build (void) const;

        const unsigned* begin (void) const noexcept;
        const unsigned* end   (void) const noexcept;
        const unsigned* cbegin (void) const noexcept;
        const unsigned* cend   (void) const noexcept;

        const unsigned& operator[] (size_t) const;
        unsigned& operator[] (size_t);

        size_t size;
        std::array<unsigned,4u> components;
        release_t release = PRODUCTION;

        static version parse (const std::string&);
        static version parse (const char*);

        bool operator<  (const version& rhs) const noexcept;
        bool operator>  (const version& rhs) const noexcept;
        bool operator>= (const version& rhs) const noexcept;
        bool operator<= (const version& rhs) const noexcept;
        bool operator== (const version& rhs) const noexcept;
    };

    std::ostream& operator<< (std::ostream& os, const cruft::version& rhs);
}


#endif // __VERSION_HPP
