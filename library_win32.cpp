/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include "library_win32.hpp"

#include "win32/except.hpp"

#include <winbase.h>

using cruft::detail::win32::library;


///////////////////////////////////////////////////////////////////////////////
library::library (const std::filesystem::path &path):
    m_handle (cruft::win32::error::try_call (LoadLibraryW, path.c_str ()))
{ ; }


//-----------------------------------------------------------------------------
library::library (library &&rhs):
    m_handle (nullptr)
{
    std::swap (m_handle, rhs.m_handle);
}


//-----------------------------------------------------------------------------
library&
library::operator= (cruft::library &&rhs)
{
    std::swap (m_handle, rhs.m_handle);
    return *this;
}


//-----------------------------------------------------------------------------
library::~library ()
{
    if (m_handle)
        FreeLibrary (m_handle);
}
