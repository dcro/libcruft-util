/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2019 Danny Robson <danny@nerdcruft.net>
 */

#include "./assert.hpp"

#include "../backtrace.hpp"
#include "../log.hpp"

#include <iostream>


////////////////////////////////////////////////////////////////////////////////
void
cruft::debug::detail::panic (const char *msg)
{
    std::cerr << "PANIC: " << msg << "\n" << ::cruft::backtrace () << std::endl;
    breakpoint ();
    abort ();
}


////////////////////////////////////////////////////////////////////////////////
void
cruft::debug::detail::not_implemented (const char *msg)
{
    panic (msg);
}


//-----------------------------------------------------------------------------
void
cruft::debug::detail::unreachable (const char *msg)
{
    panic (msg);
}


////////////////////////////////////////////////////////////////////////////////
void
warn (void)
{
    warn ("Unusual code path found.");
}


//-----------------------------------------------------------------------------
void
warn (const std::string &msg)
{
    warn (msg.c_str ());
}


//-----------------------------------------------------------------------------
void
warn (const char *msg)
{
    LOG_WARN (msg);
}

