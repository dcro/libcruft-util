/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2015 Danny Robson <danny@nerdcruft.net>
 */

#include "fnv1a.hpp"

using cruft::hash::fnv1a;


///////////////////////////////////////////////////////////////////////////////
// Prime is,
//   32: 2^ 24 + 2^8 + 0x93
//   64: 2^ 40 + 2^8 + 0xb3
//  128: 2^ 88 + 2^8 + 0x3B
//  256: 2^168 + 2^8 + 0x63
//  512: 2^344 + 2^8 + 0x57
// 1024: 2^680 + 2^8 + 0x8D
//
// Bias is the FNV-0 hash of "chongo <Landon Curt Noll> /\\../\\"

template <typename DigestT>
struct constants { };


//-----------------------------------------------------------------------------
template <>
struct constants<uint32_t> {
    static constexpr uint32_t prime =   16777619u;
    static constexpr uint32_t bias  = 2166136261u;
};


//-----------------------------------------------------------------------------
template <>
struct constants<uint64_t> {
    static constexpr uint64_t prime = 1099511628211u;
    static constexpr uint64_t bias  = 14695981039346656037u;
};


///////////////////////////////////////////////////////////////////////////////
template <typename DigestT>
typename fnv1a<DigestT>::digest_t
fnv1a<DigestT>::operator() (const cruft::view<const uint8_t*> data) const noexcept
{
    auto result = constants<DigestT>::bias;

    for (auto i: data) {
        result ^= i;
        result *= constants<DigestT>::prime;
    }

    return result;
}


template struct cruft::hash::fnv1a<uint32_t>;
template struct cruft::hash::fnv1a<uint64_t>;
