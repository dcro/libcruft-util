/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2012-2018, Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <functional>


namespace cruft::scoped {
    ///////////////////////////////////////////////////////////////////////////
    /// restores a referenced target variable to its original value at
    /// destruction time. the type must be copyable.
    template <typename ValueT>
    class restore {
    public:
        explicit restore (ValueT &_target):
            m_target (_target),
            m_value  (_target)
        { ; }

        ~restore ()
        {
            if (m_restore)
                m_target = m_value;
        }

        /// Disable restoration of the value.
        void release (void) { m_restore = false; }

    private:
        bool m_restore = true;
        ValueT &m_target;
        ValueT const m_value;
    };


    //-------------------------------------------------------------------------
    template <typename ValueT>
    restore (ValueT&) -> restore<ValueT>;


    ///////////////////////////////////////////////////////////////////////////
    /// increments the referenced variable at construction time, and
    /// decrements at destruction time.
    template <typename ValueT>
    class increment {
    public:
        explicit increment (ValueT &_target):
            m_target (_target)
        {
            ++m_target;
        }


        ~increment ()
        {
            --m_target;
        }

    private:
        ValueT &m_target;
    };


    //-------------------------------------------------------------------------
    template <typename ValueT>
    increment (ValueT&) -> increment<ValueT>;


    ///////////////////////////////////////////////////////////////////////////
    /// calls a function with the supplied arguments at destruction time
    template <typename FuncT, typename ...Args>
    class function {
    public:
        explicit function (FuncT _function, Args ..._args):
            m_function (std::forward<FuncT> (_function)),
            m_args (std::forward<Args> (_args)...)
        { ; }

        function (function&&);
        function (const function&);

        ~function ()
        {
            if (m_enabled)
                std::apply (m_function, m_args);
        }

        void release (void) { m_enabled = false; }

    private:
        bool m_enabled = true;
        FuncT m_function;
        std::tuple<Args...> m_args;
    };


    //-------------------------------------------------------------------------
    template <typename FuncT, typename ...Args>
    function (FuncT,Args...) -> function<FuncT,Args...>;
};
