/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2018, Danny Robson <danny@nerdcruft.net>
 */

#include "algo/search.hpp"
#include "tap.hpp"
#include "point.hpp"


int
main (void)
{
    cruft::TAP::logger tap;

    // Check that 'minimises' uses the distance2 metric to correctly find the
    // closest point to the origin.
    {
        cruft::point3f const dst { 0, 0, 0 };

        cruft::point3f const src[] {
            {  9, 9,  9 },
            {  3, 4, 55 },
            {  5, 1,  1 },
            { 13, 4,  6 },
        };


        auto const [best_score, best_iterator] = cruft::search::minimises (
            std::begin (src),
            std::end   (src),
            [] (auto const a, auto const b) { return cruft::distance2 (a, b); },
            dst
        );

        tap.expect_eq (best_iterator, src + 2, "minimises point3 using distance2");
    }


    return tap.status ();
}
