/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */


#include "json/except.hpp"
#include "json/schema.hpp"
#include "json/tree.hpp"

#include "json/constraint/except.hpp"

#include "io.hpp"

#include <filesystem>
#include <iostream>


namespace fs = std::filesystem;


///////////////////////////////////////////////////////////////////////////////
enum {
    ARG_CMD,
    ARG_SCHEMA,
    ARG_INPUT,

    NUM_ARGS
};


///////////////////////////////////////////////////////////////////////////////
int
main (int argc, char **argv) {
    if (argc != NUM_ARGS) {
        std::cerr << argv[ARG_CMD] << " <schema> <json>\n";
        return EXIT_FAILURE;
    }

    try {
        const cruft::mapped_file input_src  (argv[ARG_INPUT]);

        json::schema::validator schema (argv[ARG_SCHEMA]);
        auto input = json::tree::parse (cruft::view{input_src} .cast<const char*>());

        if (!schema.validate (*input)) {
            std::cerr << "fail\n";
            return EXIT_FAILURE;
        }

        std::cout << *input << '\n';
    } catch (cruft::json::schema::error const &x) {
        std::cerr << "schema: " << x.what () << '\n';
        return EXIT_FAILURE;
    } catch (const json::error &e) {
        std::cerr << "error: " << e.what () << '\n';
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
