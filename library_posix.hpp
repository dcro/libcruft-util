/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_LIBRARY_POSIX_HPP
#define __UTIL_LIBRARY_POSIX_HPP

#include <filesystem>

#include <dlfcn.h>

namespace cruft {
    namespace detail {
        class library_posix {
        public:
            explicit library_posix (const std::filesystem::path&);
            library_posix (library_posix&&) noexcept;
            ~library_posix ();

            library_posix (const library_posix&) = delete;
            library_posix& operator= (const library_posix&) = delete;

            template <typename FunctionT>
            FunctionT
            symbol (const char *name)
            {
                return reinterpret_cast<FunctionT> (
                    dlsym (m_handle, name)
                );
            }

        private:
            void *m_handle;

        };
    }

    typedef detail::library_posix library;
}

#endif
