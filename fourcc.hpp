/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_FOURCC_HPP
#define __UTIL_FOURCC_HPP

#include <iosfwd>
#include <cstdint>

namespace cruft {
    struct fourcc {
        uint8_t data[4];

        static fourcc from_string(const char[4]);
        static fourcc from_chars(uint8_t, uint8_t, uint8_t, uint8_t);

        bool operator== (const char[4]) const;
        operator uint32_t (void) const;
    };

    std::ostream& operator<< (std::ostream&, cruft::fourcc);
}


#endif
