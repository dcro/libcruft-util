/******************************************************************************
                              _       _
                             | |     | |
                             | | ___ | |__   ___
                             | |/ _ \| '_ \ / _ \
                             | | (_) | |_) | (_) |
                             |_|\___/|_.__/ \___/
    Copyright:
        Danny Robson, 2020
 *****************************************************************************/

#include "init.hpp"

#include "log.hpp"

#include <atomic>
#include <memory>

using cruft::init::cookie;


///////////////////////////////////////////////////////////////////////////////
namespace {
    // Stores the various RAII handlers for any registered functionality that
    // requires it.
    struct state {
        state ()
            : log (cruft::log::setup_level_reflection ())
        { ; }

        cruft::parse::enumeration::cookie log;
    };

    std::unique_ptr<state> s_state;
    std::atomic<int> s_count;
}


//-----------------------------------------------------------------------------
// Increase the reference count and initialising the requisite functionality if
// this is the first instance of the class.
static void up (void)
{
    if (int old = s_count++; !old)
        s_state = std::make_unique<state> ();
}


//-----------------------------------------------------------------------------
// Decrease the reference count and release any resources if this was the last
// instance of the class.
static void down (void)
{
    if (int old = --s_count; !old)
        s_state.reset ();
}


///////////////////////////////////////////////////////////////////////////////
cookie::cookie () { up (); }
cookie::cookie (cookie &&) noexcept { up (); }
cookie::cookie (cookie const&) { up (); }
cookie& cookie::operator= (cookie &&) noexcept { up (); return *this; }
cookie& cookie::operator= (cookie const&) { up (); return *this; }
cookie::~cookie () { down (); }
