/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2013-2015, Danny Robson <danny@nerdcruft.net>
 */

#include "zlib.hpp"

#include "debug.hpp"

const char *
cruft::zlib::version (void) {
    return zlibVersion ();
}


const char*
cruft::zlib::code_to_string (int code) {
    static const int MIN_CODE = -6;
    static const int MAX_CODE =  2;

    if (code > MAX_CODE || code < MIN_CODE) {
        unreachable ();
    }

    static const char* CODES[] = {
        "OK",
        "STREAM_END",
        "NEED_DICT",
        "ERRNO",
        "STREAM_ERROR",
        "DATA_ERROR",
        "MEM_ERROR",
        "BUF_ERROR",
        "VERSION_ERROR",
    };

    return CODES[code + MIN_CODE];
}


size_t
cruft::zlib::uncompress (uint8_t       *dst, size_t dst_len,
                        const uint8_t *src, size_t src_len) {
    size_t actual_len = dst_len;
    uLongf zlib_actual = actual_len;

    int err = ::uncompress (dst, &zlib_actual, src, src_len);
    error::try_code (err);

    actual_len = zlib_actual;
    return actual_len;
}


void
cruft::zlib::error::try_code (int code) {
    if (Z_OK == code)
        return;

    throw_code(code);
}


cruft::zlib::error::error (const std::string &_what):
    runtime_error (_what)
{ ; }


void
cruft::zlib::error::throw_code (int code) {
    CHECK_NEQ (code, Z_OK);

    switch (code) {
        #define do_code(c) case c: throw code_error<c> ();

        do_code(Z_STREAM_ERROR);
        do_code(Z_DATA_ERROR);
        do_code(Z_MEM_ERROR);
        do_code(Z_BUF_ERROR);
        do_code(Z_VERSION_ERROR);
    
        default:
            panic ("Unknown code");
    }
}


template class cruft::zlib::code_error<Z_STREAM_ERROR>;
template class cruft::zlib::code_error<Z_DATA_ERROR>;
template class cruft::zlib::code_error<Z_MEM_ERROR>;
template class cruft::zlib::code_error<Z_BUF_ERROR>;
template class cruft::zlib::code_error<Z_VERSION_ERROR>;
