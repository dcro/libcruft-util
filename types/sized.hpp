/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011-2020 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../std.hpp"

#include <cstddef>


namespace cruft::types::sized {
    template <size_t BITS>
    struct bits;


    template <>
    struct bits< 8u> {
        using uint = u08;
        using sint = i08;
    };


    template <>
    struct bits<16u> {
        using uint = u16;
        using sint = i16;
    };


    template <>
    struct bits<32u> {
        using uint = u32;
        using sint = i32;
        using real = f32;
    };


    template <>
    struct bits<64u> {
        using uint = u64;
        using sint = i64;
        using real = f64;
    };


    template <std::size_t BytesV>
    struct bytes : public bits<BytesV * 8u>
    { };


    template <typename BaseT>
    struct sized_type : public bits<sizeof(BaseT) * 8u>
    { };
}
