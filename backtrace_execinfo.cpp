/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019, Danny Robson <danny@nerdcruft.net>
 */

#include "backtrace.hpp"

#include "cast.hpp"
#include "iterator/zip.hpp"

#include <ostream>
#include <memory>

#include <execinfo.h>

using cruft::backtrace;

static constexpr std::size_t DEFAULT_DEPTH = 16;


///////////////////////////////////////////////////////////////////////////////
backtrace::backtrace (void)
    : m_frames (DEFAULT_DEPTH)
{
    do {
        int const target = static_cast<int> (m_frames.size ());
        int const last = ::backtrace (
            m_frames.data (), target
        );

        if (last < target) {
            m_frames.resize (last);
            break;
        }

        m_frames.resize (m_frames.size () * 2);
    } while (1);
}


///////////////////////////////////////////////////////////////////////////////
std::ostream&
cruft::operator <<(std::ostream &os, backtrace const &rhs) {
    os << "[ ";

    auto const &frames = rhs.frames ();

    using strings_t = std::unique_ptr<char *, decltype(&std::free)>;
    strings_t const names (
        backtrace_symbols (
            frames.data (),
            cruft::cast::lossless <int> (frames.size ())
        ),
        ::free
    );

    for (auto const [idx,addr]: cruft::iterator::izip (frames)) {
        os << "{ addr: "  << frames[idx]
           << ", name: '" << names.get()[idx] << '\''
           << " }, ";
    }

    os << " ]";
    return os;
}
