#include "tap.hpp"

#include "geom/sphere.hpp"

using cruft::geom::ray2f;
using cruft::geom::ray3f;


///////////////////////////////////////////////////////////////////////////////
void
test_intersect (cruft::TAP::logger &tap)
{
    using cruft::geom::sphere3f;

    static const struct {
        float d;
        sphere3f s;
        ray3f r;
        const char *message;
    } TESTS[] = {
        {
            1.f,
            { cruft::point3f {0.f}, 1.f },
            { cruft::point3f {0, 2, 0}, cruft::vector3f {0,-1,0} },
            "straight towards unit-origin sphere"
        },
        {
            0.f,
            { cruft::point3f {0}, 1.f },
            { cruft::point3f {0,1,0}, cruft::vector3f {0,1,0} },
            "ray origin coincides with unit-origin sphere",
        },
        {
            INFINITY,
            { cruft::point3f {0}, 1 },
            { cruft::point3f {0,2,0}, cruft::vector3f {0,1,0} },
            "offset ray never intersects unit-origin sphere"
        },
        {
            1.f,
            { cruft::point3f {1,1,1}, 1 },
            { cruft::point3f {2,2,1}, cruft::vector3f{-1,0,0} },
            "glancing ray at unit distance"
        }
    };

    for (const auto &[d,s,r,msg]: TESTS) {
        tap.expect_eq (d, distance (r, s), "%!", msg);
    }
}


///////////////////////////////////////////////////////////////////////////////
int
main ()
{
    cruft::TAP::logger tap;
    test_intersect (tap);
    return tap.status ();
}