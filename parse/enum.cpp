/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019, Danny Robson <danny@nerdcruft.net>
 */

#include "enum.hpp"


//-----------------------------------------------------------------------------

namespace cruft::parse::enumeration::detail {
    std::map<int, std::unique_ptr<lookup_base>>&
    cache (void)
    {
        static std::map<int, std::unique_ptr<lookup_base>> s_cache;
        return s_cache;
    }
}
