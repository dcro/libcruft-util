/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018-2020, Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <cstdint>

/// A collection of convenience typedefs that define short aliases (three
/// letters) to std sized types in the global namespace.
///
/// We guarantee that there will be minimal, and safe, includes in this header.


///////////////////////////////////////////////////////////////////////////////
using u08 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;


//-----------------------------------------------------------------------------
static_assert (sizeof (u08) == 1);
static_assert (sizeof (u16) == 2);
static_assert (sizeof (u32) == 4);
static_assert (sizeof (u64) == 8);


///////////////////////////////////////////////////////////////////////////////
using i08 = std::int8_t;
using i16 = std::int16_t;
using i32 = std::int32_t;
using i64 = std::int64_t;

//-----------------------------------------------------------------------------
static_assert (sizeof (i08) == 1);
static_assert (sizeof (i16) == 2);
static_assert (sizeof (i32) == 4);
static_assert (sizeof (i64) == 8);


///////////////////////////////////////////////////////////////////////////////
using f32 = float;
using f64 = double;

//-----------------------------------------------------------------------------
static_assert (sizeof (f32) == 4);
static_assert (sizeof (f64) == 8);
