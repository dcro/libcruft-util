/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2012-2016 Danny Robson <danny@nerdcruft.net>
 */

#include "io.hpp"
#include "json/except.hpp"
#include "json/tree.hpp"

#include <cstdlib>
#include <iostream>


///////////////////////////////////////////////////////////////////////////////
enum {
    ARG_COMMAND,
    ARG_INPUT,

    NUM_ARGS
};


//-----------------------------------------------------------------------------
void
print_usage (int argc, char **argv)
{
    (void)argc;
    std::cerr << "usage: " << argv[0] << " <input>\n";
}


//-----------------------------------------------------------------------------
int
main (int argc, char **argv)
{
    if (argc != NUM_ARGS) {
        print_usage (argc, argv);
        return EXIT_FAILURE;
    }

    try {
        const cruft::mapped_file src (argv[ARG_INPUT]);
        std::cout << *json::tree::parse (cruft::view{src}.cast<const char*> ()) << '\n';
    } catch (const json::error& err) {
        std::cerr << err.what () << "\n";
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
