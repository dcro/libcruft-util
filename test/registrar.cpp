#include "registrar.hpp"

#include "tap.hpp"


void foo () { }

int main ()
{
    cruft::TAP::logger tap;

    using void_fptr = void(*)();
    using registry_type = cruft::registry<char const*, void_fptr>;

    {
        auto cookie = registry_type::add ("foo", foo);
        tap.expect_eq (registry_type::at ("foo"), foo, "queried value matches registered value");
    }

    tap.expect_throw<std::out_of_range> (
        [] () { registry_type::at ("foo"); },
        "destruction of cookie removes value"
    );

    return tap.status ();
}
