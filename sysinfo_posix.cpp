/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#include "sysinfo.hpp"

#include "posix/except.hpp"

#include <ostream>

#include <pwd.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <unistd.h>

using cruft::util::sysinfo;


///////////////////////////////////////////////////////////////////////////////
std::ostream&
cruft::util::operator<< (std::ostream &os, sysinfo const &)
{
    os << "{ ";

    {
        struct utsname name;
        cruft::posix::error::try_code (uname (&name));

        os << "os: { name: '" << name.sysname << "', version: '" << name.version << "' }"
           << ", hostname: '" << name.nodename << '\'';
    }

    {
        auto const uid = geteuid ();
        auto const user = getpwuid (uid);
        os << ", username: '" << user->pw_name << '\'';
    }

    {
        auto const size  = posix::error::try_call (sysconf, _SC_PAGESIZE);
        auto const total = posix::error::try_call (sysconf, _SC_PHYS_PAGES);
        auto const now   = posix::error::try_call (sysconf, _SC_AVPHYS_PAGES);

        os << ", ram: { total: " << size * total << ", available: " << size * now << " },";
    }

    return os << " }";
}
