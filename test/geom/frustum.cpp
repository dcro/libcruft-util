#include "tap.hpp"

#include "geom/frustum.hpp"
#include "geom/aabb.hpp"
#include "matrix.hpp"

#include "geom/iostream.hpp"


///////////////////////////////////////////////////////////////////////////////
int
main ()
{
    cruft::TAP::logger tap;

    const cruft::geom::frustum3f origin90 (
        cruft::perspective (cruft::to_radians (90.f), 1.f, {0.01f, 1.f})
    );

    static const struct {
        cruft::geom::aabb3f box;
        bool intersects;
        const char *message;
    } TESTS[] = {
        { { { -1, -1, -1 }, {  1,  1,  1 } }, true,  "covers origin with unit radius" },
        { { { -9, -9, -9 }, { -8, -8, -8 } }, false, "out of view negative" },
        { { { -1, -1,  2 }, {  1,  1,  3 } }, false, "past far plane" },
        { { {  0,  0,  0 }, {  0,  0,  0 } }, false, "before near plane" },
    };

    for (const auto &t: TESTS) {
        tap.expect_eq (intersects (origin90, t.box), t.intersects, "origin frustrum, %s", t.message);
    }

    return tap.status ();
};