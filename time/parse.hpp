/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2017 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_UTIL_TIME_PARSE_HPP
#define CRUFT_UTIL_TIME_PARSE_HPP

#include "../view.hpp"

#include <chrono>
#include <ratio>

namespace cruft::time::iso8601 {
    /// parse ISO8601 formatted datetime strings
    ///
    /// returns nanoseconds since the UNIX epoch (excluding leap seconds).
    ///
    /// recognises fractional seconds up to the numeric limits of
    /// std::chrono::nanoseconds.
    ///
    /// may throw on improperly formatted strings or unrepresentable values.
    std::chrono::nanoseconds
    parse (cruft::view<const char*>);
}

#endif
