/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <utility>
#include <type_traits>
#include <cstddef>
#include <cstdint>


namespace cruft::ptr {
    /// A utiliity function that deletes the provided pointer.
    ///
    /// Useful as a means of moving `delete` calls inside a translation unit
    /// and hence removing the need to include implementation headers for a
    /// type that needs deletion.
    template <typename ValueT>
    void destroy (ValueT*);


    /// A smart pointer that defers deletion to `destroy`.
    ///
    /// This is solely useful as a way of avoid undefined type errors when
    /// using std::unique_ptr and forward declarations.
    template <typename ValueT>
    class thin {
    public:
        explicit thin (ValueT *_value) { m_value = _value; }
        ~thin () { destroy (m_value); }

        thin (thin const&) = delete;
        thin& operator= (thin const&) = delete;

        thin (thin &&rhs)
           : m_value (rhs.m_value)
       { rhs.m_value = nullptr; }

        thin& operator= (thin &&rhs) noexcept
        {
            std::swap (m_value, rhs.m_value);
        }

        ValueT      & operator* (void)       &{ return *m_value; }
        ValueT const& operator* (void) const &{ return *m_value; }
        ValueT      * operator->(void)       &{ return  m_value; }
        ValueT const* operator->(void) const &{ return  m_value; }

    private:
        ValueT *m_value;
    };


    template <typename ValueT, typename ...ArgsT>
    thin<ValueT>
    make_thin (ArgsT &&...args)
    {
        return thin (new ValueT (std::forward<ArgsT> (args)...));
    }
}


namespace cruft::align {
    ///////////////////////////////////////////////////////////////////////////
    /// round the pointer upwards to satisfy the provided alignment
    constexpr inline uintptr_t
    up (uintptr_t ptr, size_t alignment)
    {
        // we perform this as two steps to avoid unnecessarily incrementing when
        // remainder is zero.
        if (auto mod = ptr % alignment; mod)
            ptr += alignment - mod;
        return ptr;
    }


    ///////////////////////////////////////////////////////////////////////////
    /// round the pointer upwards to satisfy the provided alignment
    template <typename T>
    constexpr T*
    up (T *ptr, size_t alignment)
    {
        // we perform this as two steps to avoid unnecessarily incrementing when
        // remainder is zero.
        return reinterpret_cast<T*>(
            up (reinterpret_cast<uintptr_t> (ptr), alignment)
        );
    }


    //-------------------------------------------------------------------------
    template <typename T>
    constexpr T*
    up (T *ptr, std::align_val_t _alignment)
    {
        auto alignment = std::underlying_type_t<decltype(_alignment)> (_alignment);
        return up (ptr, alignment);
    }


    ///------------------------------------------------------------------------
    /// round the pointer upwards to the nearest valid alignment for T
    template <typename T>
    constexpr auto
    up (T *t)
    {
        return up (t, alignof (T));
    }


    ///------------------------------------------------------------------------
    /// round the pointer upwards to the nearest valid alignment for T
    template <typename T>
    constexpr auto
    up (uintptr_t ptr)
    {
        return up (ptr, alignof (T));
    }


    ///////////////////////////////////////////////////////////////////////////
    constexpr inline uintptr_t
    down (uintptr_t ptr, size_t alignment)
    {
        return ptr - ptr % alignment;
    }


    //-------------------------------------------------------------------------
    template <typename T>
    constexpr T*
    down (T *ptr, size_t alignment)
    {
        return reinterpret_cast<T*> (
            down (reinterpret_cast<uintptr_t> (ptr), alignment)
        );
    }
}
