/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include "linear.hpp"

#include "traits.hpp"

#include "../pointer.hpp"
#include "../debug/assert.hpp"

using cruft::alloc::linear;


///////////////////////////////////////////////////////////////////////////////
linear::linear (cruft::view<u08*> _data):
    m_begin  (_data.begin ()),
    m_end    (_data.end ()),
    m_cursor (_data.begin ())
{ ; }


///////////////////////////////////////////////////////////////////////////////
linear::linear (linear &&rhs) noexcept
    : m_begin  (std::exchange (rhs.m_begin,  nullptr))
    , m_end    (std::exchange (rhs.m_end,    nullptr))
    , m_cursor (std::exchange (rhs.m_cursor, nullptr))
{ ; }


//-----------------------------------------------------------------------------
linear&
linear::operator= (linear &&rhs) noexcept
{
    m_begin  = std::exchange (rhs.m_begin,  nullptr);
    m_end    = std::exchange (rhs.m_end,    nullptr);
    m_cursor = std::exchange (rhs.m_cursor, nullptr);

    return *this;
}


///////////////////////////////////////////////////////////////////////////////
u08*
linear::data (void)
{
    return m_begin;
}


//-----------------------------------------------------------------------------
const u08*
linear::data (void) const
{
    return m_begin;
}


//-----------------------------------------------------------------------------
u08*
linear::begin (void)
{
    return m_begin;
}


//-----------------------------------------------------------------------------
const u08*
linear::begin (void) const
{
    return m_begin;
}


//-----------------------------------------------------------------------------
u08*
linear::end (void)
{
    return m_end;
}


//-----------------------------------------------------------------------------
const u08*
linear::end (void) const
{
    return m_end;
}


//-----------------------------------------------------------------------------
size_t
linear::offset (const void *_ptr) const
{
    auto ptr = reinterpret_cast<const u08*> (_ptr);

    CHECK_GE (ptr, m_begin);
    return ptr - m_begin;
}


///////////////////////////////////////////////////////////////////////////////
void
linear::reset (void)
{
    m_cursor = m_begin;
}


///////////////////////////////////////////////////////////////////////////////
size_t
linear::capacity (void) const
{
    return m_end - m_begin;
}


//-----------------------------------------------------------------------------
size_t
linear::used (void) const
{
    return m_cursor - m_begin;
}


//-----------------------------------------------------------------------------
size_t
linear::remain (void) const
{
    return capacity () - used ();
}


///////////////////////////////////////////////////////////////////////////////
static_assert (cruft::alloc::is_allocator_v<linear>);
