/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include "murmur1.hpp"

#include "common.hpp"
#include "../../endian.hpp"

using cruft::hash::murmur1;


///////////////////////////////////////////////////////////////////////////////
uint32_t
murmur1::operator() (cruft::view<const uint8_t*> data) const noexcept
{
    static const uint32_t m = 0xc6a4a793;
    uint32_t h = m_seed ^ ((data.size () & 0xffffffff) * m);

    // mix the body
    auto cursor = data.begin ();
    for (auto last = data.end () - data.size ()  %  sizeof (uint32_t); cursor < last; cursor += 4)
        h = mix (h, cruft::readhe<uint32_t> (cursor));

    // mix the tail
    if (data.size () % sizeof (uint32_t))
        h = mix (h, murmur::tail<uint32_t> (cursor, data.size ()));

    // finalise
    h *= m; h ^= h >> 10;
    h *= m; h ^= h >> 17;

    return h;
}

