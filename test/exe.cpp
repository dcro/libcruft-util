#include "../tap.hpp"

#include "../exe.hpp"

#include <filesystem>


///////////////////////////////////////////////////////////////////////////////
int
main (int, char **argv)
{
    cruft::TAP::logger tap;

    // we'd like to compare based on the total path, but systems like msys2
    // have varying styles of paths and it's difficult to harmonize the
    // expected and queried values.
    //
    // so we compare the filename instead under the assumption that it should
    // be stable across path styles.
    auto const query = cruft::image_path ();
    auto const truth = std::filesystem::path (argv[0]);
    tap.expect_eq (
        query.stem (),
        truth.stem (),
        "identify executable path # %!", query
    );

    return tap.status ();
}
