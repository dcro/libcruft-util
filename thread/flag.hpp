#include "../platform.hpp"

#if defined(PLATFORM_LINUX)
#include "flag_futex.hpp"
#else
#include "flag_std.hpp"
#endif
