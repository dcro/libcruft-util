/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019, Danny Robson <danny@nerdcruft.net>
 */

#include "crash.hpp"

#include "assert.hpp"

#include "../backtrace.hpp"
#include "../win32/windows.hpp"

#include <iostream>


///////////////////////////////////////////////////////////////////////////////
static LPTOP_LEVEL_EXCEPTION_FILTER previous_handler;


///////////////////////////////////////////////////////////////////////////////
static LONG __stdcall
backtrace_handler (EXCEPTION_POINTERS *data)
{
    std::clog << cruft::backtrace () << '\n';

    if (previous_handler)
        return previous_handler (data);
    else
        return EXCEPTION_CONTINUE_SEARCH;
}


///////////////////////////////////////////////////////////////////////////////
void
cruft::debug::init_crash_handler (void)
{
    (void)backtrace_handler;
#if 0
    CHECK (!previous_handler);
    previous_handler = SetUnhandledExceptionFilter (backtrace_handler);
#endif
}
