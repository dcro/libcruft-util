/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../view.hpp"

#include <cstddef>

namespace cruft::alloc {
    /// a raw memory allocator which initialises an instance of PrefixT
    /// immediately before each allocation and, if specified, an instance
    /// of SuffixT after the allocation.
    ///
    /// uses an instance of ParentT to actually perform the bulk allocations.
    ///
    /// useful for sentinels, reference counts, etc.
    template <class ParentT, class PrefixT, class SuffixT>
    class affix {

        template <typename T>
        cruft::view<T*> allocate (size_t bytes);

        template <typename T>
        cruft::view<T*> allocate (size_t bytes, size_t align);

        template <typename T>
        void deallocate (cruft::view<std::byte*>);

        std::byte* begin (void);
        const std::byte* begin (void) const;

        std::byte* end (void);
        const std::byte* end (void) const;

        size_t offset (const void*) const;
    };
}
