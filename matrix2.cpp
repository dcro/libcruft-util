/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include "matrix.hpp"

using cruft::matrix;


///////////////////////////////////////////////////////////////////////////////
template <std::size_t Rows, std::size_t Cols, typename T>
T
cruft::determinant (const matrix<Rows,Cols,T> &m)
{
    static_assert (Rows == 2 && Cols == 2);

    return m[0][0] * m[1][1] - m[0][1] * m[1][0];
}

template float  cruft::determinant (const matrix<2,2,float>&);
template double cruft::determinant (const matrix<2,2,double>&);


///////////////////////////////////////////////////////////////////////////////
template <std::size_t Rows, std::size_t Cols, typename T>
matrix<Rows,Cols,T>
cruft::inverse (const matrix<Rows,Cols,T> &m)
{
    static_assert (Rows == 2 && Cols == 2);

    return matrix<2,2,T> {{
        {  m[1][1], -m[0][1], },
        { -m[1][0],  m[0][0], },
    }} / determinant (m);
}


//-----------------------------------------------------------------------------
template cruft::matrix<2,2,float>  cruft::inverse (const matrix<2,2,float>&);
template cruft::matrix<2,2,double> cruft::inverse (const matrix<2,2,double>&);


///////////////////////////////////////////////////////////////////////////////
template struct cruft::matrix<2,2,float>;
template struct cruft::matrix<2,2,double>;
