/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "platform.hpp"

#include <iosfwd>


namespace cruft::cpu {
    struct base {
        struct {
            int logical;
            int physical;
        } cores;
    };


    std::ostream&
    operator<< (std::ostream&, const cruft::cpu::base&);
};

#if defined(PROCESSOR_AMD64)
#include "cpuid/x86.hpp"
#else
#error "Unsupported CPU"
#endif


namespace cruft::cpu {
#if defined(PROCESSOR_AMD64)
    using native = x86;
#else
    #error "Unsupported CPU"
#endif
}
