#include "flag.hpp"

#include "../win32/except.hpp"

using cruft::thread::flag;


///////////////////////////////////////////////////////////////////////////////
flag::flag ():
    fired (false)
{ ; }


///////////////////////////////////////////////////////////////////////////////
void
flag::wait (void)
{
    while (!fired) {
        std::unique_lock lk (m_mutex);
        m_condition.wait (lk, [this] () { return !!fired; });
    }
}

///////////////////////////////////////////////////////////////////////////////
void
flag::notify_one (void)
{
    std::unique_lock lk (m_mutex);
    fired = true;
    m_condition.notify_one ();
}


//-----------------------------------------------------------------------------
void
flag::notify_all (void)
{
    std::unique_lock lk (m_mutex);
    fired = true;
    m_condition.notify_all ();
}
