/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_WIN32_REGISTRY_HPP
#define __UTIL_WIN32_REGISTRY_HPP

#include "windows.hpp"

#include <winreg.h>

#include "../view.hpp"

#include <set>
#include <string>

namespace cruft::win32 {
    class key {
    public:
        key (key const &root, const char *child, REGSAM rights = KEY_READ);
        key (HKEY root, const char *child, REGSAM rights = KEY_READ);
        ~key ();

        class child_iterator {
        public:
            using value_type = key;
            using difference_type = std::ptrdiff_t;
            using pointer = value_type*;
            using reference = value_type&;
            using iterator_category = std::input_iterator_tag;

            child_iterator (key const &_parent);
            child_iterator (key const &_parent, int _index);

            key operator* (void) const;
            child_iterator& operator++ ();
            bool operator== (child_iterator const&);
            bool operator!= (child_iterator const&);

        private:
            key const &m_parent;
            int m_index;
        };

        cruft::view<child_iterator> subkeys (void);

        std::string name (void) const;

        template <typename T>
        T data (const char *name = nullptr) const;

        std::set<std::string>
        values (void) const;

    private:
        HKEY m_handle;
    };
}

#endif
