#include "backtrace.hpp"
#include "tap.hpp"
#include "stream.hpp"

#include <cstdlib>


///////////////////////////////////////////////////////////////////////////////
int
main (int, char **) {
    cruft::TAP::logger tap;

    // Instantiate a backtrace just so we know that linking has worked.
    ::cruft::backtrace foo;
    (void)foo;

    tap.noop ();

    return tap.status ();
}
