/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2012-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "log.hpp"

#include <string>
#include <cstdint>

namespace cruft::log {
    ///////////////////////////////////////////////////////////////////////////
    class scoped_logger {
    public:
        scoped_logger (level_t, std::string);
        ~scoped_logger ();

        scoped_logger (scoped_logger const&) = delete;
        scoped_logger& operator= (scoped_logger const&) = delete;

        scoped_logger (scoped_logger &&) = delete;
        scoped_logger& operator= (scoped_logger &&) = delete;

    protected:
        const level_t     m_level;
        const std::string m_message;
    };


    ///////////////////////////////////////////////////////////////////////////
    class scoped_timer {
    public:
        scoped_timer (level_t, std::string);
        ~scoped_timer ();

        scoped_timer (scoped_timer const&) = delete;
        scoped_timer& operator= (scoped_timer const&) = delete;

        scoped_timer (scoped_timer &&) = delete;
        scoped_timer& operator= (scoped_timer &&) = delete;

    private:
        const level_t m_level;
        const std::string m_message;
        uint64_t m_start;
    };
}
