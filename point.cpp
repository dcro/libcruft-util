/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011-2018 Danny Robson <danny@nerdcruft.net>
 */

#include "point.hpp"

#include "debug/assert.hpp"
#include "debug/validate.hpp"
#include "std.hpp"

#include <cstdlib>

using cruft::point;


///////////////////////////////////////////////////////////////////////////////
namespace cruft::debug  {
    template <size_t S, typename T>
    struct validator<point<S,T>> {
        static bool is_valid (const point<S,T> &p)
        {
            // ensure we don't have a nan anywhere
            return std::all_of (p.cbegin (), p.cend (), [] (auto i) {
                return !(std::is_floating_point<T>::value && std::isnan (i));
            });
        }
    };
}


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
std::pair<
    cruft::point<S,T>,
    cruft::point<S,T>
>
cruft::furthest (cruft::view<const cruft::point<S,T>*> src)
{
    CHECK_GE (src.size (), 2u);

    size_t a = 0;
    size_t b = 1;
    auto d = distance2 (src[a],src[b]);

    for (size_t i = 0; i < src.size (); ++i) {
        for (size_t j = i + 1; j < src.size (); ++j) {
            if (auto d2 = distance2 (src[i], src[j]); d2 > d) {
                a = i;
                b = j;
                d = d2;
            }
        }
    }

    return { src[a], src[b] };
}


///////////////////////////////////////////////////////////////////////////////
#define INSTANTIATE_S_T(S,T)                                    \
template struct cruft::point<(S),T>;                            \
template bool cruft::debug::is_valid (const point<(S),T>&);     \
template std::pair<                                             \
    cruft::point<(S),T>,                                        \
    cruft::point<(S),T>                                         \
> cruft::furthest (cruft::view<const cruft::point<(S),T>*>);    \
template struct cruft::debug::validator<point<(S),T>>;


//-----------------------------------------------------------------------------
MAP_CRUFT_COORD_PARAMS(INSTANTIATE_S_T)
