/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2012-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "level.hpp"
#include "packet.hpp"
#include "sink/base.hpp"
#include "../format.hpp"

#include <memory>
#include <string>


namespace cruft::log {
    ///////////////////////////////////////////////////////////////////////////
    sink::base& default_sink (void);
    sink::base& default_sink (std::unique_ptr<sink::base>);


    //-------------------------------------------------------------------------
    void write (level_t, std::string const &msg);
    void write (level_t, char const *msg);
    void write (level_t, std::string_view msg);


    //-------------------------------------------------------------------------
    template <typename FormatT, typename ...ArgsT>
    void
    write (level_t l, FormatT fmt, ArgsT &&...args)
    {
        default_sink ().write (
            packet (
                l,
                std::forward<FormatT> (fmt),
                std::forward<ArgsT> (args)...
            )
        );
    }


    //-------------------------------------------------------------------------
    // Various convenience macros for logging specific strings with a well
    // known severity.
    //
    // LOG_DEBUG is treated similarly to assert; if NDEBUG is defined then we
    // compile out the statement so as to gain a little runtime efficiency
    // speed.
    #define LOG_EMERGENCY(...)  do { cruft::log::write (cruft::log::EMERGENCY, ##__VA_ARGS__); } while (0)
    #define LOG_ALERT(...)      do { cruft::log::write (cruft::log::ALERT,     ##__VA_ARGS__); } while (0)
    #define LOG_CRITICAL(...)   do { cruft::log::write (cruft::log::CRITICAL,  ##__VA_ARGS__); } while (0)
    #define LOG_ERROR(...)      do { cruft::log::write (cruft::log::ERROR,     ##__VA_ARGS__); } while (0)
    #define LOG_WARNING(...)    do { cruft::log::write (cruft::log::WARNING,   ##__VA_ARGS__); } while (0)
    #define LOG_WARN(...)       do { cruft::log::write (cruft::log::WARN,      ##__VA_ARGS__); } while (0)
    #define LOG_NOTICE(...)     do { cruft::log::write (cruft::log::NOTICE,    ##__VA_ARGS__); } while (0)
    #define LOG_INFO(...)       do { cruft::log::write (cruft::log::INFO,      ##__VA_ARGS__); } while (0)
#if !defined(NDEBUG)
    #define LOG_DEBUG(...)      do { cruft::log::write (cruft::log::DEBUG,     ##__VA_ARGS__); } while (0)
#else
    #define LOG_DEBUG(...)      do { ; } while (0)
#endif
}
