#include "event_std.hpp"

using cruft::thread::event;


///////////////////////////////////////////////////////////////////////////////
event::event ():
    m_value (0)
{ ; }


///////////////////////////////////////////////////////////////////////////////
void
event::wait (void)
{
    int const now = m_value;
    std::unique_lock lk (m_mutex);
    m_cv.wait (lk, [&,this] () { return now != m_value; });
}


///////////////////////////////////////////////////////////////////////////////
void
event::notify_one (void)
{
    ++m_value;
    m_cv.notify_one ();
}


//-----------------------------------------------------------------------------
void event::notify_all()
{
    ++m_value;
    m_cv.notify_all ();
}
