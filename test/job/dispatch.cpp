/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#include "coord/comparator.hpp"
#include "job/dispatch.hpp"
#include "tap.hpp"

#include <vector>


///////////////////////////////////////////////////////////////////////////////
struct container {
    cruft::point2i
    operator[] (cruft::point2i p)&
    { return values.emplace_back () = p; }

    bool is_unique (void)
    {
        cruft::coord::ordering<cruft::point2i> comp {};
        std::sort (values.begin (), values.end (), comp);
        auto const pos = std::adjacent_find (values.begin (), values.end ());
        return pos == values.end ();
    }

    std::size_t size (void) const { return values.size (); }

    std::vector<cruft::point2i> values;
};


//-----------------------------------------------------------------------------
void test_dispatch_uniqueness (cruft::TAP::logger &tap)
{
    container store;

    cruft::region2i const work_region {
        cruft::point2i { 0 },
        cruft::extent2i { 512 }
    };

    cruft::job::queue q (1, 512  * 512 + 1);
    cruft::job::dispatch (
        q, store,
        work_region,
        cruft::extent2i {2},
        [] (auto &&arg) { return arg; }
    );

    tap.expect (
        store.is_unique (),
        "job::queue indices are unique"
    );

    tap.expect_eq (
        store.size (),
        std::size_t (work_region.magnitude ().area ()),
        "job::queue evaluated expected times"
    );
}


///////////////////////////////////////////////////////////////////////////////
int main ()
{
    cruft::TAP::logger tap;
    test_dispatch_uniqueness (tap);
    return tap.status ();
}
