/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include "system.hpp"

#include "../cast.hpp"
#include "../posix/except.hpp"

#include <unistd.h>

///////////////////////////////////////////////////////////////////////////////
size_t
cruft::memory::pagesize (void)
{
    static size_t val;

    if (!val) {
        val = cruft::cast::sign<unsigned long> (posix::error::try_value (sysconf (_SC_PAGE_SIZE)));
    }

    return val;
}
