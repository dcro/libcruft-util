/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2015 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_HASH_FNV1A_HPP
#define __UTIL_HASH_FNV1A_HPP

#include "view.hpp"

#include <cstdint>
#include <cstddef>

namespace cruft::hash {
    // Fast and general hashing using FNV-1a
    template <typename DigestT>
    struct fnv1a {
        using digest_t = DigestT;

        digest_t operator() (cruft::view<const uint8_t*>) const noexcept;
    };
}

#endif
