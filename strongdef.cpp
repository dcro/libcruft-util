/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include "strongdef.hpp"


///////////////////////////////////////////////////////////////////////////////
// This instantiation is not meant to be exported, only being used as a local
// compilation error canary.

template class cruft::strongdef::index<void,unsigned>;
