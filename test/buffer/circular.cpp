/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include "std.hpp"
#include "buffer/circular.hpp"
#include "tap.hpp"

#include <algorithm>

///////////////////////////////////////////////////////////////////////////////
int
main (void)
{
    cruft::TAP::logger tap;

    // provoke usage of the smallest size buffer we can get away with so we
    // might detect caching issues or similar.
    constexpr size_t CAPACITY = 1;
    cruft::buffer::circular<u08> data (CAPACITY);

    // zero fill to ensure our value setting tests don't accidentall succeed
    std::fill_n (data.begin (), data.size () * 2, 0);

    // sanity check we haven't accidentally mapped an empty region
    tap.expect_neq (data.begin (), data.end (), "non-zero sized region");

    // check a near overrun is replicated
    data.end ()[0] = 1;
    tap.expect_eq (data.begin ()[0], data.end ()[0], "near overrun is replicated");

    // check a far overrun is replicated
    data.end ()[data.size () - 1] = 2;
    tap.expect_eq (data.begin ()[data.size () - 1],
                   data.end   ()[data.size () - 1],
                   "far overrun is replicated");

    return tap.status ();
}
