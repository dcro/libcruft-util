/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2016 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_IO_POSIX_HPP
#define __UTIL_IO_POSIX_HPP

#include "posix/fd.hpp"

#include "view.hpp"

#include <type_traits>
#include <filesystem>

#include <sys/mman.h>
#include <fcntl.h>

namespace cruft {
    namespace detail::posix {
        // represents a memory mapped file.
        //
        // the data pointer may be null in the case of a zero length file. it
        // is expected that the user will take measures to avoid dereferencing
        // such a pointer.
        class mapped_file {
        public:
            using value_type = uint8_t;
            using reference = value_type&;
            using const_reference = const value_type&;
            using iterator = value_type*;
            using const_iterator = const value_type*;
            using difference_type = std::iterator_traits<iterator>::difference_type;
            using size_type = size_t;

            mapped_file (const std::filesystem::path&,
                         int fflags = O_RDONLY | O_BINARY,
                         int mflags = PROT_READ);
            mapped_file (const cruft::posix::fd&,
                         int mflags = PROT_READ);

            mapped_file (const mapped_file&) = delete;
            mapped_file& operator= (const mapped_file&) = delete;

            mapped_file (mapped_file&&) noexcept;
            mapped_file& operator= (mapped_file&&) noexcept;

            ~mapped_file ();

            bool empty (void) const;

            /// returns the total allocated mapped region in bytes.
            ///
            /// result is typed size_t (rather than the native signed type)
            /// because we often use this in conjunction with sizeof and
            /// packed structure.
            ///
            /// it is greatly simpler to cast to signed where it's actually
            /// required rather than the other way around.
            size_type size (void) const;

            const_iterator data (void) const &;
            iterator       data (void) &;

            iterator begin (void) &;
            iterator end   (void) &;

            const_iterator begin (void) const &;
            const_iterator end   (void) const &;

            const_iterator cbegin (void) const &;
            const_iterator cend   (void) const &;

        private:
            uint8_t *m_data;
            size_t   m_size;
        };
    }

    typedef detail::posix::mapped_file mapped_file;
}

#endif
