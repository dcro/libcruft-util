/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#include "sysinfo.hpp"

#include "win32/windows.hpp"
#include "win32/except.hpp"

#include <ostream>


///////////////////////////////////////////////////////////////////////////////
std::ostream&
cruft::util::operator<< (std::ostream &os, sysinfo const &)
{
    os << "{ ";

    {
        OSVERSIONINFO version {};
        version.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
        if (!GetVersionExA (&version))
            win32::error::throw_code ();

        os << "os: { name: win32"
               << ", version: " << +version.dwMajorVersion << '.'
                                << +version.dwMinorVersion << '.'
                                << +version.dwPlatformId
               << "}";
    }

    DWORD size = 0;
    std::string name;

    {
        if (0 == GetComputerNameExA (ComputerNamePhysicalNetBIOS, nullptr, &size)) {
            auto const code = win32::error::last_code ();
            if (code != ERROR_MORE_DATA)
                win32::error::throw_code (code);
        }
        name.resize (size);

        if (0 == GetComputerNameExA (ComputerNamePhysicalNetBIOS, &name[0], &size))
            win32::error::throw_code ();
        name.resize (size);

        os << ", hostname: " << name;
    }

    {
        size = 0;
        if (0 == GetUserNameA (nullptr, &size)) {
            auto const code = win32::error::last_code ();
            if (code != ERROR_INSUFFICIENT_BUFFER)
                win32::error::throw_code (code);

        }
        name.resize (size);

        if (0 == GetUserNameA (&name[0], &size))
            win32::error::throw_code ();
        // Remove the provided trailing null terminator
        name.resize (size - 1);

        os << ", username: " << name;
    }

    {
        MEMORYSTATUSEX status {};
        status.dwLength = sizeof (status);

        if (0 == GlobalMemoryStatusEx (&status))
            win32::error::throw_code ();

        os << ", ram: { total: " << status.ullTotalPhys << ", available: " << status.ullAvailPhys << " },";
    };

    return os << " }";
}
