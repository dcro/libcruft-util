#include "tap.hpp"

#include "tuple/value.hpp"


int
main (void)
{
    cruft::TAP::logger tap;

    {
        auto tuple = std::make_tuple (1,2,3,4);
        std::vector<int> expected {{ 1, 2, 3, 4 }};

        std::vector<int> actual;
        cruft::tuple::value::each ([&actual] (auto i) { actual.push_back (i); }, tuple);

        tap.expect_eq (actual, expected, "value iteration");
    }

    {
        std::tuple a (1, 2);
        std::array<char,2> b { 'a', 'b' };
        std::tuple c (
            std::tuple(1, 'a'),
            std::tuple(2, 'b')
        );

        tap.expect_eq (c, cruft::tuple::value::zip (a, b), "tuple zipping");
    }

    {
        std::tuple a (1, 2);
        auto result = cruft::tuple::value::reverse (a);
        std::tuple expected (2, 1);

        tap.expect_eq (result, expected, "tuple reverse");
    }

    tap.expect (
        cruft::tuple::value::all (std::tuple<bool,int> { true, 3 }),
        "tuple::all succeeds"
    );

    tap.expect (
        !cruft::tuple::value::all (std::tuple<bool,int> (true, 0)),
        "tuple::all fails"
    );

    tap.expect (
        cruft::tuple::value::none (std::tuple<bool,int> (false, 0)),
        "tuple::none succeeds"
    );

    tap.expect (
        !cruft::tuple::value::none (std::tuple<bool,int> (false, 1)),
        "tuple::none fails"
    );

    return tap.status ();
}