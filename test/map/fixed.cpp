#include "tap.hpp"

#include "map/fixed.hpp"

int main ()
{
    cruft::TAP::logger tap;

    cruft::map::fixed<4, int,std::string> map;

    auto const [pos, success] = map.insert ({ 0, "zero" });
    tap.expect (success, "map insertion reports success");

    tap.expect_eq (pos->second, "zero", "inserted value is equal");

    map.insert ({ 1, "one" });
    map.insert ({ 2, "two" });
    map.insert ({ 3, "three" });

    tap.expect_throw<std::bad_alloc> (
        [&] () { map.insert ({ 4, "four" }); },
        "over-allocation throws bad_alloc"
    );

    return tap.status ();
}