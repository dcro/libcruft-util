/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011-2016 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __FLOAT_HPP
#define __FLOAT_HPP

#include "types/sized.hpp"


///////////////////////////////////////////////////////////////////////////////
namespace cruft {
    template <unsigned int EXPONENT, unsigned int SIGNIFICAND>
    class ieee_float
    {
    public:
        static const unsigned int EXPONENT_BITS    = EXPONENT;
        static const unsigned int SIGNIFICAND_BITS = SIGNIFICAND;
        static const unsigned int TOTAL_BITS       = 1 + EXPONENT + SIGNIFICAND;

        static const unsigned int BIAS = (1 << (EXPONENT - 1)) - 1;

        using sint_t = typename types::sized::bits<TOTAL_BITS>::sint;
        using uint_t = typename types::sized::bits<TOTAL_BITS>::uint;
        using real_t = typename types::sized::bits<TOTAL_BITS>::real;

    protected:
        union {
            uint_t m_bits;
            real_t m_floating;

            struct {
                uint_t sign        : 1;
                uint_t exponent    : EXPONENT;
                uint_t significand : SIGNIFICAND;
            } m_components;
        };

    public:
        ieee_float (void);
        ieee_float (real_t  _floating);
        ieee_float (const ieee_float &rhs);

        static unsigned int bias (void)
            { return BIAS; }

        void   set_bits (uint_t _bits) { m_bits = _bits; }
        uint_t bits     (void) const   { return m_bits;  }

        bool negative (void) const { return m_components.sign; }
        void negate   (void)       { m_components.sign ^= m_components.sign; }

        bool is_zero      (void) const;
        bool is_subnormal (void) const;
        bool is_inifinity (void) const;
        bool is_nan       (void) const;
        bool operator==   (real_t) const;

        static bool almost_equal (real_t, real_t);
        static bool almost_equal (real_t, real_t, unsigned ulps);
    };


    //typedef ieee_float< 5,  10> ieee_half;
    typedef ieee_float< 8,  23> ieee_single;
    typedef ieee_float<11,  52> ieee_double;

    //extern template class ieee_float< 5,10>;
    extern template class ieee_float< 8,23>;
    extern template class ieee_float<11,52>;

    //static_assert (sizeof(ieee_half   ) == 2, "ieee_half must be 2 bytes");
    static_assert (sizeof(ieee_single ) == 4, "ieee_single must be 4 bytes");
    static_assert (sizeof(ieee_double ) == 8, "ieee_double must be 8 bytes");
}
#endif // __FLOAT_HPP
