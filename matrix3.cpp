/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include "matrix.hpp"

using cruft::matrix;


///////////////////////////////////////////////////////////////////////////////
template <std::size_t Rows, std::size_t Cols, typename T>
T
cruft::determinant (const matrix<Rows,Cols,T>& m)
{
    static_assert (Rows == 3 && Cols == 3);

    return m[0][0] * (m[1][1] * m[2][2] - m[2][1] * m[1][2]) -
           m[0][1] * (m[1][0] * m[2][2] - m[1][2] * m[2][0]) +
           m[0][2] * (m[1][0] * m[2][1] - m[1][1] * m[2][0]);
}

template float  cruft::determinant (const matrix<3,3,float>&);
template double cruft::determinant (const matrix<3,3,double>&);


//-----------------------------------------------------------------------------
template <std::size_t Rows, std::size_t Cols, typename T>
matrix<Rows,Cols,T>
cruft::inverse (const matrix<Rows,Cols,T> &m)
{
    static_assert (Rows == 3 && Cols == 3);

    return matrix<3,3,T> {{
        { m[1][1] * m[2][2] - m[2][1] * m[1][2],
          m[0][2] * m[2][1] - m[0][1] * m[2][2],
          m[0][1] * m[1][2] - m[0][2] * m[1][1], },

        { m[1][2] * m[2][0] - m[1][0] * m[2][2],
          m[0][0] * m[2][2] - m[0][2] * m[2][0],
          m[1][0] * m[0][2] - m[0][0] * m[1][2], },

        { m[1][0] * m[2][1] - m[2][0] * m[1][1],
          m[2][0] * m[0][1] - m[0][0] * m[2][1],
          m[0][0] * m[1][1] - m[1][0] * m[0][1], },
    }} / determinant (m);
}

template cruft::matrix<3,3,float>  cruft::inverse (const matrix<3,3,float>&);
template cruft::matrix<3,3,double> cruft::inverse (const matrix<3,3,double>&);


///////////////////////////////////////////////////////////////////////////////
template struct cruft::matrix<3,3,float>;
template struct cruft::matrix<3,3,double>;
