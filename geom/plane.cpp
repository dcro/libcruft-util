/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */


#include "plane.hpp"

using cruft::geom::plane;


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
plane<S,T>::plane (point<S,T> base, vector<S,T> normal)
{
    CHECK (is_normalised (normal));

    std::copy (std::begin (normal), std::end (normal), std::begin (coefficients));
    coefficients[S] = -dot (base, normal);
}


///////////////////////////////////////////////////////////////////////////////
template struct cruft::geom::plane<2,float>;
template struct cruft::geom::plane<3,float>;
