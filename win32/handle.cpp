#include "handle.hpp"

#include "except.hpp"

using cruft::win32::handle;


///////////////////////////////////////////////////////////////////////////////
handle::handle ():
    m_native (INVALID_HANDLE_VALUE)
{ ; }


//-----------------------------------------------------------------------------
handle::handle (HANDLE &&h):
    m_native (h)
{ ; }


//-----------------------------------------------------------------------------
handle::handle (posix::fd &&rhs):
    m_native (reinterpret_cast<HANDLE> (_get_osfhandle (rhs.release ())))
{
    win32::error::try_value (m_native);
}


//-----------------------------------------------------------------------------
handle::handle (cruft::win32::handle &&rhs) noexcept
    : handle ()
{
    std::swap (m_native, rhs.m_native);
}


//-----------------------------------------------------------------------------
handle& handle::operator= (handle &&rhs) noexcept
{
    std::swap (m_native, rhs.m_native);
    return *this;
}


//-----------------------------------------------------------------------------
handle::~handle ()
{
    if (m_native != INVALID_HANDLE_VALUE)
        CloseHandle (m_native);
}


///////////////////////////////////////////////////////////////////////////////
handle::operator HANDLE& (void) &
{
    return native ();
}


//-----------------------------------------------------------------------------
HANDLE&
handle::native (void) &
{
    return m_native;
}


//-----------------------------------------------------------------------------
const HANDLE&
handle::native (void) const &
{
    return m_native;
}


///////////////////////////////////////////////////////////////////////////////
void
handle::reset (HANDLE rhs)
{
    if (m_native != INVALID_HANDLE_VALUE)
        CloseHandle (m_native);
    m_native = rhs;
}


///////////////////////////////////////////////////////////////////////////////
handle
handle::current_process (void)
{
    return handle (GetCurrentProcess ());
}
