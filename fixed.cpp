/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011-2015 Danny Robson <danny@nerdcruft.net>
 */

#include "fixed.hpp"

#include "maths.hpp"

#include <ostream>

using namespace cruft;


///////////////////////////////////////////////////////////////////////////////
template <typename T, unsigned I, unsigned E>
double
fixed<T,I,E>::to_double (void) const
{
    return static_cast<double> (m_value) / pow (2, E);
}


//-----------------------------------------------------------------------------
template <typename T, unsigned I, unsigned E>
float
fixed<T,I,E>::to_float (void) const
{
    return static_cast<float> (m_value) / pow (2, E);
}


//-----------------------------------------------------------------------------
template <typename T, unsigned I, unsigned E>
typename fixed<T,I,E>::integer_type
fixed<T,I,E>::to_integer (void) const
{
    return m_value >> E;
}


//-----------------------------------------------------------------------------
template <typename T, unsigned I, unsigned E>
typename fixed<T,I,E>::native_type
fixed<T,I,E>::to_native (void) const
{
    return m_value;
}


//-----------------------------------------------------------------------------
template <typename T, unsigned I, unsigned E>
fixed<T,I,E>
fixed<T,I,E>::from_native (native_type i)
{
    fixed<T,I,E> v;
    v.m_value = i;
    return v;
}


//-----------------------------------------------------------------------------
template <typename T, unsigned I, unsigned E>
fixed<T,I,E>
fixed<T,I,E>::from_integer (integer_type val)
{
    constexpr auto rshift = sizeof (val) * 8 - I;
    constexpr auto lshift = E - rshift;

    fixed<T,I,E> res;
    res.m_value = static_cast<native_type> (val) << lshift;
    return res;
}


//-----------------------------------------------------------------------------
template <typename T, unsigned I, unsigned E>
typename fixed<T,I,E>::integer_type
fixed<T,I,E>::to_integer (native_type n)
{
    return n >> E;
}


///////////////////////////////////////////////////////////////////////////////
// Fixed operators
#define SIMPLE_FIXED_REF(OP)                                \
template <typename T, unsigned I, unsigned E>               \
cruft::fixed<T,I,E>&                                         \
cruft::fixed<T,I,E>::operator OP (const fixed<T,I,E> rhs)    \
{                                                           \
    m_value OP rhs.m_value;                                 \
    return *this;                                           \
}

SIMPLE_FIXED_REF(-=)
SIMPLE_FIXED_REF(+=)


#define SIMPLE_FIXED_LIT(OP)                                    \
template <typename T, unsigned I, unsigned E>                   \
cruft::fixed<T,I,E>                                              \
cruft::fixed<T,I,E>::operator OP (const fixed<T,I,E> rhs) const  \
{                                                               \
    fixed<T,I,E> v;                                             \
    v.m_value = m_value OP rhs.m_value;                         \
    return v;                                                   \
}

SIMPLE_FIXED_LIT(-)
SIMPLE_FIXED_LIT(+)


///////////////////////////////////////////////////////////////////////////////
// Integer operators
#define SIMPLE_INTEGER_REF(OP)                        \
template <typename T, unsigned I, unsigned E>         \
fixed<T,I,E>&                                         \
fixed<T,I,E>::operator OP (integer_type val)          \
{                                                     \
    m_value OP (static_cast<native_type> (val) << E); \
    return *this;                                     \
}

SIMPLE_INTEGER_REF(+=)
SIMPLE_INTEGER_REF(-=)
SIMPLE_INTEGER_REF(*=)
SIMPLE_INTEGER_REF(/=)


//-----------------------------------------------------------------------------
#define SIMPLE_INTEGER_LIT(OP)                              \
template <typename T, unsigned I, unsigned E>               \
fixed<T,I,E>                                                \
fixed<T,I,E>::operator OP (integer_type val) const          \
{                                                           \
    return fixed<T,I,E>::from_native (m_value OP (native_type{val} << E)); \
}

SIMPLE_INTEGER_LIT(+)
SIMPLE_INTEGER_LIT(-)
SIMPLE_INTEGER_LIT(*)
SIMPLE_INTEGER_LIT(/)


///////////////////////////////////////////////////////////////////////////////
// logical operators

#define LOGIC_OP(OP)                            \
template <typename T, unsigned I, unsigned E>   \
bool                                            \
cruft::operator OP (cruft::fixed<T,I,E> a,        \
                   cruft::fixed<T,I,E> b)        \
{                                               \
    return a.to_native () OP b.to_native ();    \
}                                                               \
                                                                \
template <typename T, unsigned I, unsigned E>                   \
bool                                                            \
cruft::operator OP (cruft::fixed<T,I,E> a,                        \
                   typename cruft::fixed<T,I,E>::integer_type b) \
{                                                               \
    return a OP cruft::fixed<T,I,E>::from_integer (b);           \
}

LOGIC_OP(==)
LOGIC_OP(!=)
LOGIC_OP(<)
LOGIC_OP(<=)
LOGIC_OP(>)
LOGIC_OP(>=)


///////////////////////////////////////////////////////////////////////////////
// iostream operators
template <typename T, unsigned I, unsigned E>
std::ostream&
cruft::operator<< (std::ostream &os, fixed<T,I,E> v)
{
    return os << v.to_double ();
}


///////////////////////////////////////////////////////////////////////////////
// Instantiations

#define INSTANTIATE(T,I,E)                                                  \
template class cruft::fixed<T,I,E>;                                          \
template std::ostream& cruft::operator<< (std::ostream&, fixed<T,I,E>);      \
template bool cruft::operator== (cruft::fixed<T,I,E>, cruft::fixed<T,I,E>);    \
template bool cruft::operator!= (cruft::fixed<T,I,E>, cruft::fixed<T,I,E>);    \
template bool cruft::operator<  (cruft::fixed<T,I,E>, cruft::fixed<T,I,E>);    \
template bool cruft::operator<= (cruft::fixed<T,I,E>, cruft::fixed<T,I,E>);    \
template bool cruft::operator>  (cruft::fixed<T,I,E>, cruft::fixed<T,I,E>);    \
template bool cruft::operator>= (cruft::fixed<T,I,E>, cruft::fixed<T,I,E>);    \
template bool cruft::operator== (cruft::fixed<T,I,E>, typename cruft::fixed<T,I,E>::integer_type); \
template bool cruft::operator!= (cruft::fixed<T,I,E>, typename cruft::fixed<T,I,E>::integer_type); \
template bool cruft::operator<  (cruft::fixed<T,I,E>, typename cruft::fixed<T,I,E>::integer_type); \
template bool cruft::operator<= (cruft::fixed<T,I,E>, typename cruft::fixed<T,I,E>::integer_type); \
template bool cruft::operator>  (cruft::fixed<T,I,E>, typename cruft::fixed<T,I,E>::integer_type); \
template bool cruft::operator>= (cruft::fixed<T,I,E>, typename cruft::fixed<T,I,E>::integer_type);

template class cruft::fixed<signed,8,8>;

INSTANTIATE(signed, 2,14)
INSTANTIATE(signed,16,16)
INSTANTIATE(signed,26, 6)
INSTANTIATE(signed,32,32)
INSTANTIATE(unsigned,16,16)
INSTANTIATE(unsigned,26, 6)
INSTANTIATE(unsigned,32,32)
