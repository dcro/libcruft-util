#pragma once

#include "../fwd.hpp"

#include <string>


namespace cruft::log::sink {
    class base {
    public:
        virtual ~base () = default;

        base (std::string name);

        std::string const& name (void);

        virtual void write (packet const&) = 0;

    private:
        std::string m_name;
    };


    template <typename DerivedT>
    class crtp : public base {
    public:
        template <typename ...ArgsT>
        crtp (ArgsT &&...args)
            : base (std::forward<ArgsT> (args)...)
        { ; }
    };
}