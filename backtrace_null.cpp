/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2011-2012, Danny Robson <danny@nerdcruft.net>
 */

#include "backtrace.hpp"

#include <ostream>


///////////////////////////////////////////////////////////////////////////////
debug::backtrace::backtrace (void):
    m_frames (DEFAULT_DEPTH)
{ ; }


///////////////////////////////////////////////////////////////////////////////
std::ostream&
debug::operator <<(std::ostream &os, const debug::backtrace&)
{
    return os << "null_backtrace";
}


