/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2012-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "fwd.hpp"

#include "ops.hpp"
#include "init.hpp"
#include "traits.hpp"
#include "../maths.hpp"

#include <algorithm>
#include <array>
#include <cstdlib>
#include <type_traits>


namespace cruft::coord {
    /////////////////////////////////////////////////////////////////////////
    // the base class for all coordinate-like types.
    //
    // SelfT should not be exposed as a template template directly because
    // some types (eg, XYZ colours) do not conform to the same template
    // parameters are others (eg, vector2f). ie, it does not make sense to
    // allow redim, or type changing on some types so they just aren't exposed.
    template <
        std::size_t S,
        typename T,
        typename SelfT
    >
    struct base : public init<S,T,SelfT> {
        static_assert (S > 0);
        static_assert (std::is_arithmetic<T>::value);
        static_assert (sizeof (init<S,T,SelfT>) == S * sizeof (T));

        using self_t = SelfT;
        using value_type = T;
        static constexpr std::size_t dimension = S;
        static constexpr std::size_t elements = S;

        /// returns the number of elements we contain
        static constexpr auto size (void) { return S; }

        // constructors
        using init<S,T,SelfT>::init;

        /// constructs, but does not initialise, the data.
        ///
        /// used to avoid unnecessary initialisation in many situations where
        /// we have arrays of these types that are about to be overwritten. it
        /// is a very important performance optimisation.
        base () = default;

        /// constructs an instance where all elements are initialised to `val'.
        constexpr
        base (T fill)
        {
            for (decltype(S) i = 0; i < S; ++i)
                this->data[i] = fill;
        }

        constexpr base (const base<S,T,SelfT> &rhs) = default;
        base& operator= (const base<S,T,SelfT> &rhs)& = default;
        base& operator= (const T t)&
        {
            for (auto &v: *this)
                v = t;
            return *this;
        }

        // element accessors
        constexpr T& operator[] (size_t i)& noexcept { return this->data[i]; }
        constexpr T& operator[] (int i)& noexcept { return this->data[i]; }

        constexpr const T& operator[] (size_t i) const& noexcept { return this->data[i]; }
        constexpr const T& operator[] (int i) const& noexcept { return this->data[i]; }

        auto cbegin (void) const { return std::cbegin (this->data); }
        auto cend   (void) const { return std::cend   (this->data); }

        auto begin (void) const { return std::begin (this->data); }
        auto end   (void) const { return std::end   (this->data); }

        auto begin (void) { return std::begin (this->data); }
        auto end   (void) { return std::end   (this->data); }

        const T& front (void) const { return this->data[0]; }
        T& front (void) { return this->data[0]; }

        const T& back (void) const { return this->data[S-1]; }
        T& back (void) { return this->data[S-1]; }


        ///////////////////////////////////////////////////////////////////////
        // conversions
        template <template <std::size_t, typename> class K>
        K<S,T> as (void) const
        {
            K<S,T> k;
            std::copy (begin (), end (), k.begin ());
            return k;
        }


        //---------------------------------------------------------------------
        template <
            typename K,
            typename = std::enable_if_t<is_coord_v<K>,void>
        >
        K as (void) const
        {
            static_assert (K::elements == elements);
            K k;
            std::copy (begin (), end (), k.begin ());
            return k;
        }


        //---------------------------------
        template <typename U>
        auto
        cast (void) const
        {
            typename revalue_type<SelfT>::template type<U> out;

            std::copy (std::cbegin (this->data),
                       std::cend   (this->data),
                       std::begin  (out.data));
            return out;
        }


        ///////////////////////////////////////////////////////////////////////
        operator std::array<value_type,elements> () const
        {
            std::array<value_type,elements> res;
            std::copy (begin (), end (), std::begin (res));
            return res;
        }


        ///////////////////////////////////////////////////////////////////////
        /// returns an instance with the same data, but truncated to `D'
        /// elements
        ///
        /// explicitly does not allow a fill parameter given it can't be used
        /// when reducing dimensions.
        ///
        /// HACK,gcc#,gcc-7.x: This function _must not_ use std::copy or
        /// similar algorithms to move the data as it produces an ICE on some
        /// code fragements if it does. Namely:
        ///   "internal compiler error: in trunc_int_for_mode, at explow.c:55"
        template <
            size_t D,
            typename = std::enable_if_t<
                has_redim_v<SelfT> && S >= D,
                void
            >
        >
        auto
        redim (void) const
        {
            redim_t<SelfT,D> out;
            for (size_t i = 0; i < D; ++i)
                out[i] = this->data[i];
            return out;
        }


        //---------------------------------------------------------------------
        /// returns an instance with the same data, but more elements, where
        /// the new elements are initialised with values with the same index
        /// in the coordinate `fill'.
        ///
        /// explicitly requires a fill parameter so that we avoid undefined
        /// values.
        ///
        /// HACK,gcc#,gcc-7.x: This function _must not_ use std::copy or
        /// similar algorithms to move the data as it produces an ICE on some
        /// code fragements if it does. Namely:
        ///   "internal compiler error: in trunc_int_for_mode, at explow.c:55"
        template<size_t D,typename _sfinae = SelfT>
        std::enable_if_t<
            has_redim_v<_sfinae> && S <= D,
            redim_t<_sfinae,D>
        >
        redim (const redim_t<_sfinae,D> fill) const
        {
            redim_t<SelfT,D> out;

            for (size_t i = 0; i < S; ++i)
                out[i] = this->data[i];
            for (size_t i = S; i < D; ++i)
                out[i] = fill[i];
            return out;
        }

        //---------------------------------------------------------------------
        /// returns an instance with the same data, but more elements, where
        /// all the new elemenst are initialised with the scalar `fill'.
        ///
        /// explicitly requires a fill parameter so that we avoid undefined
        /// values.
        ///
        /// HACK,gcc#,gcc-7.x: This function _must not_ use std::copy or
        /// similar algorithms to move the data as it produces an ICE on some
        /// code fragements if it does. Namely:
        ///   "internal compiler error: in trunc_int_for_mode, at explow.c:55"
        template <
            size_t D,
            typename _sfinae = SelfT
        >
        std::enable_if_t<
            has_redim_v<_sfinae> && S <= D,
            redim_t<_sfinae,D>
        >
        redim (T fill) const
        {
            redim_t<SelfT,D> out;
            for (size_t i = 0; i < S; ++i)
                out[i] = this->data[i];
            for (size_t i = S; i < D; ++i)
                out[i] = fill;
            return out;
        }


        ///////////////////////////////////////////////////////////////////////
        /// Returns an instance with elements specified by the Indices
        /// parameter. eg, point2f p{}.indices<0,2> would return {p.x, p.z}.
        ///
        /// Given we don't take any arguments all indices must be valid for
        /// the current object.
        template <
            std::size_t ...Indices,
            typename = std::enable_if_t<cruft::max (Indices...) < S>
        >
        constexpr auto
        indices (void) const
        {
            return redim_t<SelfT,sizeof...(Indices)> {
                this->data[Indices]...
            };
        }


        /// Return an instance of the same type where the elements of the new
        /// value area specified by the Indices... template parameter.
        ///
        /// If the index is greater than the maximum index of this type then
        /// the value is taken from a pack of supplementary values.
        ///
        /// eg, vector2f{0,1}.indices<0,2,1> (2) would result in {0,2,1}
        template <
            std::size_t ...Indices,
            typename ...U,
            typename = std::enable_if_t<
                // At least one index doesn't address our current data
                cruft::max (Indices...) >= S &&
                // The new data type must be the old data type
                (std::is_same_v<T,U> && ...)
            >
        >
        constexpr auto
        indices (U const &...supplementary) const
        {
            static_assert (
                cruft::max (Indices...) < S + sizeof...(supplementary),
                "indices must fall within the defined range for the type"
            );

            // Expand the pack using either:
            //  * data from ourselves,
            //  * or rebasing the index into a tuple of the supplementary values
            //
            // The index to std::get _must_ be valid even if we don't use the
            // result (otherwise we tend to encounter static assertions). Thus
            // we conditionally specify a zero index if we know it won't get
            // used.
            return redim_t<SelfT,sizeof...(Indices)> {
                  Indices < S
                ? this->data[Indices]
                : std::get<
                    (Indices > S) ? Indices - S : 0
                > (std::tuple (supplementary...))...
            };
        }
    };
}


/// Invoke a macro across all typical coordinate parameter combinations.
///
/// ie, each of the types i16, i32, i64, f32, and f64; and arities of 1, 2, 3,
/// and 4.
///
/// This is useful for exhaustive delayed instantiation in .cpp files.

#define MAP_CRUFT_COORD_PARAMS(F)   \
F(1,i16) F(2,i16) F(3,i16) F(4,i16) \
F(1,i32) F(2,i32) F(3,i32) F(4,i32) \
F(1,i64) F(2,i64) F(3,i64) F(4,i64) \
F(1,u16) F(2,u16) F(3,u16) F(4,u16) \
F(1,u32) F(2,u32) F(3,u32) F(4,u32) \
F(1,u64) F(2,u64) F(3,u64) F(4,u64) \
F(1,f32) F(2,f32) F(3,f32) F(4,f32) \
F(1,f64) F(2,f64) F(3,f64) F(4,f64)
