/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2016 Danny Robson <danny@nerdcruft.net>
 */

#include "time.hpp"

#include "cast.hpp"

#include <ctime>

static constexpr uint64_t SECOND = 1'000'000'000UL;

///////////////////////////////////////////////////////////////////////////////
void
cruft::sleep (uint64_t ns)
{
    struct timespec req, rem;

    req.tv_sec  = cruft::cast::lossless<time_t> (ns / SECOND);
    req.tv_nsec = cruft::cast::lossless<long  > (ns % SECOND);

    while (nanosleep (&req, &rem)) {
        req = rem;
    }
}
