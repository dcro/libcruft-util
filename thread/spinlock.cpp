/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018-2019 Danny Robson <danny@nerdcruft.net>
 */

#include "spinlock.hpp"

#include "primitive.hpp"

#include "../debug/assert.hpp"

using cruft::thread::spinlock;


///////////////////////////////////////////////////////////////////////////////
spinlock::spinlock ():
    held (false)
{ ; }


//-----------------------------------------------------------------------------
spinlock::~spinlock ()
{
    CHECK (!held);
}


//-----------------------------------------------------------------------------
spinlock::spinlock (spinlock &&rhs) noexcept:
    held (rhs.held.load ())
{
    rhs.held = false;
}


//-----------------------------------------------------------------------------
spinlock& spinlock::operator= (spinlock &&rhs) noexcept
{
    CHECK (!held);

    held = rhs.held.load ();
    rhs.held = false;
    return *this;
}


///////////////////////////////////////////////////////////////////////////////
void
spinlock::lock (void)
{
    do {
        bool expected = false;
        if (held.compare_exchange_weak (expected, true))
            return;

        while (held)
            pause ();
    } while (true);
}


//-----------------------------------------------------------------------------
void
spinlock::unlock (void)
{
    CHECK (held);
    held = false;
}
