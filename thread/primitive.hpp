/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../platform.hpp"


namespace cruft {
    inline void pause [[gnu::always_inline]] (void)
    {
        #ifdef PROCESSOR_AMD64
        asm volatile ("pause");
        #elif PROCESSOR_ARM
        asm volatile ("yield");
        #else
        #error "Unhandled processor type"
        #endif
    }
}
