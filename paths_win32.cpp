/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2020, Danny Robson <danny@nerdcruft.net>
 */

#include "paths.hpp"

#include "win32/windows.hpp"
#include "win32/except.hpp"

#include <stdexcept>


///////////////////////////////////////////////////////////////////////////////
std::filesystem::path
cruft::paths::expand (std::filesystem::path const &val)
{
    auto const required_size = ExpandEnvironmentStringsW (val.native ().data (), nullptr, 0) + 1;

    std::wstring store (required_size, '\0');

    auto const actual_size = ExpandEnvironmentStringsW (
        val.native ().data (),
        store.data (),
        store.size ()
    );

    if (actual_size == 0)
        win32::error::throw_code ();
    if (actual_size > required_size)
        throw std::runtime_error ("Unable to expand path");
    store.resize (actual_size);

    return store;
}