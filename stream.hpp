/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011-2016 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_STREAM_HPP
#define __UTIL_STREAM_HPP

#include <iosfwd>
#include <cstdint>
#include <ostream>


namespace cruft::stream {
    namespace scoped {
        #define SCOPED(NAME,TYPE)           \
        class NAME {                        \
        public:                             \
            explicit NAME (std::ios_base&); \
            ~NAME ();                       \
                                            \
        private:                            \
            std::ios_base &m_ios;           \
            TYPE m_state;                   \
        }

        SCOPED(flags, std::ios_base::fmtflags);
        SCOPED(precision, std::streamsize);
        SCOPED(width, std::streamsize);

        #undef SCOPED
    };


    ///////////////////////////////////////////////////////////////////////////
    class null_streambuf : public std::basic_streambuf<char> {
    public:
        virtual ~null_streambuf () { ; }
    };


    ///////////////////////////////////////////////////////////////////////////
    class null_ostream : public std::basic_ostream<char> {
    public:
        virtual ~null_ostream () { ; }

        std::ostream & put   (char c);
        std::ostream & write (const char *s, std::streamsize n);

        std::streampos tellp (void);
        std::ostream & seekp (std::streampos pos);
        std::ostream & seekp (std::streamoff off,
                              std::ios_base::seekdir dir);

        std::ostream & flush (void);

        bool good (void) const;
        bool bad  (void) const;
        bool eof  (void) const;
        bool fail (void) const;
    };


    ///////////////////////////////////////////////////////////////////////////
    struct bits {
        bits (uintmax_t value, unsigned count);

        uintmax_t value;
        unsigned  count;
    };


    //---------------------------------------------------------------------
    std::ostream& operator<< (std::ostream&, bits);
}


#endif
