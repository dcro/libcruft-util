/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2019 Danny Robson <danny@nerdcruft.net>
 */

#include "./validate.hpp"

#include "preprocessor.hpp"

#include <cmath>


///////////////////////////////////////////////////////////////////////////////
template <>
bool
cruft::debug::validator<float>::is_valid (const float &val) noexcept
{
    return !std::isnan (val);
}


//-----------------------------------------------------------------------------
template <>
bool
cruft::debug::validator<double>::is_valid (const double &val) noexcept
{
    return !std::isnan (val);
}

///////////////////////////////////////////////////////////////////////////////
#include "std.hpp"


//-----------------------------------------------------------------------------
#define INSTANTIATE(KLASS)                              \
template <>                                             \
struct cruft::debug::validator<KLASS> {                  \
    static bool is_valid(KLASS const&);                 \
};                                                      \
                                                        \
bool                                                    \
cruft::debug::validator<KLASS>::is_valid(KLASS const&)   \
{                                                       \
    return true;                                        \
}


//-----------------------------------------------------------------------------
MAP0(INSTANTIATE,
    u08, u16, u32, u64,
    i08, i16, i32, i64,
char
)
