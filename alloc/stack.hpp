/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../view.hpp"
#include "../pointer.hpp"

#include <cstddef>


namespace cruft::alloc {
    // allocate memory from a buffer in a stacklike manner. deallocation that
    // is not correctly ordered has undefined (read 'bad') results.
    class stack {
    public:
        stack (const stack&) = delete;
        stack (stack&&) = delete;
        stack& operator= (const stack&) = delete;
        stack& operator= (stack&&) = delete;

        stack (cruft::view<std::byte*> _data);

        template <typename T>
        cruft::view<T*>
        allocate (size_t count, size_t alignment)
        {
            constexpr auto MIN_ALIGNMENT = sizeof (record::offset_t);
            const auto bytes = count * sizeof (T);

            // reserve space at the front of the allocation to record the total
            // allocation size so we can account for alignment if required.
            auto ptr = m_cursor + sizeof (record::offset_t);

            // align the outgoing pointer if required
            alignment = cruft::max (MIN_ALIGNMENT, alignment);
            ptr = cruft::align::up (ptr, alignment);

            // ensure we haven't overrun our allocated segment
            if (ptr + bytes > m_end)
                throw std::bad_alloc ();

            // use a 'record' struct as a window into the reserved space at the front
            // of the allocation and store the offset to the previous allocation head
            // (from the record struct). allows us to account for alignment.
            record r;
            r.as_bytes  = ptr - sizeof (record::offset_t);
            *r.as_offset = cruft::cast::lossless <uint32_t> (ptr - m_cursor);

            m_cursor = ptr + bytes;
            return { cruft::cast::alignment<T*> (ptr), count };
        }


        template <typename T>
        cruft::view<T*>
        allocate (size_t count)
        {
            return allocate<T> (count, alignof (T));
        }


        template <typename T>
        void deallocate (cruft::view<T*> _ptr)
        {
            auto ptr = reinterpret_cast<std::byte*> (_ptr.data ());

            record r;
            r.as_bytes = ptr - sizeof (record::offset_t);

            //CHECK_LE (bytes, *record.as_offset);
            CHECK_GE (m_cursor - *r.as_offset, m_begin);

            m_cursor -= sizeof (T) * _ptr.size ();
            m_cursor -= *r.as_offset;
        }


        cruft::view<std::byte*> data (void);
        cruft::view<const std::byte*> data (void) const;

        std::byte* begin (void);
        const std::byte* begin (void) const;

        std::byte* end (void);
        const std::byte* end (void) const;

        size_t offset (const void*) const;

        void reset (void);

        size_t capacity (void) const;
        size_t used     (void) const;
        size_t remain   (void) const;

    private:
        union record {
            using offset_t = uint32_t;

            std::byte *as_bytes;
            offset_t  *as_offset;
        };

        std::byte *const m_begin;
        std::byte *const m_end;
        std::byte *m_cursor;
    };
}
