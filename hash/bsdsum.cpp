/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2018 Danny Robson <danny@nerdcruft.net>
 */

#include "bsdsum.hpp"

#include "../bitwise.hpp"

using cruft::hash::bsdsum;


///////////////////////////////////////////////////////////////////////////////
typename bsdsum::digest_t
bsdsum::operator() (cruft::view<const uint8_t*> data) const noexcept
{
    digest_t accum = 0;

    for (const auto i: data)
        accum = cruft::rotater (accum, 1) + i;

    return accum;
}
