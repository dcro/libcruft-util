/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2012 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_EXE_HPP
#define __UTIL_EXE_HPP

#include <filesystem>

namespace cruft {
    /// Returns a path to the current executable.
    std::filesystem::path image_path (void);
}

#endif
