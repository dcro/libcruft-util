/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2020 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <cstdint>
#include <limits>
#include <type_traits>
#include <iosfwd>

namespace cruft {
    /**
     * Represents a continuous range of values. Contains convenience functions
     * and debugging checks.
     */
    template <typename T>
    struct range {
        T lo;
        T hi;

        range (T _min, T _max);

        T magnitude (void) const;

        /// Check whether value falls within this range (inclusive)
        bool contains (T val) const;
        /// Check whether a range falls completely within (inclusive) this range
        bool contains (const range <T> &r) const;
        /// Check whether a range falls partially within (inclusive) this range
        //bool includes (const range <T> &r) const;

        /// interpolate between min-max using the unit position
        T at (float) const;

        /// Return the closest number that falls within the range.
        T clamp (T val) const;

        /// Expand the range to include this value if necessary
        void expand (T val);

        /// Normalise a number to [0, 1] within the range. Does not check
        /// bounds, it is the caller's responsibility to clamp the result if
        /// needed.
        template <typename U>
        U
        normalise (T val) const
        {
            static_assert (std::is_floating_point<U>::value,
                           "normalise isn't implemented for integer types");

            return static_cast<U> (val - lo) /
                   static_cast<U> ( hi - lo);
        }

        range& operator*= (T);
        range  operator*  (T) const;

        range& operator/= (T);
        range  operator/  (T) const;

        range& operator+= (T);
        range  operator+  (T) const;

        range& operator-= (T);
        range  operator-  (T) const;

        /// Return a pseudo-random uniformly distributed value within the range.
        /// There are no statistical randomness guarantees whatsoever.
        T random (void) const;

        bool operator ==(const range<T>& rhs) const;
        bool operator !=(const range<T>& rhs) const
            { return !(*this == rhs); }

        /// A range which is guaranteed to contain all elements type T
        static constexpr range<T> unlimited (void)
        {
            return {
                std::numeric_limits<T>::has_infinity ? -std::numeric_limits<T>::infinity () :
                std::numeric_limits<T>::lowest   (),
                std::numeric_limits<T>::has_infinity ?  std::numeric_limits<T>::infinity () :
                std::numeric_limits<T>::max      ()
            };
        }

        static constexpr range<T> max (void)
        {
            return {
                std::numeric_limits<T>::lowest (),
                std::numeric_limits<T>::max ()
            };
        }

        /// A range which only contains elements between 0 and 1 inclusive
        static constexpr range<T> unit (void)
        {
            return {
                T {0}, T {1}
            };
        }


        void sanity (void) const;
    };


    // convenience typedefs
    typedef cruft::range<float> rangef;
    typedef cruft::range<uintmax_t> rangeu;
    typedef cruft::range<intmax_t> rangei;

    // ostream operators
    template <typename T>
    std::ostream&
    operator <<(std::ostream &os, const range<T> &);
}
