/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2018 Danny Robson <danny@nerdcruft.net>
 */

#include "ray.hpp"

#include "iostream.hpp"
#include "ops.hpp"
#include "../coord/iostream.hpp"

using cruft::geom::ray;


///////////////////////////////////////////////////////////////////////////////
/// returns the closest parameter along the ray to a given point
template <size_t S, typename T>
T
ray<S,T>::closest (point<S,T> q) const
{
    // project the origin-point difference onto the direction
    return dot (origin - q, direction);
}


//-----------------------------------------------------------------------------
template <size_t S, typename T>
cruft::point<S,T>
ray<S,T>::at (T t) const
{
    return origin + direction * t;
}


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
std::ostream&
cruft::geom::operator<< (std::ostream &os, ray<S,T> r)
{
    return os << "ray(" << r.origin << ',' << r.direction << ')';
}


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
struct cruft::debug::validator<ray<S,T>> {
    static bool
    is_valid (ray<S,T> const& val)
    {
        return cruft::debug::is_valid (val.origin) &&
               cruft::debug::is_valid (val.direction) &&
               is_normalised (val.direction);
    }
};


///////////////////////////////////////////////////////////////////////////////
#define INSTANTIATE_S_T(S,T) \
template std::ostream& cruft::geom::operator<< (std::ostream&, ray<(S),T>); \
template struct cruft::debug::validator<ray<(S),T>>; \
template struct cruft::geom::ray<(S),T>;


INSTANTIATE_S_T(2,float)
INSTANTIATE_S_T(3,float)
