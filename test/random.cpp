#include "tap.hpp"

#include "random.hpp"

#include <iostream>


///////////////////////////////////////////////////////////////////////////////
int
main ()
{
    cruft::TAP::logger tap;

    {
        constexpr int ITERATIONS = 2048;
        constexpr int HI = 13;
        constexpr int LO = 3;

        bool success = true;

        for (int i = 0; i < ITERATIONS; ++i) {
            auto const val = cruft::random::uniform<int> (LO, HI);
            if (val < LO || val > HI) {
                std::cerr << val << '\n';
                success = false;
                break;
            }
        }

        tap.expect (success, "default uniform random int is within correct range");
    }

    return tap.status ();
}