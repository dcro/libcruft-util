/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <limits>

namespace cruft::rand {
    // implements a naive xorshift random generator.
    //
    // * users may not rely on identical output across executions or library
    //   updates. internal constants may change across releases
    template <typename ValueT>
    struct xorshift {
    public:
        using result_type = ValueT;

        explicit xorshift (ValueT seed);

        result_type operator() (void);

        static constexpr result_type min (void) noexcept { return 1u; }
        static constexpr auto max (void) noexcept { return std::numeric_limits<result_type>::max (); }

        void discard (unsigned);

    private:
        ValueT m_state;
    };
}