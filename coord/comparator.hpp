/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "base.hpp"

#include <compare>

#include <cstddef>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::coord {
    /// A StrictWeakOrder over coordinate types.
    ///
    /// The comparison operates across successive elements of the two
    /// coordinates.
    template <typename CoordT>
    std::weak_ordering
    element_ordering (
        CoordT const &a,
        CoordT const &b
    ) noexcept {
        for (std::size_t i = 0; i < CoordT::elements; ++i)
            if (auto cmp = a[i] <=> b[i]; cmp != 0)
                return cmp;

        return std::weak_ordering::equivalent;
    }


    /// A StrictWeakOrder over coordinate types.
    ///
    /// Deduction is deferred until the function is called, in the same way
    /// that std::less<> operates.
    template <typename = void>
    struct ordering {
        template <typename CoordT>
        bool
        operator() (CoordT const &a, CoordT const &b) const noexcept
        {
            return element_ordering<CoordT> (a, b) < 0;
        }
    };
}
