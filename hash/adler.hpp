/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2014 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_HASH_ADLER_HPP
#define __UTIL_HASH_ADLER_HPP

#include "fletcher.hpp"

#include <cstdint>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::hash {
    class adler32 : public fletcher<uint32_t> {
    public:
        adler32 ();
    };
}

#endif
