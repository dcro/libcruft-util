/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2020, Danny Robson <danny@nerdcruft.net>
 */

#include "path.hpp"

#include "../level.hpp"
#include "../packet.hpp"
#include "../../paths.hpp"

using cruft::log::sink::path;


///////////////////////////////////////////////////////////////////////////////
static std::filesystem::path
find_path (void)
{
    if (char const *log_path = getenv ("CRUFT_LOG_PATH"); log_path)
        return cruft::paths::expand (log_path);

    static constexpr char DEFAULT_PATH[] = PACKAGE_NAME ".log";
    return std::filesystem::current_path () / DEFAULT_PATH;
}


///////////////////////////////////////////////////////////////////////////////
path::path (std::string name)
    : crtp (std::move (name))
    , m_output (find_path ())
{ ; }


///////////////////////////////////////////////////////////////////////////////
void
path::write (packet const &val)
{
    if (val.level > log_level ())
        return;

    static const size_t time_len = strlen("YYYY-mm-dd HHMMhSS") + 1;
    std::string time_string (time_len - 1, '\0');
    time_t unix_time = time (nullptr);

    if (0 == strftime (&time_string[0],
                       time_len,
                       "%Y-%m-%d %H%Mh%S",
                       localtime (&unix_time)))
    {
        warn ("failed to log time");
        return;
    }

    m_output
        << time_string << " ["
        << std::setw (cruft::cast::lossless<int> (level_width ()))
        << std::left
        << val.level
        << std::setw (0)
        << "] " << val.message << std::endl;

    if (needs_break (val.level))
        breakpoint ();
}
