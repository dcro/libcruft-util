/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../posix/fd.hpp"


namespace cruft::rand {
    /// Models std::random_device and is suitable as a drop-in replacement.
    class file {
    public:
        using result_type = unsigned;

        file ();
        file (std::string const&);
        file (file const &) = delete;

        file& operator= (file const&) = delete;

        result_type operator() (void);

        double entropy (void) const noexcept;

        static constexpr result_type min (void) { return std::numeric_limits<result_type>::min (); }
        static constexpr result_type max (void) { return std::numeric_limits<result_type>::max (); }

    private:
        cruft::posix::fd m_fd;
    };

    using system = file;
}