///////////////////////////////////////////////////////////////////////////////
#include "job/queue.hpp"
#include "tap.hpp"

#include <chrono>
#include <iostream>
#include <thread>


///////////////////////////////////////////////////////////////////////////////
void sleep_inc (std::atomic<int> &count) noexcept
{
    ++count;
}


///////////////////////////////////////////////////////////////////////////////
int
main (void)
{
    cruft::TAP::logger tap;

    // dispatch `INNER' simple jobs `OUTTER' times that simply increment an
    // atomic variable and quit. this tests that all threads are created,
    // executed, and finished. it's not definitive, but executing this many
    // items this many times seems reasonably reliable in exposing deadlocks.
    bool success = true;
    constexpr int OUTTER = 4;
    constexpr int INNER  = 1<<10;

    for (auto i = 0; i < OUTTER && success; ++i) {
        std::atomic<int> count = 0;

        {
            cruft::job::queue q {std::thread::hardware_concurrency (), INNER};
            std::vector<cruft::job::queue::cookie> cookies;
            std::generate_n (std::back_inserter (cookies), INNER, [&] () {
                return q.submit (
                    sleep_inc,
                    std::ref (count)
                );
            });
        }

        success = count == INNER && success;
    }

    tap.expect (success, "%! trivial increment jobs of size %!", OUTTER, INNER);
    return tap.status ();
}