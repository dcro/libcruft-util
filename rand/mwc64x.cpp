/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016 Danny Robson <danny@nerdcruft.net>
 */


#include "mwc64x.hpp"

#include "../debug/assert.hpp"

using cruft::rand::mwc64x;


///////////////////////////////////////////////////////////////////////////////
mwc64x::mwc64x (seed_type _seed):
    m_state (_seed)
{
    CHECK_NEZ (m_state);
}


///////////////////////////////////////////////////////////////////////////////
mwc64x::result_type
mwc64x::operator() (void)
{
    CHECK_NEZ (m_state);

    uint32_t c = m_state >> 32u;
    uint32_t x = m_state & 0xFFFFFFFFu;

    m_state = x * 4294883355ULL + c;

    return x ^ c;
}
