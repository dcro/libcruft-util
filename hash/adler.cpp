/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2014 Danny Robson <danny@nerdcruft.net>
 */

#include "adler.hpp"

static constexpr unsigned MODULUS = 65521;

using cruft::hash::adler32;


///////////////////////////////////////////////////////////////////////////////
adler32::adler32 ():
    fletcher (MODULUS, 1, 0)
{ ; }

