/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2014-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "std.hpp"

#include "pointer.hpp"
#include "io.hpp"
#include "win32/file.hpp"
#include "win32/handle.hpp"
#include "win32/windows.hpp"
#include "view.hpp"

#include <cstdint>
#include <filesystem>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>


///////////////////////////////////////////////////////////////////////////////
// compatibility definitions
enum : int {
    PROT_NONE   = 0,
    PROT_READ   = 1 << 0,
    PROT_EXEC   = 1 << 1,
    PROT_WRITE  = 1 << 2
};


//-----------------------------------------------------------------------------
enum : int {
    MAP_SHARED,
    MAP_PRIVATE,

    MAP_ANONYMOUS
};


///////////////////////////////////////////////////////////////////////////////
// implementation definitions
namespace cruft {
    namespace detail::win32 {
        class mapped_file {
        public:
            using value_type = u08;
            using reference = value_type&;
            using const_reference = const value_type&;
            using iterator = value_type*;
            using const_iterator = const value_type*;
            using difference_type = std::iterator_traits<iterator>::difference_type;
            using size_type = std::size_t;

            mapped_file (::cruft::win32::file &&,
                         int fflags = O_RDONLY,
                         int mflags = PROT_READ);
            mapped_file (const std::filesystem::path &path,
                         int fflags = O_RDONLY,
                         int mflags = PROT_READ);
            mapped_file (cruft::posix::fd const&,
                         int fflag = O_RDONLY,
                         int mflags = PROT_READ);

            mapped_file (const mapped_file&) = delete;
            mapped_file& operator= (const mapped_file&) = delete;

            mapped_file (mapped_file&&) noexcept;
            mapped_file& operator= (mapped_file&&) noexcept;

            size_t size  (void) const;
            bool   empty (void) const;

            const uint8_t* data (void) const &;
            uint8_t*       data (void) &;

            uint8_t* begin (void) &;
            uint8_t* end   (void) &;

            const uint8_t* begin (void) const &;
            const uint8_t* end   (void) const &;

            const uint8_t* cbegin (void) const &;
            const uint8_t* cend   (void) const &;

            template <typename T>
            cruft::view<std::add_const_t<T>*>
            as_view () const &
            {
                return {
                    reinterpret_cast<const T*> (cbegin ()),
                    reinterpret_cast<const T*> (align::up (cend (), alignof (T)))
                };
            }

            template <typename T>
            cruft::view<T*>
            as_view () &
            {
                return {
                    reinterpret_cast<T *> (begin ()),
                    reinterpret_cast<T *> (align::up (end (), alignof(T)))
                };
            }

        private:
            ::cruft::win32::file m_file;
            ::cruft::win32::handle m_mapping;

            std::unique_ptr<uint8_t,BOOL(*)(LPCVOID)> m_data;
            uint64_t m_size;
        };
    }

    typedef detail::win32::mapped_file mapped_file;
}
