/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

///////////////////////////////////////////////////////////////////////////
namespace cruft::iterator {
    template <typename ValueT>
    class constant {
    public:
        using iterator_category = std::random_access_iterator_tag;
        using value_type = ValueT;
        using difference_type = std::size_t;
        using pointer = value_type*;
        using reference = value_type&;

        constant () = delete;

        constant (ValueT &&_value) noexcept
            : m_value (std::move (_value))
        { ; }

        constant (ValueT const &_value)
            : m_value (_value)
        { ; }

        constant (constant const &) = default;
        constant& operator= (constant const &) = default;

        constant (constant &&) = default;
        constant& operator= (constant &&) = default;

        ValueT const& operator*  () const& { return  m_value; }
        ValueT      & operator*  ()      & { return  m_value; }

        ValueT const* operator-> () const & { return &m_value; }
        ValueT      * operator-> ()       & { return &m_value; }

        constant& operator++ (   ) { return *this; }
        constant& operator++ (int) { return *this; }

        constant& operator-- (   ) { return *this; }
        constant& operator-- (int) { return *this; }

    private:
        ValueT m_value;
    };
}
