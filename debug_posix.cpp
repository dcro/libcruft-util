/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2011-2016, Danny Robson <danny@nerdcruft.net>
 */

#include "debug/debugger.hpp"
#include "debug/system.hpp"

#include "log.hpp"
#include "platform.hpp"

#include <unistd.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <signal.h>


#if defined(PLATFORM_FREEBSD)
#define PTRACE_ATTACH PT_ATTACH
#define PTRACE_DETACH PT_DETACH
#endif


///////////////////////////////////////////////////////////////////////////////
// Will return true if it is known we are under a debugger. This isn't
// designed to exhaustively check for debuggers, only to offer conveniences.
// Specifically: errors will return false.
static
bool
is_debugged (void)
{
    auto pid = fork ();
    if (pid == -1)
        return false;

    // We are the child
    if (pid == 0) {
        auto ppid = getppid ();
        int res;

        // attempt to trace our parent. this will fail if we're being debugged.
        if (ptrace (PTRACE_ATTACH, ppid, nullptr, 0) == 0) {
            waitpid (ppid, nullptr, 0);
            ptrace (PTRACE_DETACH, ppid, nullptr, 0);

            res = 0;
        } else {
            res = 1;
        }

        _exit (res);
    // We are the parent. Wait for our child to tell us the result.
    } else {
        int status;
        waitpid (pid, &status, 0);
        return WEXITSTATUS (status);
    }
}


///////////////////////////////////////////////////////////////////////////////
void
breakpoint (void)
{
    raise (SIGINT);
}


///////////////////////////////////////////////////////////////////////////////
void
prepare_debugger (void)
{
    ;
}


//-----------------------------------------------------------------------------
void
await_debugger (void)
{
    LOG_INFO ("awaiting debugger");

    if (is_debugged ())
        breakpoint ();
    else
        raise (SIGSTOP);

}


///////////////////////////////////////////////////////////////////////////////
#include <fenv.h>
void
enable_fpe (void)
{
    feenableexcept (FE_INVALID);
}


//-----------------------------------------------------------------------------
void
disable_fpe (void)
{
    feenableexcept (0);
}


///////////////////////////////////////////////////////////////////////////////
void
force_console (void)
{
    if (!isatty (fileno (stdout) && !isatty (fileno (stderr)))) {
        LOG_WARN ("expected debugging under a console");
    }
}
