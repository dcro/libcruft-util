/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

namespace cruft::iterator {
    template <typename Base>
    class referencing_iterator {
    protected:
        typedef typename std::enable_if<
            is_dereferencable<
                typename Base::value_type
            >::value,
            typename Base::value_type
        >::type base_value_type;

    public:
        typedef typename dereferenced_type<base_value_type>::type value_type        ;
        typedef typename Base::difference_type                    difference_type   ;
        typedef value_type&                                       reference         ;
        typedef value_type*                                       pointer;
        typedef typename Base::iterator_category                  iterator_category;

    protected:
        Base m_base;

    public:
        explicit referencing_iterator (Base _base):
            m_base (_base)
        { ; }

        referencing_iterator& operator++()    {                   ++m_base; return *this; }
        referencing_iterator  operator++(int) { auto val = *this; ++m_base; return   val; }

        bool operator== (const referencing_iterator<Base> &rhs) const { return m_base == rhs.m_base; }
        bool operator!= (const referencing_iterator<Base> &rhs) const { return m_base != rhs.m_base; }
        bool operator>= (const referencing_iterator<Base> &rhs) const { return m_base >= rhs.m_base; }
        bool operator<= (const referencing_iterator<Base> &rhs) const { return m_base <= rhs.m_base; }
        bool operator>  (const referencing_iterator<Base> &rhs) const { return m_base >  rhs.m_base; }
        bool operator<  (const referencing_iterator<Base> &rhs) const { return m_base <  rhs.m_base; }

        const value_type& operator*() const
        { return **m_base; }
        reference operator*()
        { return **m_base; }

        difference_type operator-(const referencing_iterator<Base>& rhs) const { return m_base - rhs.m_base; }
        referencing_iterator<Base> operator-(int rhs) const { return referencing_iterator (m_base - rhs); }
        referencing_iterator<Base> operator+(int rhs) const { return referencing_iterator (m_base + rhs); }

    };
}
