/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2020, Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "base.hpp"

#include <string>
#include <fstream>


namespace cruft::log::sink {
    class path : public crtp<path> {
    public:
        path (std::string name);

        void write (packet const&) override;

    private:
        std::ofstream m_output;
    };
}
