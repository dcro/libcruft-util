/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../expected.hpp"
#include "../view.hpp"

#include <system_error>

namespace cruft::parse {
    /// Parse a number with an SI suffix. eg, 10K for 10 * 1024
    template <typename ValueT>
    expected<ValueT, std::errc>
    si (cruft::view<char const*>);
}

