/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#include "buffer/simple.hpp"

#include <cstdlib>
#include <algorithm>


///////////////////////////////////////////////////////////////////////////////
int
main (int, char**)
{
    // Make a buffer and fill it with non-zero elements. This is just enough
    // to test that we can write to the memory without (obviously) crashing.
    static constexpr std::size_t elements = 2049;
    cruft::buffer::simple buf (elements);
    std::fill (std::begin (buf), std::end (buf), 0xff);

    return EXIT_SUCCESS;
}