/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#include "simd.hpp"

#include <ostream>


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
std::ostream&
cruft::coord::operator<< (std::ostream &os, simd<S,T> val)
{
    return os << "[ "
        << val[0] << ", "
        << val[1] << ", "
        << val[2] << ", "
        << val[3]
        << " ]";
}