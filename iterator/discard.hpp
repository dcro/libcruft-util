/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

namespace cruft::iterator {
    ///////////////////////////////////////////////////////////////////////////
    /// an output iterator that always discards any parameters on assignment.
    ///
    /// sometimes useful to pass to algorithms that generate useful results as
    /// a return value, while not caring about the implicit OutputIterator
    /// results.
    struct discard_iterator : public std::iterator<std::output_iterator_tag, discard_iterator> {
        template <typename T>
        void operator= (const T&) { ; }

        discard_iterator& operator++ (   ) { return *this; }
        discard_iterator  operator++ (int) { return *this; }
        discard_iterator& operator*  (   ) { return *this; }
    };
}
