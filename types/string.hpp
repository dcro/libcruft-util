/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_TYPES_STRING_HPP
#define __UTIL_TYPES_STRING_HPP

#include <string>

template <typename T>
std::string
type_to_string (void);

#endif
