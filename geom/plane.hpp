/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../point.hpp"
#include "../vector.hpp"


namespace cruft::geom {
    /// represents an S dimensional plane in parametric form: ax + by + cz + d = 0
    template <size_t S, typename T>
    struct plane {
        plane () = default;
        plane (cruft::point<S,T> base, cruft::vector<S,T> normal);

        explicit plane (cruft::vector<S+1,T> _coefficients):
            coefficients (_coefficients)
        { ; }

        cruft::vector<S+1,T> coefficients;
    };


    typedef plane<2,float> plane2f;
    typedef plane<3,float> plane3f;

    inline plane3f
    make_plane (cruft::point3f a, cruft::point3f b, cruft::point3f c)
    {
        return plane3f (a, normalised (cross (b - a, c - a)));
    }


    ///////////////////////////////////////////////////////////////////////////
    /// returns the normal for a plane
    template <size_t S, typename T>
    cruft::vector<S,T>
    normal (plane<S,T> p)
    {
        return p.coefficients.template redim<S> ();
    }


    ///////////////////////////////////////////////////////////////////////////
    /// normalises a plane's parametric form
    ///
    /// useful only after manually modifying the coefficients (as in default
    /// construction and piecewise initialisation). a plane will otherwise be
    /// assumed to be in normal form.
    template <size_t S, typename T>
    plane<S,T>
    normalised (plane<S,T> p)
    {
        const auto mag = norm (normal (p));
        CHECK_NEZ (mag);
        return plane<S,T> (p.coefficients / mag);
    }


    ///////////////////////////////////////////////////////////////////////////
    /// calculates the distance from a plane to a point.
    ///
    /// positive distances are in front of the plane, negative is behind;
    ///
    /// NOTE: `distance2` is _not_ the squared distance. It is the
    /// unnormalised distance. It needs to be divided by the norm of the
    /// plane normal.
    template <size_t S, typename T>
    T
    distance2 (plane<S,T> p, point<S,T> q)
    {
        return dot (p.coefficients, q.template redim<S+1> (1));
    }


    template <size_t S, typename T>
    T
    distance2 (point<S,T> p, plane<S,T> q)
    {
        return -distance2 (q, p);
    }


    template <size_t S, typename T>
    T
    distance (plane<S,T> p, point<S,T> q)
    {
        return distance2 (p, q) / norm (normal (p));
    }


    template <size_t S, typename T>
    T
    distance (point<S,T> p, plane<S,T> q)
    {
        return -distance (q, p);
    }


    ///////////////////////////////////////////////////////////////////////////
    template <size_t S, typename T>
    auto
    furthest (plane<S,T> p, const std::vector<cruft::point<S,T>> &cloud)
    {
        T maxd = -INFINITY;
        auto best = cloud.begin ();

        for (auto q = cloud.begin (), last = cloud.end (); q != last; ++q) {
            if (auto d = distance (p, *q); d > maxd) {
                maxd = d;
                best = q;
            }
        }

        return std::make_tuple (best, maxd);
    }

    template <size_t S, typename T>
    auto
    furthest (plane<S,T>, std::vector<cruft::point<S,T>> &&) = delete;
}
