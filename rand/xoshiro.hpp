/*
 * Written in 2019 by David Blackman and Sebastiano Vigna (vigna@acm.org)
 *
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 */

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2020, Danny Robson <danny@nerdcruft.net>
 */


#pragma once

#include "../std.hpp"

#include <array>
#include <limits>
#include <random>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::rand {
    class xoshiro256plusplus {
    public:
        using result_type = u64;

        xoshiro256plusplus (u64 seed);
        xoshiro256plusplus (std::seed_seq&);

        static constexpr result_type min (void)
        {
            return std::numeric_limits<result_type>::min ();
        }

        static constexpr result_type max (void)
        {
            return std::numeric_limits<result_type>::max ();
        }

        result_type operator() (void);

        void jump (void);
        void long_jump (void);

    private:
        std::array<u64, 4> m_state;
    };
}