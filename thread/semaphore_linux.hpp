/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#include <atomic>

namespace cruft::thread {
    /// Explicitly does not conform to BasicLockable.
    class semaphore {
    public:
        semaphore (int initial);
        semaphore ();

        semaphore (const semaphore&) = delete;
        semaphore& operator= (const semaphore&) = delete;
        semaphore (semaphore&&) = delete;
        semaphore& operator= (semaphore&&) = delete;

        int acquire (void);
        int acquire (int count);
        int release (void);
        int release (int count);

        auto   lock (void) { return acquire (); }
        auto unlock (void) { return release (); }

        int value (void) const;

        int operator++ (void);
        int operator-- (void);

    private:
        ::std::atomic<int> m_value;
    };
};
