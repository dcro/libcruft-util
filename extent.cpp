/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2016 Danny Robson <danny@nerdcruft.net>
 */

#include "extent.hpp"

#include "maths.hpp"

#include <algorithm>
#include <numeric>

using cruft::extent;


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
extent<S,T>::extent (vector<S,T> _v)
{
    std::transform (std::begin (_v),
                    std::end   (_v),
                    this->begin (),
                    [] (auto i) {
        // using std::abs gives unsigned abs warnings under clang. this tricks
        // it sufficiently to quiet the warnings.
        return i < 0 ? -i : i;
    });
}


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
extent<S,T>
extent<S,T>::expanded (cruft::vector<S,T> mag) const
{
    return *this + mag;
}


//-----------------------------------------------------------------------------
template <size_t S, typename T>
extent<S,T>
extent<S,T>::expanded (T t) const
{
    return *this + cruft::vector<S,T> {t};
}


//-----------------------------------------------------------------------------
template <size_t S, typename T>
extent<S,T>
extent<S,T>::contracted (vector<S,T> t) const
{
    return *this - t;
}


//-----------------------------------------------------------------------------
template <size_t S, typename T>
extent<S,T>
extent<S,T>::contracted (T t) const
{
    return *this - vector<S,T> {t};
}


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
bool
extent<S,T>::empty (void) const
{
    return almost_zero (area());
}


///////////////////////////////////////////////////////////////////////////////
template <typename T>
cruft::extent2<T>
cruft::rotate90 (cruft::extent2<T> obj, int steps)
{
    if (steps % 2) {
        return { obj.h, obj.w };
    } else {
        return obj;
    }
}


//-----------------------------------------------------------------------------
template cruft::extent2<i32> cruft::rotate90 (cruft::extent2<i32>, int);
template cruft::extent2<i64> cruft::rotate90 (cruft::extent2<i64>, int);
template cruft::extent2<f32> cruft::rotate90 (cruft::extent2<f32>, int);


///////////////////////////////////////////////////////////////////////////////
namespace cruft::debug  {
    template <size_t S, typename T>
    struct validator<extent<S,T>> {
        static bool is_valid (const extent<S,T> &e)
        {
            return std::all_of (std::begin (e.data),
                                std::end   (e.data),
                                [] (auto i) { return i >= 0; });
        }
    };
}


//-----------------------------------------------------------------------------
#define INSTANTIATE_S_T(S,T)        \
template struct ::cruft::extent<(S),T>;  \
template bool ::cruft::debug::is_valid (const ::cruft::extent<(S),T>&); \
template struct ::cruft::debug::validator<::cruft::extent<(S),T>>;

#define INSTANTIATE(T)      \
INSTANTIATE_S_T(1,T)        \
INSTANTIATE_S_T(2,T)        \
INSTANTIATE_S_T(3,T)

INSTANTIATE( int16_t)
INSTANTIATE( int32_t)
INSTANTIATE( int64_t)

INSTANTIATE(uint16_t)
INSTANTIATE(uint32_t)
INSTANTIATE(uint64_t)

INSTANTIATE(float)
INSTANTIATE(double)
