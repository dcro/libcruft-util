/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010 Danny Robson <danny@nerdcruft.net>
 */

#include "stats.hpp"

#include <limits>
#include <ostream>


template <typename T>
cruft::stats::accumulator<T>::accumulator () {
    reset ();
}


template <typename T>
void
cruft::stats::accumulator<T>::reset (void) {
    count = 0;
    min   = std::numeric_limits<T>::max ();
    max   = std::numeric_limits<T>::min ();
    sum   = 0;
}


template <typename T>
void
cruft::stats::accumulator<T>::add (T val) {
    min  = std::min (val, min);
    max  = std::max (val, max);
    sum += val;

    ++count;
}


template <typename T>
void
cruft::stats::accumulator<T>::add (const accumulator<T> &rhs) {
    min    = std::min (rhs.min, min);
    max    = std::max (rhs.max, max);
    sum   += rhs.sum;
    count += rhs.count;
}


template <typename T>
T
cruft::stats::accumulator<T>::range (void) const
    { return max - min; }


template <typename T>
T
cruft::stats::accumulator<T>::mean (void) const
    { return sum / count; }


template struct cruft::stats::accumulator<uint64_t>;
template struct cruft::stats::accumulator<double>;
template struct cruft::stats::accumulator<float>;


template <typename T>
std::ostream&
cruft::stats::operator<< (std::ostream &os, const accumulator<T> &rhs) {
    os << "(min: " << rhs.min << ", max: " << rhs.max << ")";
    return os;
}


template std::ostream& cruft::stats::operator<< (std::ostream&, const accumulator<uint64_t> &);
template std::ostream& cruft::stats::operator<< (std::ostream&, const accumulator<double> &);
template std::ostream& cruft::stats::operator<< (std::ostream&, const accumulator<float > &);

