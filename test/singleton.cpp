#include "singleton.hpp"

#include "tap.hpp"


int main ()
{
    cruft::TAP::logger tap;

    auto obj = cruft::singleton<int>::instantiate (42);
    tap.expect_eq (cruft::singleton<int>::get (), 42, "singleton stores value");

    return tap.status ();
}