/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_LIBRARY_HPP
#define __UTIL_LIBRARY_HPP

#include "platform.hpp"

#if defined(PLATFORM_LINUX)
#include "library_posix.hpp"
#elif defined(PLATFORM_WIN32)
#include "library_win32.hpp"
#endif

#endif
