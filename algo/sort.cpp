/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2015-2016, Danny Robson <danny@nerdcruft.net>
 */

#include "sort.hpp"


///////////////////////////////////////////////////////////////////////////////
// instantiate the soa sorting function as a compilation check.
//
// this particular instantiation isn't necessarily required by any user, it's
// just convenient.
template void cruft::sort::soa (int*, int*, bool (*)(int,int), double*);
