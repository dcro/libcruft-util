/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_LIBRARY_WIN32_HPP
#define __UTIL_LIBRARY_WIN32_HPP

#include "win32/windows.hpp"

#include "cast.hpp"

#include <filesystem>
#include <libloaderapi.h>

namespace cruft {
    namespace detail::win32 {
        class library {
        public:
            explicit library (const std::filesystem::path&);
            library (library const&) = delete;
            library& operator=(library const&) = delete;
            library (library&&);
            library& operator= (library&&);

            ~library ();

            template <typename FunctionT>
            FunctionT const
            symbol (const char *name) const
            {
                return cast::ffs<FunctionT> (
                      GetProcAddress (m_handle, name)
                );
            }

            operator HMODULE&() { return m_handle; }

        private:
            HMODULE m_handle;
        };
    }

    typedef detail::win32::library library;
}

#endif
