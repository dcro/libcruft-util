/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2020, Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "xoshiro.hpp"


///////////////////////////////////////////////////////////////////////////////
namespace cruft::rand {
    /// A generator with a good tradeoff between speed, size, and quality.
    using general_generator = xoshiro256plusplus;
}