#include "geom/aabb.hpp"
#include "geom/ray.hpp"

#include "tap.hpp"

#include <tuple>


int
main (int, char**)
{
    cruft::TAP::logger tap;

    {
        const auto upright = normalised (cruft::vector2f {1});

        static struct {
            cruft::point2f lo;
            cruft::point2f hi;

            cruft::point2f base;
            cruft::vector2f direction;

            float distance;

            const char *message;
        } const TESTS[] {
            {
                { 0, 0 }, { 1, 1 },
                { 0, 0 }, upright,
                std::sqrt (2.f),
                "ray on aabb corner, pointing to opposite corner",
            },
            {
                { 0, 0 }, { 1, 1 },
                { 0, 1 }, upright,
                INFINITY,
                "ray on aabb implied corner, pointing away",
            },
            {
                { 1, 1 }, { 9, 9 },
                { 2, 2 }, upright,
                std::sqrt (7.f * 7 * 2),
                "ray inside aabb, close to one corner and pointing to far corner",
            },
        };

        for (auto const &t: TESTS) {
            cruft::geom::aabb2f const shape (t.lo, t.hi);
            cruft::geom::ray2f const r {t.base, t.direction};
            tap.expect_eq (distance (r, shape), t.distance, "%!", t.message);
        }
    };


    return tap.status ();
}
