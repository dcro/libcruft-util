#include "hash/halfsipmix.hpp"
#include "tap.hpp"

#include <cstdio>


static constexpr u32
vectors_hsip32[64] = {
    0x5b9f35a9,
    0x89466e2a,
    0x8f84b8d0,
    0x2f87f057,
    0x84acb5d9,
    0x45104de3,
    0x630c4027,
    0x004064b0,
    0x1221e7fe,
    0xecc65ce7,
    0x4018bcb6,
    0x48d50577,
    0xe32ce169,
    0xd4f3853b,
    0x40b9c5a5,
    0x736ce7d4,
};


static constexpr u64
vectors_hsip64[64] = {
    0xc83cb8b9591f8d21,
    0xa12ee55b178ae7d5,
    0x8c85e4bc20e8feed,
    0x99c7f5ae9f1fc77b,
    0xb5f37b5fd2aa3673,
    0xdba7ee6f0a2bf51b,
    0xf1a63fae45107470,
    0xb516001efb5f922d,
    0x6c6211d8469d7028,
    0xdc7642ec407ad686,
    0x4caec8671cc8385b,
    0x5ab1dc27adf3301e,
    0x3e3ea94bc0a8eaa9,
    0xe150f598795a4402,
    0x1d5ff142f992a4a1,
    0x60e426bf902876d6,
};


static constexpr
u32 SRC[64] {
    0x03020100,
    0x07060504,
    0x0b0a0908,
    0x0f0e0d0c,
    0x13121110,
    0x17161514,
    0x1b1a1918,
    0x1f1e1d1c,
    0x23222120,
    0x27262524,
    0x2b2a2928,
    0x2f2e2d2c,
    0x33323130,
    0x37363534,
    0x3b3a3938,
    0x3f3e3d3c,
};


template <typename ValueT>
int siphash_test (ValueT const *v) {
    bool any_failed = false;
    int fails = 0;

    u32 const k0 = 0x03020100;
    u32 const k1 = 0x07060504;

    for (int i = 0; i < 16; ++i) {
        ValueT res = cruft::hash::halfsiphash<ValueT, 2, 4>{k0, k1} (std::span (SRC, SRC + i));

        if (v[i] != res) {
            fails++;
            any_failed = true;
        }
    }

    return any_failed;
}


int main ()
{
    cruft::TAP::logger tap;
    tap.expect_eq  (siphash_test<u32> (vectors_hsip32), 0, "siphash-2-4: u32");
    tap.expect_eq  (siphash_test<u64> (vectors_hsip64), 0, "siphash-2-4: u64");

    return tap.status ();
}