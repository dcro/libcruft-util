/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../point.hpp"
#include "../vector.hpp"

namespace cruft::geom {
    // an line of infinite extent that passes though 'base'.
    //
    // direction should be normalised
    template <size_t S, typename T>
    struct line {
        line () = default;
        line (cruft::point<S,T> a, cruft::point<S,T> b):
            line (a, normalised (b - a))
        { ; }

        line (cruft::point<S,T> _base, cruft::vector<S,T> _direction):
            base (_base),
            direction (_direction)
        {
            CHECK (is_normalised (direction));
        }

        cruft::point<S,T> base;
        cruft::vector<S,T> direction;
    };

    using line2f = line<2,float>;
    using line3f = line<3,float>;


    template <size_t S, typename T>
    T
    distance2 (line<S,T> l, point<S,T> p)
    {
        const auto t = dot (p - l.base, l.direction);
        return distance2 (p, l.base + l.direction * t);
    }


    template <size_t S, typename T>
    T
    distance (line<S,T> l, point<S,T> p)
    {
        return std::sqrt (distance2 (l, p));
    }
};
