/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "fwd.hpp"

#include <iosfwd>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::geom {
    template <size_t S, typename T>
    std::ostream&
    operator<< (std::ostream&, aabb<S,T>);

    template <size_t S, typename T>
    std::ostream&
    operator<< (std::ostream&, segment<S,T> const&);

    template <size_t S, typename T>
    std::ostream&
    operator<< (std::ostream&, ray<S,T>);

    template <size_t S, typename T>
    std::ostream&
    operator<< (std::ostream&, sphere<S,T>);

    template <size_t S, typename T>
    std::ostream&
    operator<< (std::ostream&, plane<S,T>);

    template <typename T>
    std::ostream&
    operator<< (std::ostream&, frustum<T>);
}
