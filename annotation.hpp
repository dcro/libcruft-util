/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <type_traits>


///////////////////////////////////////////////////////////////////////////////
/// Forwards the supplied `value` and annotates the result as likely to be the
/// value `typical`.
///
/// Must be an integral type.
template <typename T, typename = std::enable_if_t<std::is_integral_v<T>>>
constexpr inline
T
expect (T val, T typical)
{
    return __builtin_expect (val, typical);
}


/// Coerces the argument into a bool and annotates the result as likely to be
/// true.
template <typename T>
constexpr inline
bool
likely (T &&t)
{ return __builtin_expect (!!t, true); }


/// Coerces the argument into a bool and annotates the result as likely to be
/// false.
template <typename T>
constexpr inline
bool
unlikely (T &&t)
{ return __builtin_expect (!!t, false); }
