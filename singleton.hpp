/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "debug/assert.hpp"

#include <memory>


namespace cruft {
    /// statically stores a single value of the named type.
    ///
    /// this does not prevent the instantiation of the named type, but
    /// instead defines a method to refer to a single instance across an
    /// application.
    template <typename SelfT>
    class singleton {
    private:
        singleton () = default;

        static SelfT *instance;

    public:
        static bool
        is_valid (void) noexcept
        {
            return instance;
        }


        /// instantiates the one blessed value of this type.
        ///
        /// `instantiate` must be called once and once only before any calls
        /// to `get`. it is not threadsafe. there are some (unsafe)
        /// assertions against multiple instantiations but they may not be
        /// relied upon.
        ///
        /// returns a unique_ptr to the value which the caller must store
        /// until it will never be needed again.
        template <typename ...Args>
        static std::unique_ptr<SelfT>
        instantiate [[nodiscard]] (Args &&...args)
        {
            CHECK (!instance);

            auto cookie =std::make_unique<SelfT> (std::forward<Args> (args)...);
            instance = cookie.get ();
            return cookie;
        }


        /// Swap the stored object pointer for the provided one and return
        /// the old value.
        ///
        /// Useful for bypassing `instantiate` by initialising the pointer
        /// to a pre-existing object.
        ///
        /// The caller to swap must free any resources associated with the
        /// prior value and arrange for the new object to be safely destructed
        /// in the future.
        static SelfT*
        swap (SelfT *next)
        {
            return std::exchange (instance, next);
        }


        /// Returns a reference to the instantiated value.
        ///
        /// `instantiate` must have already been called before `get` is called
        /// otherwise the results are undefined.
        static SelfT&
        get (void)
        {
            CHECK (instance);
            return *instance;
        }


        /// returns a pointer to the instantiated value if it exists, else we
        /// return nullptr.
        static SelfT*
        maybe (void)
        {
            return instance;
        }
    };
};


template <typename SelfT>
SelfT*
cruft::singleton<SelfT>::instance;
