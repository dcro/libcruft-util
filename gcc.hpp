/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_GCC_HPP
#define __UTIL_GCC_HPP

#define GCC_VERSION (__GNUC__       * 10000 + \
                     __GNUC_MINOR__ * 100   + \
                     __GNUC_PATCHLEVEL__)

#endif
