#include "tap.hpp"
#include "alloc/linear.hpp"


///////////////////////////////////////////////////////////////////////////////
int
main (void)
{
    cruft::TAP::logger tap;

    constexpr size_t BUFFER_SIZE = 1024;

    alignas (std::max_align_t) u08 memory[BUFFER_SIZE];
    cruft::alloc::linear store (cruft::make_view (memory));

    tap.expect_eq (store.begin (), std::begin (memory), "base pointers match");
    tap.expect_eq (store.offset (std::begin (memory)), 0u, "base offset is 0");
    tap.expect_eq (store.capacity (), BUFFER_SIZE, "bytes capacity matches");

    tap.expect_throw<std::bad_alloc> (
        [&] (void) { store.allocate (BUFFER_SIZE + 1, 1); },
        "excessive allocation throws bad_alloc"
    );

    tap.expect_nothrow (
        [&] (void) { store.allocate (BUFFER_SIZE); },
        "maximum allocation succeeds"
    );

    tap.expect_eq (store.used (), BUFFER_SIZE, "bytes used matches");
    tap.expect_eq (store.remain (), 0u, "bytes remain matches");

    tap.expect_throw<std::bad_alloc> (
        [&] (void) { store.allocate (1, 1); },
        "minimum allocation fails after exhaustion"
    );

    store.reset ();

    tap.expect_nothrow (
        [&] (void) { store.allocate (1, 1); },
        "minimum allocation succeeds after reset"
    );

    return tap.status ();
}
