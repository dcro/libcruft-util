/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2017 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_UTIL_COORD_STORE_HPP
#define CRUFT_UTIL_COORD_STORE_HPP

#include "fwd.hpp"

#include "../platform.hpp"

#include <cruft/util/preprocessor.hpp>

#include <cstddef>
#include <type_traits>


///////////////////////////////////////////////////////////////////////////////
// Calculate a reasonable alignment for the given type and arity given what we
// know about the platform. Only intended to be used with alignas specifiers.
namespace cruft::coord::detail {
    template <typename T>
    constexpr
    std::size_t
    alignment (std::size_t S)
    {
        (void)S;

#if defined(__SSE_MATH__)
        // Align to 16 if we have 4x floats on SSE/NEON. There are other
        // possiblities, but we don't care about them right at this point.
        if (!std::is_same<T,float>::value)
            return alignof (T);

        if (S % 4 == 0)
            return 16;
#elif defined (__ARM_NEON__)
        // TODO: deal with alignment issues before adding alignas specifiers
#endif

        return alignof (T);
    }
}

#define SIMD_ALIGN(S,T) alignas (cruft::coord::detail::alignment<T> (S))


///////////////////////////////////////////////////////////////////////////////
// defines the named member variables that a coordinate type is composed of
#define DEFINE_COORD_STORE(TAG,...)                         \
namespace cruft::coord {                                    \
    template <typename T>                                   \
    struct store<                                           \
        VA_ARGS_COUNT(__VA_ARGS__),                         \
        T,                                                  \
        TAG                                                 \
    > {                                                     \
        union {                                             \
            struct {                                        \
                T __VA_ARGS__;                              \
            };                                              \
            std::array<T,VA_ARGS_COUNT(__VA_ARGS__)> data;  \
        };                                                  \
    };                                                      \
}


#define DEFINE_STORE(KLASS,...)                         \
template <typename T>                                   \
struct cruft::coord::store<                             \
    VA_ARGS_COUNT(__VA_ARGS__),                         \
    T,                                                  \
    ::cruft::KLASS<                                     \
        VA_ARGS_COUNT(__VA_ARGS__),                     \
        T                                               \
    >                                                   \
> {                                                     \
    union {                                             \
        T data[VA_ARGS_COUNT(__VA_ARGS__)];             \
        struct { T __VA_ARGS__; };                      \
    };                                                  \
};

DEFINE_STORE(extent,w)
DEFINE_STORE(extent,w,h)
DEFINE_STORE(extent,w,h,d)

DEFINE_STORE(point, x)
DEFINE_STORE(point, x, y)
DEFINE_STORE(point, x, y, z)
DEFINE_STORE(point, x, y, z, w)

DEFINE_STORE(vector, x)
DEFINE_STORE(vector, x, y)
DEFINE_STORE(vector, x, y, z)
DEFINE_STORE(vector, x, y, z, w)

#undef DEFINE_STORE

namespace cruft::coord {
    template <size_t S, typename T, typename SelfT>
    struct store {
        T data[S];
    };
}


#if 0
template <typename T>
struct cruft::coord::store<1,T,::cruft::extent<1,T>> {
    union { T data[1]; struct { T w; }; };
};

template <typename T>
struct cruft::coord::store<2,T,::cruft::extent<2,T>> {
    union { struct { T w, h; }; T data[2]; };
};

template <typename T>
struct cruft::coord::store<3,T,::cruft::extent<3,T>> {
    union { struct { T w, h, d; }; T data[3]; };
};

template <typename T>
struct cruft::coord::store<1,T,::cruft::point<1,T>> {
    union { struct { T x; }; T data[1]; };
};

template <typename T>
struct cruft::coord::store<2,T,::cruft::point<2,T>> {
    union { struct { T x, y; }; T data[2]; };
};

template <typename T>
struct cruft::coord::store<3,T,::cruft::point<3,T>> {
    union { struct { T x, y, z; }; T data[3]; };
};

template <typename T>
struct cruft::coord::store<4,T,::cruft::point<4,T>> {
    union { struct { T x, y, z, w; }; T data[4]; };
};

template <typename T>
struct cruft::coord::store<1,T,::cruft::vector<1,T>> {
    union { struct { T x; }; T data[1]; };
};

template <typename T>
struct cruft::coord::store<2,T,::cruft::vector<2,T>> {
    union { struct { T x, y; }; T data[2]; };
};

template <typename T>
struct cruft::coord::store<3,T,::cruft::vector<3,T>> {
    union { struct { T x, y, z; }; T data[3]; };
};

template <typename T>
struct cruft::coord::store<4,T,::cruft::vector<4,T>> {
    union { struct { T x, y, z, w; }; T data[4]; };
};
#endif
#endif
