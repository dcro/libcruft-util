#include <cruft/util/tap.hpp>
#include <cruft/util/concepts.hpp>

int main ()
{
    cruft::TAP::logger tap;

    tap.expect (cruft::concepts::container<std::vector<int>>, "vector is a container");
    tap.expect (!cruft::concepts::container<int>, "int is not a container");

    tap.expect (cruft::concepts::tuple<std::tuple<>>, "tuple<> is a tuple");
    tap.expect (cruft::concepts::tuple<std::tuple<int>>, "tuple<int> is a tuple");

    return tap.status ();
}