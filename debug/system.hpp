/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once
///////////////////////////////////////////////////////////////////////////////
void enable_fpe (void);
void disable_fpe (void);
void force_console (void);


///////////////////////////////////////////////////////////////////////////////
namespace cruft::debug {
    void init (void);


    /// Return true if the application appears to be running as an
    /// interactive console application.
    bool is_interactive_console (void);
}
