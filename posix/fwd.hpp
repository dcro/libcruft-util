/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __CRUFT_UTIL_POSIX_FWD_HPP
#define __CRUFT_UTIL_POSIX_FWD_HPP

namespace cruft::posix {
    class fd;
    class dir;
    class map;
}

#endif
