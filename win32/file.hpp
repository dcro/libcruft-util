/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "handle.hpp"
#include "../std.hpp"

#include <filesystem>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::win32 {
    class file {
    public:
        explicit file ();
        file (std::filesystem::path const &, DWORD access, DWORD share, DWORD create, DWORD flags);
        explicit file (handle&&) noexcept;
        explicit file (posix::fd&&);
        file (file&&) noexcept;
        file& operator= (file&&) noexcept;

        file (file const&) = delete;
        file& operator= (file const&) = delete;

        [[nodiscard]] DWORD read  (void* buf, size_t count);
        [[nodiscard]] DWORD write (void* buf, size_t count);

        [[nodiscard]] DWORD read  (void* buf, size_t count, OVERLAPPED&);
        [[nodiscard]] DWORD write (void* buf, size_t count, OVERLAPPED&);

        [[nodiscard]] u64 set_pointer (u64 offset, DWORD method);

        decltype(auto) native (void) { return m_handle.native (); }

        operator auto () { return m_handle.native (); }

    private:
        handle m_handle;
    };
}
