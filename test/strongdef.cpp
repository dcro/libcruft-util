#include "tap.hpp"
#include "strongdef.hpp"


///////////////////////////////////////////////////////////////////////////////
int
main (void)
{
    cruft::TAP::logger tap;

    // These tests are less about functional testing, and more about link testing.
    cruft::strongdef::index<void,unsigned> fortytwo (42u);
    tap.expect_eq (*fortytwo, 42u, "raw data equality");
    tap.expect_eq ( fortytwo, decltype(fortytwo) (42u), "passthrough equality");

    return tap.status ();
}
