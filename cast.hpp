/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once


#include "concepts.hpp"
#include "debug/assert.hpp"
#include "debug/validate.hpp"
#include "platform.hpp"

#include <string_view>
#include <type_traits>
#include <limits>

#include <cstring>


namespace cruft::cast {
    ///////////////////////////////////////////////////////////////////////////
    /// Safely cast a numeric type to its (un)signed counterpart, aborting if
    /// the dynamically checked result is not representable. May be optimised
    /// out if NDEBUG is defined.
    ///
    /// The signed/unsigned and unsigned/signed cases are split so we can
    /// simplify the out of range tests.
    ///
    /// The same-type case is not provided because we want to error out on
    /// unnecessary casts.
    template <
        typename T,
        typename U
    >
    requires
        (sizeof(T) == sizeof(U)) &&
        std::is_unsigned<T>::value &&
        std::is_signed<U>::value
    T
    sign (const U u)
    {
        CHECK_GE (u, 0);
        return static_cast<T> (u);
    }


    //-------------------------------------------------------------------------
    template <
        typename T,
        typename U
    >
    requires
        (sizeof(T) == sizeof (U))  &&
        std::is_signed<T>::value &&
        std::is_unsigned<U>::value
    T
    sign (const U u)
    {
        CHECK_LT (u, std::numeric_limits<U>::max () / 2);
        return static_cast<T> (u);
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Cast to a smaller type of the same signedness and realness and assert
    /// that both values are still equal.
    ///
    /// Any runtime checks will be compiled out if NDEBUG is defined.
    ///
    /// Identity casts are allowed so as to simplify the use of this routine
    /// in template code.
    template <
        concepts::arithmetic NarrowT,
        concepts::arithmetic WideT
    >
    requires
        (std::is_signed_v<NarrowT> == std::is_signed_v<WideT>) &&
        (std::is_floating_point_v<NarrowT> == std::is_floating_point_v<WideT>) &&
        (sizeof (NarrowT) <= sizeof (WideT))
    constexpr NarrowT
    narrow (const WideT &val)
    {
        static_assert (sizeof (NarrowT) <= sizeof (WideT));

#ifndef NDEBUG
        auto narrow = static_cast<NarrowT> (val);
        CHECK_EQ (narrow, val);
        return narrow;
#else
        return static_cast<NarrowT> (val);
#endif
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Cast between types checking that exact equality holds if the result is
    /// casted back to the original type.
    ///
    /// Runtime checks will be compiled out if NDEBUG is defined.
    template <typename DstT, typename SrcT>
    constexpr DstT
    lossless (const SrcT &src)
    {
#ifndef NDEBUG
        // GCC insists that the initial static_cast to DstT is a floating
        // point comparison if we pass in a bool and a float.
        //
        // The only way around this is to ignore the warning locally (we use
        // almost_equal inside CHECK_EQ anyway, so it should not be a concern).
        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wfloat-equal"

        auto dst = static_cast<DstT> (src);

        if constexpr (std::is_floating_point_v<SrcT>) {
            if (std::isnan (src)) {
                // NaNs must remaing as NaN's. They're important.
                CHECK (std::is_floating_point_v<DstT>);
                CHECK (std::isnan (dst));
            }
        }

        // Cast dst back to src to check round-trip conversion
        // is lossless.
        CHECK_EQ (static_cast<SrcT> (dst), src);

        #pragma GCC diagnostic pop
        return dst;
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wshorten-64-to-32"
        if constexpr (std::is_enum_v<DstT>) {
            auto const val = static_cast<std::underlying_type_t<DstT>> (src);
            return static_cast<DstT> (val);
        } else {
            return static_cast<DstT> (src);
        }
#pragma GCC diagnostic pop
#endif
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Assert if the value is not a pointer to a subclass of T, else return
    /// the converted value. Note: this is only a debug-time check and is
    /// compiled out in optimised builds.
    template <
        concepts::pointer T,
        typename V
    >
    T
    known (V *const v)
    {
        CHECK (dynamic_cast<T> (v));
        return static_cast<T> (v);
    }


    //-------------------------------------------------------------------------
    template <
        concepts::reference T,
        typename V
    >
    T
    known (V &v)
    {
        CHECK_NOTHROW (dynamic_cast<T> (v));
        return reinterpret_cast<T> (v);
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Cast a pointer from one type to another, asserting that the required
    /// alignment of the destination type has been satisfied.
    ///
    /// Runtime checks will be compiled out if NDEBUG is defined.
    template <
        concepts::pointer DstT,
        concepts::pointer SrcT
    >
    DstT
    alignment (SrcT src)
    {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-align"
#ifdef COMPILER_GCC
#pragma GCC diagnostic ignored "-Waddress-of-packed-member"
#endif
        CHECK_MOD (reinterpret_cast<uintptr_t> (src), alignof (std::remove_pointer_t<DstT>));
        return reinterpret_cast<DstT> (src);
#pragma GCC diagnostic pop
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Cast from SrcT to DstT and damn any consequences; just make it compile.
    template <typename DstT, typename SrcT>
    DstT ffs (SrcT src)
    {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wcast-align"
#pragma GCC diagnostic ignored "-Wold-style-cast"
#pragma GCC diagnostic ignored "-Wcast-qual"
#if defined(COMPILER_GCC)
#pragma GCC diagnostic ignored "-Wcast-function-type"
#endif
        return (DstT)src;
#pragma GCC diagnostic pop
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Cast from SrcT to DstT, performing sanity checks on the src and dst
    /// values before returning the result.
    template <typename DstT, typename SrcT>
    DstT sanity (SrcT src)
    {
        cruft::debug::sanity (src);
        DstT dst = static_cast<DstT> (src);
        cruft::debug::sanity (dst);
        return dst;
    }


    /// Convert from SrcT to DstT by reinterpreting the bits that make up SrcT.
    /// Effectively a reinterpret_cast of SrcT but without the undefined
    /// behaviour.
    ///
    /// CXX#20: Convert instances of me to std::bit_cast when it becomes
    /// available in supported compilers.
    template <typename DstT, typename SrcT>
    DstT
    bit (SrcT &&src)
    {
        static_assert (sizeof (DstT) == sizeof (SrcT));
        static_assert (std::is_trivially_copyable_v<std::decay_t<DstT>>);
        static_assert (std::is_trivially_copyable_v<std::decay_t<SrcT>>);

        DstT dst;
        std::memcpy (&dst, &src, sizeof (DstT));
        return dst;
    }
}
