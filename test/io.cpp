#include "tap.hpp"

#include "io.hpp"
#include "types/traits.hpp"


///////////////////////////////////////////////////////////////////////////////
int main ()
{
    cruft::TAP::logger tap;
    tap.expect (is_container_v<cruft::mapped_file>, "mapped_file is a container");
    return tap.status ();
}
