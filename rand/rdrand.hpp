/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../posix/fd.hpp"


namespace cruft::rand {
    /// Models std::random_device and is suitable as a drop-in replacement.
    ///
    /// This implementation assumes that the CPU supports the `rdrand`
    /// instruction, and does not check that this is the case.
    ///
    /// It is safe to instantiatate the object even if the instructions are
    /// not supported, but it is the users responsibility to prevent calls to
    /// the object if this is not the case.
    class rdrand {
    public:
        using result_type = unsigned;

        rdrand ();
        rdrand (std::string const&);
        rdrand (rdrand const &) = delete;

        rdrand& operator= (rdrand const&) = delete;

        result_type operator() (void);

        double entropy (void) const noexcept;

        static constexpr result_type min (void) { return std::numeric_limits<result_type>::min (); }
        static constexpr result_type max (void) { return std::numeric_limits<result_type>::max (); }
    };
}
