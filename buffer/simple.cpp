/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#include "simple.hpp"

#include "traits.hpp"

using cruft::buffer::simple;


///////////////////////////////////////////////////////////////////////////////
simple::simple (size_t _size)
    : m_base (new u08[_size])
    , m_size (_size)
{ ; }


///////////////////////////////////////////////////////////////////////////////
simple::value_type* simple::begin (void)& { return m_base.get (); }
simple::value_type* simple::end   (void)& { return begin () + m_size; }


//-----------------------------------------------------------------------------
simple::value_type const* simple::begin (void) const& { return m_base.get (); }
simple::value_type const* simple::end   (void) const& { return begin () + m_size; }


///////////////////////////////////////////////////////////////////////////////
static_assert (cruft::buffer::is_buffer_v<cruft::buffer::simple>);
