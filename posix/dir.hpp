/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "except.hpp"

#include <cerrno>
#include <dirent.h>

#include <functional>
#include <filesystem>


namespace cruft::posix {
    struct dir {
    public:
        explicit dir (const std::filesystem::path&);
        ~dir ();

        operator DIR* (void);

        // run a callback for each entry in the provided directory
        template <typename FunctionT, typename ...Args>
        void
        scan (FunctionT &&func, Args&&...args)
        {
            rewind ();

            for (dirent *cursor; errno = 0, cursor = readdir (m_handle); )
                std::invoke (func, args..., cursor->d_name);

            error::try_code ();
        }

        //entry begin (void) { rewind (); return { readdir (m_handle), m_handle }; }
        //entry end (void) { return { nullptr, m_handle }; }

        void rewind (void);

    private:
        DIR *m_handle;
    };
}
