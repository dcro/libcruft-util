/******************************************************************************
                              _       _
                             | |     | |
                             | | ___ | |__   ___
                             | |/ _ \| '_ \ / _ \
                             | | (_) | |_) | (_) |
                             |_|\___/|_.__/ \___/
    Copyright:
        Danny Robson, 2020
 *****************************************************************************/

#include "stringcache.hpp"

#include "cast.hpp"

using cruft::stringcache;


///////////////////////////////////////////////////////////////////////////////
stringcache::id_t
stringcache::add (std::string_view val)
{
    auto const required_size = std::ssize (val);

    if (m_values.size () > std::numeric_limits<value_type>::max ())
        throw std::bad_alloc ();
    if (m_store.size () + required_size > std::numeric_limits<value_type>::max ())
        throw std::bad_alloc ();

    slot_t allocation {
        .start  = value_type (m_store.size ()),
        .length = value_type (required_size),
    };

    m_store.resize (m_store.size () + allocation.length);
    std::copy (
        std::begin (val),
        std::end   (val),
        std::begin (m_store) + allocation.start
    );

    m_values.push_back (allocation);
    return id_t (m_values.size () - 1);
}


//-----------------------------------------------------------------------------
stringcache::id_t
stringcache::add (std::string const &val)
{
    return add (std::string_view (val));
}

//-----------------------------------------------------------------------------
stringcache::id_t
stringcache::add (char const *val)
{
    return add (std::string_view (val));
}


///////////////////////////////////////////////////////////////////////////////
std::string_view
stringcache::operator[] (id_t idx)
{
    slot_t const entry = m_values[value_type (idx)];
    return std::string_view (m_store.data () + entry.start, entry.length);
}
