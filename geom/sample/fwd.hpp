/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <type_traits>


namespace cruft::geom::sample {
    /// Provides an interface to randomly sample points within the volume of
    /// a supplied shape.
    ///
    /// This class _should_ be specialised for performance. Though a basic
    /// rejection sampling implementation is defined as a fallback.
    template <typename ShapeT>
    class volume;

    template <typename ShapeT>
    volume (ShapeT const&) -> volume<std::decay_t<ShapeT>>;


    /// Provides an interface to randomly sample points across the surface of
    /// a supplied shape.
    ///
    /// The class _must_ be specialised for each shape.
    template <typename ShapeT>
    class surface;

    template <typename ShapeT>
    surface (ShapeT const&) -> surface<std::decay_t<ShapeT>>;


    /// Provides an interface to randomly sample points across the edges of a
    /// shape; eg, the perimeter of an extent or region.
    template <typename ShapeT>
    struct edge;


    template <typename ShapeT>
    edge (ShapeT const &) -> edge<std::decay_t<ShapeT>>;
}
