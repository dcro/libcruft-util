/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2019 Danny Robson <danny@nerdcruft.net>
 */

#include "xorshift.hpp"

#include "../debug/assert.hpp"

#include <cstdint>

using cruft::rand::xorshift;


///////////////////////////////////////////////////////////////////////////////
template <typename T>
xorshift<T>::xorshift (T seed):
    m_state (seed)
{
    if (!m_state)
        throw std::invalid_argument ("xorshift seed must not be zero");
}


///////////////////////////////////////////////////////////////////////////////
// setup the constants a, b, and c. we use the values from the example
// generators provided in Marsaglia's paper.
template <typename T>
struct constants;


//-----------------------------------------------------------------------------
template <>
struct constants<uint32_t>
{
    static constexpr uint32_t a = 13u;
    static constexpr uint32_t b = 17u;
    static constexpr uint32_t c =  5u;
};


//-----------------------------------------------------------------------------
template <>
struct constants<uint64_t>
{
    static constexpr uint64_t a = 13u;
    static constexpr uint64_t b =  7u;
    static constexpr uint64_t c = 17u;
};


///////////////////////////////////////////////////////////////////////////////
template <typename T>
typename xorshift<T>::result_type
xorshift<T>::operator() (void)
{
    m_state ^= m_state >> constants<T>::a;
    m_state ^= m_state << constants<T>::b;
    m_state ^= m_state >> constants<T>::c;

    CHECK_NEZ (m_state);
    return m_state;
}


///////////////////////////////////////////////////////////////////////////////
template <typename T>
void
xorshift<T>::discard (unsigned count)
{
    while (count--)
        (*this)();
}


///////////////////////////////////////////////////////////////////////////////
template struct cruft::rand::xorshift<uint32_t>;
template struct cruft::rand::xorshift<uint64_t>;
