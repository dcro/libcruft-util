/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011-2014 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "types/sized.hpp"
#include "maths.hpp"

#include <iosfwd>

namespace cruft {
    template <
        // whether the type is signed or unsigned. supply an appropriately
        // signed type here; it won't be used for anything else.
        typename T,
        // how many bits total are used for the integral component
        unsigned I,
        // how many bits are used for the fractional component
        unsigned E
    >
    class [[gnu::packed]] fixed {
    public:
        static_assert (I > 0);
        static_assert (E > 0);
        static_assert ((I + E) % 8 == 0);

        using sint_t = typename types::sized::bits<I+E>::sint;
        using uint_t = typename types::sized::bits<I+E>::uint;

        using native_type = typename std::conditional<
            std::is_signed<T>::value,
            sint_t, uint_t
        >::type;

        using integer_type = typename std::conditional<
            std::is_signed<T>::value,
            typename types::sized::bits<divup (I, 8u) * 8u>::sint,
            typename types::sized::bits<divup (I, 8u) * 8u>::uint
        >::type;

        explicit fixed (double);
        explicit fixed (float);

        double to_double (void) const;
        float  to_float  (void) const;
        integer_type to_integer (void) const;
        native_type  to_native  (void) const;

        static fixed<T,I,E> from_native (native_type);
        static fixed<T,I,E> from_integer (integer_type);

        static integer_type to_integer (native_type);

        fixed<T,I,E>& operator +=(const fixed<T,I,E>);
        fixed<T,I,E>& operator -=(const fixed<T,I,E>);
        fixed<T,I,E>& operator *=(const fixed<T,I,E>);
        fixed<T,I,E>& operator /=(const fixed<T,I,E>);

        fixed<T,I,E>  operator +(const fixed<T,I,E>) const;
        fixed<T,I,E>  operator -(const fixed<T,I,E>) const;
        fixed<T,I,E>  operator *(const fixed<T,I,E>) const;
        fixed<T,I,E>  operator /(const fixed<T,I,E>) const;

        fixed<T,I,E>& operator +=(integer_type);
        fixed<T,I,E>& operator -=(integer_type);
        fixed<T,I,E>& operator *=(integer_type);
        fixed<T,I,E>& operator /=(integer_type);

        fixed<T,I,E>  operator +(integer_type) const;
        fixed<T,I,E>  operator -(integer_type) const;
        fixed<T,I,E>  operator *(integer_type) const;
        fixed<T,I,E>  operator /(integer_type) const;

    private:
        fixed () = default;

        native_type m_value;
    };


    template <typename T, unsigned I, unsigned E>
    constexpr
    fixed<T,I,E>
    bswap (fixed<T,I,E> val)
    {
        return fixed<T,I,E>::from_native (
            bswap (val.to_native ())
        );
    }


    template <typename T, unsigned I, unsigned E> bool operator== (cruft::fixed<T,I,E>, cruft::fixed<T,I,E>);
    template <typename T, unsigned I, unsigned E> bool operator!= (cruft::fixed<T,I,E>, cruft::fixed<T,I,E>);
    template <typename T, unsigned I, unsigned E> bool operator<  (cruft::fixed<T,I,E>, cruft::fixed<T,I,E>);
    template <typename T, unsigned I, unsigned E> bool operator<= (cruft::fixed<T,I,E>, cruft::fixed<T,I,E>);
    template <typename T, unsigned I, unsigned E> bool operator>  (cruft::fixed<T,I,E>, cruft::fixed<T,I,E>);
    template <typename T, unsigned I, unsigned E> bool operator>= (cruft::fixed<T,I,E>, cruft::fixed<T,I,E>);

    template <typename T, unsigned I, unsigned E> bool operator== (cruft::fixed<T,I,E>, typename cruft::fixed<T,I,E>::integer_type);
    template <typename T, unsigned I, unsigned E> bool operator!= (cruft::fixed<T,I,E>, typename cruft::fixed<T,I,E>::integer_type);
    template <typename T, unsigned I, unsigned E> bool operator<  (cruft::fixed<T,I,E>, typename cruft::fixed<T,I,E>::integer_type);
    template <typename T, unsigned I, unsigned E> bool operator<= (cruft::fixed<T,I,E>, typename cruft::fixed<T,I,E>::integer_type);
    template <typename T, unsigned I, unsigned E> bool operator>  (cruft::fixed<T,I,E>, typename cruft::fixed<T,I,E>::integer_type);
    template <typename T, unsigned I, unsigned E> bool operator>= (cruft::fixed<T,I,E>, typename cruft::fixed<T,I,E>::integer_type);

    template <typename T, unsigned I, unsigned E>
    std::ostream& operator<< (std::ostream&, fixed<T,I,E>);
}
