/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2014-2016, Danny Robson <danny@nerdcruft.net>
 */

#include "json/flat.hpp"
#include "json/except.hpp"
#include "io.hpp"

#include <iostream>
#include <cstdlib>


enum {
    ARG_CMD,
    ARG_PATH,

    NUM_ARGS
};


int
main (int argc, char ** argv) {
    if (argc != NUM_ARGS) {
        std::cerr << "Invalid arguments. "
                  << argv[ARG_CMD] << " <path> "
                  << std::endl;
        return EXIT_FAILURE;
    }

    try {
        const cruft::mapped_file data (argv[ARG_PATH]);
        json::flat::parse (cruft::view{data}.cast<const char*> ());
    } catch (const json::error &x) {
        std::cerr << "error: " << x.what () << '\n';
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
