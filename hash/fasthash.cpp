/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include "fasthash.hpp"

#include "../endian.hpp"

using cruft::hash::fasthash;


///////////////////////////////////////////////////////////////////////////////
template <>
uint64_t
fasthash<uint64_t>::operator() (uint64_t seed, const cruft::view<const uint8_t*> data) const
{
    static const uint64_t m = 0x880355f21e6d1965;

    uint64_t result = seed ^ (data.size () * m);

    auto cursor = data.begin ();
    for (auto last = data.end () - data.size () % 8; cursor < last; cursor += 8) {
        result ^= mix (readle<uint64_t> (cursor));
        result *= m;
    }

    size_t remain = data.size () % 8;
    if (remain) {
        uint64_t accum = 0;
        for (size_t i = 0; i < remain; ++i)
            accum ^= uint64_t {cursor[i]} << i * 8;

        result ^= mix (accum);
        result *= m;
    }

    return mix (result);
} 


//-----------------------------------------------------------------------------
template <>
uint32_t
fasthash<uint32_t>::operator() (uint64_t seed, const cruft::view<const uint8_t*> data) const
{
    auto h = fasthash<uint64_t> {} (seed, data);
    return (h & 0xffffffff) - (h >> 32);
}

