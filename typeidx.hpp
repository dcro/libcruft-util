/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2017-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <atomic>
#include <variant>


namespace cruft {
    namespace detail {
        template <typename TagT>
        int typeidx_next (void)
        {
            static std::atomic<int> counter;
            return ++counter;
        }
    }

    /// Return a globally unique runtime ID for a given type (namespaced by a
    /// tag type).
    ///
    /// This is intended to be used as a lightweight type check for variants
    /// and such without requiring RTTI.
    ///
    /// The identifier is constructed at runtime and is not guaranteed to be
    /// stable across executions (particularly in the case of threads and
    /// other non-determinism). However it _is_ threadsafe to call this.
    ///
    /// The value 0 will never be returned from this function and can be used
    /// to indicate an 'unknown' state.
    ///
    /// The range of identifiers is _probably_ contiguous. This should not
    /// be relied upon for correctness, but may be used for performance
    /// concerns.
    ///
    /// \tparam T     The type to register-and-retrieve the ID for
    /// \tparam TagT  A namespacing type; different tags will have different
    ///               sets of IDs. The default Tag is void.
    /// \return       The unique ID of the type
    template <typename T, typename TagT = void>
    int
    typeidx (void)
    {
        static auto id = detail::typeidx_next<TagT> ();
        return id;
    }


    /// Returns the typeidx of the contained value within a variant.
    ///
    /// May throw std::bad_variant_access if the variant is
    /// valueless_by_exception.
    template <typename TagT=void, typename ...ComponentT>
    int
    value_typeidx (std::variant<ComponentT...> const &val)
    {
        return std::visit (
            [] <typename ValueT> (ValueT const &) {
                return typeidx<ValueT> ();
            },
            val
        );
    }
}
