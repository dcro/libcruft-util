#include "tap.hpp"

#include "geom/segment.hpp"
#include "geom/ops.hpp"
#include "region.hpp"


///////////////////////////////////////////////////////////////////////////////
void test_point_distance (cruft::TAP::logger &tap)
{
    static struct {
        cruft::point3f a, b;
        cruft::point3f q;
        float distance;
        const char *message;
    } const TESTS[] = {
        { { 0, 0, 0 }, { 1, 1, 1 }, { 0, 0, 0 }, 0, "origin line, origin point" },
        { { 0, 0, 0 }, { 1, 1, 1 }, { 1, 1, 1 }, 0, "origin line, point on line" },
        { { 0, 1, 0 }, { 1, 1, 0 }, { 0, 0, 0 }, 1, "+x line, unit distance" },
    };

    for (auto const& t: TESTS) {
        cruft::geom::segment3f s {t.a, t.b};
        auto d = distance (s, t.q);
        tap.expect_eq (d, t.distance, "%!", t.message);
    }
}


//-----------------------------------------------------------------------------
void test_region_intersection (cruft::TAP::logger &tap)
{
    using p = cruft::point2i;

    static struct {
        cruft::geom::segment2i a;
        cruft::region2i b;
        bool expected;
        char const *message;
    } const TESTS[] = {
        {
            .a = { p { 0, 0 }, p { 4, 4 }, },
            .b = { p { 1, 1 }, p { 2, 2 }, },
            .expected = true,
            .message = "rising into"
        },
        {
            .a = { p {  0,  0 }, p {  1,  1 } },
            .b = { p { -2, -2 }, p {  2,  2 } },
            .expected = true,
            .message = "completely internal",
        },
        {
            .a = { p {  0,  0 }, p {  1,  0 } },
            .b = { p {  0,  0 }, p {  1,  1 } },
            .expected = true,
            .message = "glancing horizontal",
        },
        {
            .a = { p {  0,  0 }, p { -1, -1 } },
            .b = { p {  0,  0 }, p {  1,  1 } },
            .expected = true,
            .message = "common origin",
        },
    };

    for (auto const &t: TESTS)
        tap.expect_eq (intersects (t.a, t.b), t.expected, "region intersection: %!", t.message);
}


///////////////////////////////////////////////////////////////////////////////
void test_bresenham (cruft::TAP::logger &tap)
{
    static struct {
        cruft::point2i a;
        cruft::point2i b;
        std::vector<cruft::point2i> expected;
        char const *message;
    } const TESTS[] = {
        {
            .a = { 0 },
            .b = { 3 },
            .expected = { { 0 }, { 1 }, { 2 }, { 3 } },
            .message = "positive unit direction"
        },
        {
            .a = { 1, 2 },
            .b = { 1, 4 },
            .expected = { { 1, 2 }, { 1, 3 }, { 1, 4 } },
            .message = "vertical only"
        },
        {
            .a = { -2,  1 },
            .b = {  1, -1 },
            .expected = { { -2, 1 }, { -1, 0 }, { 0, 0 }, { 1, -1 } },
            .message = "cross origin"
        },
        {
            .a = { 1, 1 },
            .b = { 1, 1 },
            .expected = { { 1, 1 } },
            .message = "zero length segment"
        },
    };

    for (auto const &t: TESTS) {
        std::vector<cruft::point2i> computed;
        for (auto const p: cruft::geom::bresenham ({ t.a, t.b }))
            computed.push_back (p);
        tap.expect_eq (computed, t.expected, "bresenham: %!", t.message);
    }
}


///////////////////////////////////////////////////////////////////////////////
int
main (int, char**)
{
    cruft::TAP::logger tap;
    test_point_distance (tap);
    test_region_intersection (tap);
    test_bresenham (tap);
    return tap.status ();
}