#include "tap.hpp"
#include "tuple/index.hpp"


int
main ()
{
    cruft::TAP::logger tap;

    {
        using a_t = cruft::tuple::index::indices<0,1,2>;
        using b_t = cruft::tuple::index::indices<4,3>;

        using result_t = cruft::tuple::index::cat<a_t,b_t>::type;
        using expected_t = cruft::tuple::index::indices<0,1,2,4,3>;

        tap.expect (std::is_same_v<result_t, expected_t>, "index concatenation");
    }

    {
        using indices_t = cruft::tuple::index::make_indices<4>::type;
        using expected_t = cruft::tuple::index::indices<0,1,2,3>;

        tap.expect (std::is_same_v<indices_t,expected_t>, "index sequence creation");
    }

    {
        using indices_t = cruft::tuple::index::make_reverse_t<4>;
        using expected_t = cruft::tuple::index::indices<3,2,1,0>;
        tap.expect (std::is_same_v<indices_t,expected_t>, "reverse index sequence creation");
    }

    return tap.status ();
}