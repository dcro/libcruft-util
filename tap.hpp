/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "except.hpp"
#include "format.hpp"
#include "maths.hpp"

#include <iosfwd>
#include <functional>

#include <cmath>

namespace cruft::TAP {
    /// A simple TAP (Test Anything Protocol) test case output
    class logger {
    public:
        enum {
            PASS,
            FAIL,
            SKIP,
            TODO
        };

        //---------------------------------------------------------------------
        logger ();

        logger (std::ostream&);
        ~logger ();

        // NOTE: explicitly disable copy constructors and all assignment
        // operators so that we don't accidentally overwrite the contained
        // status code in any instance. This value is required in all
        // instances so that we can return the correct status code from a test
        // binary.
        logger (logger&&) = default;
        logger (logger const &) = delete;
        logger& operator= (logger const &) = delete;
        logger& operator= (logger &&) = delete;


        //---------------------------------------------------------------------
        template <typename ...Args, size_t N>
        bool
        expect (const bool test, const char (&fmt)[N], Args&&... args)
        {
            m_output << (test ? "ok " : "not ok ") << ++m_size
                     << " - "
                     << format::printf (fmt) (std::forward<Args> (args)...)
                     << std::endl;

            if (!test)
                m_status = EXIT_FAILURE;
            return test;
        }


        //---------------------------------------------------------------------
        template <typename ...Args, size_t N>
        decltype(auto)
        expect (const std::function<bool(Args...)> &test, Args&&...args, const char (&fmt)[N])
        {
            try {
                return expect (test (std::forward<Args> (args)...), fmt);
            } catch (...) {
                return expect (false, fmt);
            }
        }


        ///////////////////////////////////////////////////////////////////////
        template <typename ValueT, size_t N, typename ...ArgsT>
        decltype(auto)
        expect_valid (ValueT &&value, char const (&fmt)[N], ArgsT &&...args)
        {
            return expect (
                debug::is_valid (
                    std::forward<ValueT> (value)
                ),
                fmt,
                std::forward<ArgsT> (args)...
            );
        }


        ///////////////////////////////////////////////////////////////////////
        template <typename ValueA, typename ValueB, std::size_t N, typename ...Args>
        decltype(auto)
        expect_mod (ValueA &&a, ValueB &&b, const char (&fmt)[N], Args &&...args)
        {
            return expect (a % b == 0, fmt, std::forward<Args> (args)...);
        }


        ///////////////////////////////////////////////////////////////////////
        template <typename T, typename U, typename ...Args, size_t N>
        decltype(auto)
        expect_eq (const T &a, const U &b, const char (&fmt)[N], Args&&...args)
        {
#if 1
            return expect (almost_equal (a, b), fmt, std::forward<Args> (args)...);
#else
            if (almost_equal (a, b))
                return expect (true, fmt, std::forward<Args> (args)...);
            else
                return expect (false, "%! # %! != %!", format::printf (fmt)(std::forward<Args> (args)...), a, b);
#endif
        }


        //---------------------------------------------------------------------
        template <typename T, typename U, typename ...Args, size_t N>
        decltype(auto)
        expect_neq (const T &a, const U &b, const char (&fmt)[N], Args&&...args)
        {
            return expect (!almost_equal (a, b), fmt, std::forward<Args> (args)...);
        }


        ///////////////////////////////////////////////////////////////////////
        template <typename ValueA, typename ValueB, typename ...Args, size_t N>
        decltype(auto)
        expect_gt (const ValueA &a, const ValueB &b, const char (&fmt)[N], Args&&...args)
        {
            return expect (a > b, fmt, std::forward<Args> (args)...);
        }


        //---------------------------------------------------------------------
        template <typename T, typename U, typename ...Args, size_t N>
        decltype(auto)
        expect_ge (const T &a, const U &b, const char (&fmt)[N], Args&&...args)
        {
            return expect (a >= b, fmt, std::forward<Args> (args)...);
        }


        //---------------------------------------------------------------------
        template <typename T, typename U, typename ...Args, size_t N>
        decltype(auto)
        expect_lt (const T &a, const U &b, const char (&fmt)[N], Args&&...args)
        {
            return expect (a < b, fmt, std::forward<Args> (args)...);
        }


        //---------------------------------------------------------------------
        template <typename T, typename U, typename ...Args, size_t N>
        decltype(auto)
        expect_le (const T &a, const U &b, const char (&fmt)[N], Args&&...args)
        {
            return expect (a <= b, fmt, std::forward<Args> (args)...);
        }


        ///////////////////////////////////////////////////////////////////////
        template <typename T, typename ...Args, size_t N>
        decltype(auto)
        expect_nan (const T &t, const char (&fmt)[N], Args&&...args)
        {
            return expect (std::isnan (t), fmt, std::forward<Args> (args)...);
        }


        ///////////////////////////////////////////////////////////////////////
        template <typename T, typename ...Args, size_t N>
        decltype(auto)
        expect_nothrow (T &&t, const char (&fmt)[N], Args&&...args)
        {
            bool success = true;

            try {
                t ();
            } catch (...) {
                success = false;
            }

            return expect (success, fmt, std::forward<Args> (args)...);
        }


        //---------------------------------------------------------------------
        template <typename E, typename T, typename ...Args, size_t N>
        decltype(auto)
        expect_throw (T &&t, const char (&fmt)[N], Args&&...args)
        {
            bool success = false;

            try {
                t ();
            } catch (const E&) {
                success = true;
            } catch (...) {
                success = false;
            }

            return expect (success, fmt, std::forward<Args> (args)...);
        }


        ///////////////////////////////////////////////////////////////////////
        template <size_t N, typename ...Args>
        decltype(auto)
        fail (const char (&fmt)[N], Args &&...args)
        {
            return expect (false, fmt, std::forward<Args> (args)...);
        }


        //---------------------------------------------------------------------
        void skip (const std::string &msg);
        void todo (const std::string &msg);
        void noop (void);


        ///////////////////////////////////////////////////////////////////////
        int status [[nodiscard]] (void) const;


        /// Invoke a FunctionT with a TAP::logger as the first argument, and
        /// the remainder forwarded from the provided parameter pack.
        ///
        /// If an exception escapes from the FunctionT the logger will be
        /// marked as failed, using a message that attempts to use the raised
        /// exception value (else a static generic message if the type isn't
        /// easily discoverable).
        ///
        /// Returns the status of the logger provided to the FunctionT.
        template <typename FunctionT, typename ...Args>
        static auto
        run [[nodiscard]] (FunctionT &&function, Args&&...args) noexcept
        {
            logger tap;
            try {
                function (tap, args...);
                return tap.status ();
            } catch (std::exception const &err) {
                tap.fail ("no exceptions: %s", err.what ());
            } catch (cruft::error const &err) {
                tap.fail ("no exceptions: %s", err);
            } catch (...) {
                tap.fail ("no exceptions");
            }

            return EXIT_FAILURE;
        }


    private:
#if !defined(NDEBUG)
        mutable int m_reported = -1;
#endif

        std::ostream &m_output;
        int m_status;
        size_t m_size;
    };
}
