/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019, Danny Robson <danny@nerdcruft.net>
 */

#include "../log.hpp"

#include <iostream>
#include <cstdlib>


enum {
    ARG_SELF,
    ARG_LEVEL,
    ARG_FORMAT,

    MIN_ARGS,
};


int main (int const argc, char const **argv)
{
    if (argc < MIN_ARGS) {
        std::cerr << "usage: " << argv[ARG_SELF] << " <level> <format> [arg]...\n";
        return EXIT_FAILURE;
    }

    auto const level  = cruft::log::to_level (argv[ARG_LEVEL]);
    auto const format = argv[ARG_FORMAT];

    auto const count  = argc - 3;
    switch (count) {
    case 0: cruft::log::write (level, format); break;
    case 1: cruft::log::write (level, format, argv[MIN_ARGS + 0]); break;
    case 2: cruft::log::write (level, format, argv[MIN_ARGS + 0], argv[MIN_ARGS + 1]); break;
    }

    return EXIT_SUCCESS;
}