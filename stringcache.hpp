/******************************************************************************
                              _       _
                             | |     | |
                             | | ___ | |__   ___
                             | |/ _ \| '_ \ / _ \
                             | | (_) | |_) | (_) |
                             |_|\___/|_.__/ \___/
    Copyright:
        Danny Robson, 2020
 *****************************************************************************/

#pragma once

#include "./std.hpp"

#include <string>
#include <string_view>
#include <vector>


namespace cruft {
    /// Stores strings in a single block of memory where they can be indexed
    /// by an integral id, and compacted to save space as required.
    class stringcache {
        using value_type = i16;

    public:
        enum id_t : value_type {};

        std::string_view operator[] (id_t);

        id_t add (std::string_view);
        id_t add (std::string const &);
        id_t add (char const*);

    private:
        struct slot_t {
            value_type start;
            value_type length;
        };

        std::vector<slot_t> m_values;
        std::vector<char> m_store;
    };
}
