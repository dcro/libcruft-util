/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright:
 *      2012-2019, Danny Robson <danny@nerdcruft.net>
 */

#include "backtrace.hpp"

#include "win32/error.hpp"
#include "win32/handle.hpp"
#include "win32/windows.hpp"
#include "debug.hpp"
#include "except.hpp"

#include <cstdlib>
#include <dbghelp.h>

using cruft::backtrace;

static constexpr std::size_t DEFAULT_DEPTH = 16;


///////////////////////////////////////////////////////////////////////////////
backtrace::backtrace (void)
{
    m_frames.resize (DEFAULT_DEPTH);

    auto process = GetCurrentProcess();
    if (!SymInitialize (process, NULL, TRUE))
        cruft::win32::error::throw_code ();

    while (true) {
        auto const res = CaptureStackBackTrace (1, m_frames.size (), m_frames.data (), NULL);
        if (res < m_frames.size ()) {
            m_frames.resize (res);
            break;
        }

        m_frames.resize (m_frames.size () * 2);
    }
}


///////////////////////////////////////////////////////////////////////////////
std::ostream&
debug::operator <<(std::ostream &os, ::cruft::backtrace const &rhs)
{
    static auto process = GetCurrentProcess ();
    CHECK (ready);

    static constexpr size_t MAX_LENGTH = 255;
    struct {
        SYMBOL_INFO info;
        char name[MAX_LENGTH + 1];
    } symbol;

    symbol.info.MaxNameLen   = MAX_LENGTH;
    symbol.info.SizeOfStruct = sizeof (SYMBOL_INFO);

    os << "[ ";
    for (void *frame: rhs.frames ()) {
        symbol.name[0] = '\0';
        SymFromAddr (process, (DWORD64)frame, 0, &symbol.info);
        symbol.name[MAX_LENGTH] = '\0';

        os << "{ addr: "  << frame
           << ", name: '" << symbol.name '\''
           << " }, ";
    }

    os << " ]";
    return os;
}
