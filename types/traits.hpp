/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2012-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <memory>
#include <tuple>
#include <type_traits>


///////////////////////////////////////////////////////////////////////////////
/// applies a series of type-modifiers to a provided type.
///
/// modifiers are applied in the order they are provided.
///
/// use without any modifiers results in the identity modifier.
namespace cruft::types {
    //-------------------------------------------------------------------------
    template <typename TypeT, template <typename> class ...ModifierT>
    struct compose;


    //-------------------------------------------------------------------------
    template <typename TypeT>
    struct compose<TypeT> { using type = TypeT; };


    //-------------------------------------------------------------------------
    template <
        typename TypeT,
        template <typename> class HeadT,
        template <typename> class ...TailT
    >
    struct compose<TypeT, HeadT, TailT...> : public compose<
        typename HeadT<TypeT>::type,
        TailT...
    > { };


    //-------------------------------------------------------------------------
    template <typename T, template <typename> class ...Args>
    using chain_t = typename compose<T, Args...>::type;
}


namespace cruft::types {
    ///////////////////////////////////////////////////////////////////////////
    /// CXX20: An early implementation of std::identity_t and related
    /// structures; useful for disabling template argument deduction, and
    /// higher order template functions.
    template <typename T>
    struct identity { using type = T; };


    //-------------------------------------------------------------------------
    template <typename T>
    using identity_t = typename identity<T>::type;
}



///////////////////////////////////////////////////////////////////////////////
template <typename T> struct is_dereferencable     : std::false_type { };
template <typename T> struct is_dereferencable<T*> : std::true_type  { };
template <typename T> struct is_dereferencable<std::unique_ptr<T>> : std::true_type { };
template <typename T> struct is_dereferencable<std::shared_ptr<T>> : std::true_type { };
template <typename T> struct is_dereferencable<std::weak_ptr<T>>   : std::true_type { };


///////////////////////////////////////////////////////////////////////////////
template <typename T> struct dereferenced_type {
    typedef typename std::enable_if<
        std::is_pointer<T>::value,
        std::remove_pointer<T>
    >::type type;
};


//-----------------------------------------------------------------------------
template <typename T> struct dereferenced_type<std::unique_ptr<T>> { typedef T type; };
template <typename T> struct dereferenced_type<std::shared_ptr<T>> { typedef T type; };
template <typename T> struct dereferenced_type<std::weak_ptr<T>>   { typedef T type; };


///////////////////////////////////////////////////////////////////////////////
/// find the unsigned version of a type if one exists
template <typename T>
struct try_unsigned
{
    typedef typename std::make_unsigned<T>::type type;
};


//-----------------------------------------------------------------------------
template <> struct try_unsigned<double> { typedef double type; };
template <> struct try_unsigned<float > { typedef float  type; };


///////////////////////////////////////////////////////////////////////////////
/// find the signed version of a type if one exists
template <typename T>
struct try_signed
{
    typedef typename std::make_signed<T>::type type;
};


//-----------------------------------------------------------------------------
template <> struct try_signed<double> { typedef double type; };
template <> struct try_signed<float > { typedef float  type; };


//-----------------------------------------------------------------------------
template <typename ValueT>
using try_signed_t = typename try_signed<ValueT>::type;


///////////////////////////////////////////////////////////////////////////////
namespace cruft {
    template <typename ValueT, typename = void>
    struct try_underlying { using type = ValueT; };


    template <typename ValueT>
    struct try_underlying<
        ValueT,
        std::void_t<std::underlying_type_t<ValueT>>
    > { using type = std::underlying_type<ValueT>; };
}


///////////////////////////////////////////////////////////////////////////////
/// checks if a type can be converted in all cases without modification
template <typename T, typename U> struct is_lossless_cast : std::enable_if<
    std::is_integral<T>::value &&
    std::is_integral<U>::value &&
    std::is_signed<T>::value == std::is_signed<U>::value &&
    sizeof (T) <= sizeof (U),

    std::true_type
>::value { };


///////////////////////////////////////////////////////////////////////////////
template <typename T> struct remove_restrict              { using type = T;  };
template <typename T> struct remove_restrict<T *restrict> { using type = T*; };

template <typename T>
using remove_restrict_t = typename remove_restrict<T>::type;


///////////////////////////////////////////////////////////////////////////////
/// removes the noexcept type specifier from invokable types
namespace detail {
    template <typename T>
    struct remove_noexcept
    { using type = T; };


    //-------------------------------------------------------------------------
    template <typename ResultT, typename ...Args>
    struct remove_noexcept<ResultT(&)(Args...) noexcept> {
        using type = ResultT(&)(Args...);
    };


    //-------------------------------------------------------------------------
    template <typename ResultT, typename ...Args>
    struct remove_noexcept<ResultT(*const)(Args...) noexcept> {
        using type = ResultT(*const)(Args...);
    };


    //-------------------------------------------------------------------------
    template <typename ResultT, typename ...Args>
    struct remove_noexcept<ResultT(*)(Args...) noexcept> {
        using type = ResultT(*)(Args...);
    };


    //-------------------------------------------------------------------------
    template <typename ClassT, typename ResultT, typename ...Args>
    struct remove_noexcept<ResultT(ClassT::*)(Args...) noexcept> {
        using type = ResultT(ClassT::*)(Args...);
    };


    //-------------------------------------------------------------------------
    template <typename ClassT, typename ResultT, typename ...Args>
    struct remove_noexcept<ResultT(ClassT::*)(Args...) const noexcept> {
        using type = ResultT(ClassT::*)(Args...) const;
    };
};


//-----------------------------------------------------------------------------
template <typename T>
struct remove_noexcept : public detail::remove_noexcept<T> { };

//-----------------------------------------------------------------------------
template <typename T>
using remove_noexcept_t = typename remove_noexcept<T>::type;


///////////////////////////////////////////////////////////////////////////////
/// removes any `const' qualifier from the supplied member function
template <typename FuncT>
struct remove_member_const {
    using type = FuncT;
};


//-----------------------------------------------------------------------------
template <typename ClassT, typename ReturnT, typename ...Args>
struct remove_member_const<ReturnT(ClassT::*const)(Args...) const> {
    using type = ReturnT(ClassT::*const)(Args...);
};


//-----------------------------------------------------------------------------
template <typename ClassT, typename ReturnT, typename ...Args>
struct remove_member_const<ReturnT(ClassT::*)(Args...) const> {
    using type = ReturnT(ClassT::*)(Args...);
};


///////////////////////////////////////////////////////////////////////////////
/// type traits class for querying invokable type return values and argument
/// types.
///
/// if the type is invokable the alias `return_type' will be defined for the
/// return type, and the alias tuple `argument_types' will be defined for the
/// arguments;
namespace detail {
    template <typename T>
    struct func_traits { };


    //-------------------------------------------------------------------------
    template <typename ClassT, typename ResultT, typename ...Args>
    struct func_traits<ResultT(ClassT::*)(Args...)> {
        using return_type = ResultT;
        using argument_types = std::tuple<Args...>;
    };


    //-------------------------------------------------------------------------
    template <typename ResultT, typename ...Args>
    struct func_traits<ResultT(*)(Args...)>  {
        using return_type = ResultT;
        using argument_types = std::tuple<Args...>;
    };


    //-------------------------------------------------------------------------
    template <typename ResultT, typename ...Args>
    struct func_traits<ResultT(&)(Args...)>  {
        using return_type = ResultT;
        using argument_types = std::tuple<Args...>;
    };
};


//-----------------------------------------------------------------------------
template <typename T>
struct func_traits : public ::detail::func_traits<
    // we apply as many transforms as possible before palming it off to the
    // detail class so that we don't have to write as many distinct cases.
    ::cruft::types::chain_t<T,
        std::remove_cv,
        std::decay,
        remove_member_const,
        remove_noexcept
    >
> {
    // we may as well record the underlying type here. it might prove useful
    // to someone.
    using type = T;
};


///////////////////////////////////////////////////////////////////////////////
template <typename FunctionT>
struct function_argument_count : public std::integral_constant<
    size_t,
    std::tuple_size_v<
        typename func_traits<FunctionT>::argument_types
    >
> { };


//-----------------------------------------------------------------------------
template <typename FunctionT>
constexpr auto function_argument_count_v = function_argument_count<FunctionT>::value;



///////////////////////////////////////////////////////////////////////////////
template <std::size_t N, typename FuncT>
struct nth_argument : std::tuple_element<
    N,
    typename func_traits<FuncT>::argument_types
> { };


template <std::size_t N, typename FuncT>
using nth_argument_t = typename nth_argument<N, FuncT>::type;


///////////////////////////////////////////////////////////////////////////////
/// A trait that tests if a type `T` is a container type.
///
/// That is, a type that has:
///  *  container typedefs such as value_type, reference, iterator, etc.
///  *  viable std::begin/std::end overloads
///
/// Defaults to false.
template <typename T, typename = std::void_t<>>
struct is_container : public std::false_type {};


///----------------------------------------------------------------------------
template <typename T>
struct is_container<
    T,
    std::void_t<
        typename T::value_type,
        typename T::reference,
        typename T::const_reference,
        typename T::iterator,
        typename T::const_iterator,
        typename T::difference_type,
        typename T::size_type,
        decltype(std::end   (std::declval<T> ())),
        decltype(std::begin (std::declval<T> ()))
    >
> : public std::true_type {};


//-----------------------------------------------------------------------------
template <typename T>
constexpr auto is_container_v = is_container<T>::value;


///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <array>
#include <vector>

template <typename T>
struct is_contiguous : public std::false_type {};

template <typename CharT, typename Traits, typename Allocator>
struct is_contiguous<std::basic_string<CharT, Traits, Allocator>> : public std::true_type {};

template <typename T, std::size_t N>
struct is_contiguous<std::array<T,N>>: public std::true_type {};

template <typename T, typename Allocator>
struct is_contiguous<std::vector<T,Allocator>>: public std::true_type {};

template <typename T>
constexpr auto is_contiguous_v = is_contiguous<T>::value;


namespace cruft {
    ///////////////////////////////////////////////////////////////////////////////
    /// stores the type of '::value_type' for a given type, or the type itself if
    /// it does not have such a type.
    template <typename ValueT, typename = std::void_t<>>
    struct inner_type {
        using type = ValueT;
    };


    //-----------------------------------------------------------------------------
    template <typename ValueT>
    struct inner_type<
        ValueT,
        std::void_t<typename ValueT::value_type>
    > {
        using type = typename ValueT::value_type;
    };


    //-----------------------------------------------------------------------------
    template <typename ValueT>
    using inner_type_t = typename inner_type<ValueT>::type;
}


///////////////////////////////////////////////////////////////////////////////
template <
    template <typename...> class TemplateT,
    typename QueryT
>
struct is_same_template_template : public std::false_type {};


//-----------------------------------------------------------------------------
template <
    template <typename...> class TemplateT,
    template <typename...> class QueryT,
    typename ...Args
> struct is_same_template_template<
    TemplateT,
    QueryT<Args...>
> : public std::conditional_t<
    std::is_same_v<
        TemplateT<Args...>,
        QueryT<Args...>
    >,
    std::true_type,
    std::false_type
> { };


//-----------------------------------------------------------------------------
template <template <typename...> class TemplateT, typename QueryT>
constexpr auto is_same_template_template_v = is_same_template_template<TemplateT,QueryT>::value;


namespace cruft::traits {
    /// combines an abitrary sequence of type traits into one result
    template <
        typename ValueT,
        template <typename> class ...Traits
    > struct all : public std::integral_constant<
        bool,
        (Traits<ValueT>::value && ...)
    > { };
};



///////////////////////////////////////////////////////////////////////////////
namespace cruft {
    /// tests if the type is a signed integer type
    template <typename ValueT>
    struct is_signed_integral : public traits::all<
        ValueT, std::is_signed, std::is_integral
    > { };


    template <typename ValueT>
    constexpr auto is_signed_integral_v = is_signed_integral<ValueT>::value;


    /// tests if the type is an unsigned integer type
    template <typename ValueT>
    struct is_unsigned_integral : public traits::all<
        ValueT, std::is_unsigned, std::is_integral
    > { };

    template <typename ValueT>
    constexpr auto is_unsigned_integral_v = is_unsigned_integral<ValueT>::value;


    /// tests if the type has the same basic fundamental type.
    ///
    /// ie, both float, signed, unsigned, or char.
    template <typename ValueA, typename ValueB>
    struct is_same_basic_type {
        static constexpr bool value =
            (std::is_floating_point_v<ValueA> && std::is_floating_point_v<ValueB>) ||
            (is_unsigned_integral_v<ValueA>   && is_unsigned_integral_v<ValueB>) ||
            (is_signed_integral_v<ValueA>     && is_signed_integral_v<ValueB>) ||
            (std::is_same_v<char,ValueA>      && std::is_same_v<char,ValueB>);
    };


    template <typename ValueA, typename ValueB>
    constexpr auto is_same_basic_type_v = is_same_basic_type<ValueA,ValueB>::value;


    /// Tests if an object is 'tuple-like'; ie, it responds to std::get,
    /// std::tuple_size, and std::tuple_element.
    template <typename, typename = void>
    struct is_tuple_like : public std::false_type {};


    template <typename T>
    struct is_tuple_like<
        T,
        std::void_t<
            decltype (std::get<0> (std::declval<T> ())),
            decltype (std::tuple_size_v<T>),
            std::tuple_element_t<0,T>
        >
    > : public std::true_type {};


    template <typename T>
    constexpr auto is_tuple_like_v = is_tuple_like<T>::value;


    ///////////////////////////////////////////////////////////////////////////
    /// A trait class that models the concept LessThanComparable.
    template <
        typename ValueA,
        typename ValueB,
        typename = std::void_t<>
    > struct is_less_than_comparable : public std::false_type {};


    //-------------------------------------------------------------------------
    template <
        typename ValueA,
        typename ValueB
    > struct is_less_than_comparable<
        ValueA,
        ValueB,
        std::void_t<
            decltype(std::declval<ValueA> () < std::declval<ValueB> ())
        >
    > : public std::true_type {};


    //-------------------------------------------------------------------------
    template <typename ValueA, typename ValueB>
    constexpr auto is_less_than_comparable_v = is_less_than_comparable<ValueA,ValueB>::value;


    ///////////////////////////////////////////////////////////////////////////
    /// Tests if a type is orderable; ie, if it supports the '<=' operator
    ///
    /// We use void_t to detect the presence of an appropriate operator<=
    template <
        typename ValueA,
        typename ValueB,
        typename = std::void_t<>
    >
    struct is_lteq_orderable : public std::false_type {};


    //-------------------------------------------------------------------------
    template <typename ValueA, typename ValueB>
    struct is_lteq_orderable<
        ValueA,
        ValueB,
        std::void_t<
            decltype (
                std::declval<ValueA> () <= std::declval<ValueB> ()
            )
        >
    > : std::true_type {};


    //-----------------------------------------------------------------------------------
    template <typename ValueA, typename ValueB>
    constexpr auto is_lteq_orderable_v = is_lteq_orderable<ValueA,ValueB>::value;
};
