/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_UTIL_FIXUP_EXPERIMENTAL_FILESYSTEM_HPP
#define CRUFT_UTIL_FIXUP_EXPERIMENTAL_FILESYSTEM_HPP

#include <string>

///////////////////////////////////////////////////////////////////////////////
namespace std::filesystem {
    class path {
    public:
        using value_type = char;
        using string_type = std::basic_string<value_type>;
        
        static constexpr value_type preferred_separator = '/';

        path ();
        explicit path (const path&);

        template <class Source>
        path (const Source &s):
            m_path (s)
        { ; }

        template <class InputT>
        path (InputT first, InputT last):
            m_path (first, last)
        { ; }

        std::string string (void) const;

        const string_type& native (void) const;
        const value_type* c_str (void) const;

        path filename (void) const;
        path stem (void) const;

        path& operator/= (const path&);

    private:
        string_type m_path;
    };

    path operator/ (const path&, const path&);

    bool operator== (const path&, const path&);

    //bool is_directory (file_status);
    bool is_directory (const path&);
    //bool is_directory (const path&, error_code&);

    template <class CharT, class Traits>
    std::basic_ostream<CharT,Traits>&
    operator<< (std::basic_ostream<CharT,Traits> &os, const path &p)
    { return os << p.native (); }
}


#endif

