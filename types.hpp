/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "types/description.hpp"

#include <cstdint>
#include <cstdlib>
#include <memory>
#include <stdexcept>
#include <tuple>


///////////////////////////////////////////////////////////////////////////////
// analogue of std::data for use until we get proper c++17 support
namespace cruft {
    template <typename T>
    auto
    data (T &t)
    { return t.data (); }

    //-------------------------------------------------------------------------
    template <typename T>
    auto
    data (const T &t)
    { return t.data (); }


    //-------------------------------------------------------------------------
    template <typename T, size_t N>
    constexpr T*
    data (T (&t)[N])
    { return &t[0]; }


    //-------------------------------------------------------------------------
    template <typename T, size_t N>
    constexpr const T*
    data (const T (&t)[N])
    { return &t[0]; }
}


///////////////////////////////////////////////////////////////////////////////
/// returns the first argument from a parameter pack which can evaluate to true.
template <class T>
T
first (T a)
{
    if (a)
        return a;

    throw std::logic_error ("no valid object");
}

//-----------------------------------------------------------------------------
template <class T, class ...Args>
T
first (T a, Args&& ...b)
{
    if (a)
        return a;

    return first (std::forward<Args>(b)...);
}


namespace cruft {
    ///////////////////////////////////////////////////////////////////////////
    /// A tag type that is used when type_tag dispatching descriptions that
    /// claim to be enums. The underlying type can be recovered from the 'type'
    /// typedef of this class.
    template <typename UnderlyingT>
    struct unknown_enum_tag {
        using type = UnderlyingT;
    };


    //-------------------------------------------------------------------------
    template <typename ValueT>
    struct is_unknown_enum_tag :
    public std::false_type {};


    //-------------------------------------------------------------------------
    template <
        typename UnderlyingT
    > struct is_unknown_enum_tag<
        unknown_enum_tag<UnderlyingT>
    >: public std::true_type {};


    //-------------------------------------------------------------------------
    template <typename ValueT>
    constexpr auto is_unknown_enum_tag_v = is_unknown_enum_tag<ValueT>::value;


    ///////////////////////////////////////////////////////////////////////////
    /// represents a type as a POD struct (but is statically recoverable via
    /// the 'type' member).
    template <typename T>
    struct type_tag
    {
        typedef T type;
    };


    //-------------------------------------------------------------------------
    template <typename UnderlyingT>
    struct type_tag<unknown_enum_tag<UnderlyingT>> {
        cruft::types::description desc;
    };


    ///------------------------------------------------------------------------
    /// count the number of parameters we are given. useful for counting
    /// arguments in variadic macros (ie, sizeof... (__VA_ARGS__))
    template <typename ...T>
    constexpr size_t
    param_count (const T... t)
    {
        // prevent unused variable warnings by never forwarding recursively
        // ideally we'd use void casting, but it doesn't work for parameter
        // packs
        if (false)
            return param_count (t...);

        return sizeof... (t);
    }



    ///////////////////////////////////////////////////////////////////////////
    /// A trait that tests for the presence of a 'native' member function.
    ///
    /// Not terrifically useful by itself, but it is use reasonably frequently
    /// in various library wrappers that we maintain (to return the underlying
    /// native objects).
    template <typename ValueT, typename = std::void_t<>>
    struct has_native : public std::false_type
    {};


    //-------------------------------------------------------------------------
    template <
        typename ValueT
    > struct has_native<
        ValueT,
        std::void_t<
            decltype(std::declval<ValueT> ().native ())
        >
    > : public std::true_type {};


    //-------------------------------------------------------------------------
    template <typename ValueT>
    constexpr auto has_native_v = has_native<ValueT>::value;


    ///////////////////////////////////////////////////////////////////////////
    /// Unconditionally ignore the first argument, and store the second as `type`.
    ///
    /// This aids converting parameter packs types. eg, converting all types
    /// to strings by using an expression of the form:
    ///     `convert_t<ValueT, std::string>...`
    template <typename IgnoredT, typename ResultT>
    struct convert {
        using type = ResultT;
    };


    template <typename IgnoredT, typename ResultT>
    using convert_t = typename convert<IgnoredT, ResultT>::type;
}
