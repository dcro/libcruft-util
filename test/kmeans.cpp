#include "tap.hpp"

#include "kmeans.hpp"

#include "point.hpp"


///////////////////////////////////////////////////////////////////////////////
int
main ()
{
    cruft::TAP::logger tap;

    // create one point and check it 'converges' to this one point
    {
        const std::array<cruft::point3f,1> p { {{1,2,3}} };
        std::array<cruft::point3f,1> q;

        cruft::kmeans (cruft::view{p}, cruft::view{q});
        tap.expect_eq (p, q, "single point, single k");
    }

    // create two vectors, check if the mean converges to their average
    {
        const std::array<cruft::vector3f,2> p {{
            {1}, {2}
        }};
        std::array<cruft::vector3f,1> q;

        cruft::kmeans (cruft::view{p}, cruft::view{q});
        tap.expect_eq (q[0], (p[0]+p[1])/2, "two point, single k");
    }

    return tap.status ();
}