/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2020, Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../bitwise.hpp"

#include <utility>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::hash::adapter {
    template <typename HashT>
    class combine {
    public:
        using digest_t = typename HashT::digest_t;
        using seed_t   = typename HashT::seed_t;

        template <typename ...ArgsT>
        combine (ArgsT &&...args)
            : m_inner (std::forward<ArgsT> (args)...)
        { ; }


        decltype(auto)
        operator() (cruft::view<u08 const*> data)
        {
            return m_inner (data);
        }


        template <typename ...TailT>
        decltype(auto) operator() (
            cruft::view<u08 const*> data,
            TailT &&...tail
        ) {
            return (*this) (data) ^ cruft::rotatel ((*this) (tail...), 6);
        }

    private:
        HashT m_inner;
    };
}