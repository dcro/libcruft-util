/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_ALLOC_FWD_HPP
#define __UTIL_ALLOC_FWD_HPP


namespace cruft::alloc {
    namespace raw {
        class affix;
        class fallback;
        class linear;
        class malloc;
        class null;
        class stack;

        class dynamic;

        namespace aligned {
            template <typename AllocT> class foreign;
            template <typename AllocT> class direct;
        }
    }


    template <typename T> class arena;
    template <typename B, typename T> class allocator;
}

#endif
