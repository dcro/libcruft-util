/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2019 Danny Robson <danny@nerdcruft.net>
 */

#include "./debugger.hpp"

#include "../log.hpp"

#include <string>
#include <cstdlib>


///////////////////////////////////////////////////////////////////////////////
static void
debug_wait [[gnu::constructor]] (void)
{
    if (auto val = getenv ("DEBUG_WAIT")) {
        LOG_NOTICE ("awaiting debugger");

        if (std::string ("0") != val)
            await_debugger ();
    }
}
