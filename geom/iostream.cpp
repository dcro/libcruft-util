/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#include "iostream.hpp"

#include "frustum.hpp"
#include "plane.hpp"
#include "segment.hpp"
#include "sphere.hpp"

#include "../coord/iostream.hpp"
#include "../iterator/infix.hpp"


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
std::ostream&
cruft::geom::operator<< (std::ostream &os, cruft::geom::sphere<S,T> s)
{
    return os << "sphere(" << s.centre << ',' << s.radius << ')';
}


//-----------------------------------------------------------------------------
template std::ostream& cruft::geom::operator<< (std::ostream&, cruft::geom::sphere<2,float>);
template std::ostream& cruft::geom::operator<< (std::ostream&, cruft::geom::sphere<3,float>);


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
std::ostream&
cruft::geom::operator<< (std::ostream &os, cruft::geom::segment<S,T> const &val)
{
    return os << "[ " << val.a << ", " << val.b << " ]";
}


template std::ostream& cruft::geom::operator<< (std::ostream&, cruft::geom::segment2i const&);
template std::ostream& cruft::geom::operator<< (std::ostream&, cruft::geom::segment2f const&);


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
std::ostream&
cruft::geom::operator<< (std::ostream &os, plane<S,T> val)
{
    return os << val.coefficients;
}


//-----------------------------------------------------------------------------
template std::ostream& cruft::geom::operator<< (std::ostream&, plane<2,float>);
template std::ostream& cruft::geom::operator<< (std::ostream&, plane<3,float>);


///////////////////////////////////////////////////////////////////////////////
template <typename T>
std::ostream&
cruft::geom::operator<< (std::ostream &os, frustum<T> val)
{
    return os << "[ " << iterator::make_infix (val.planes) << " ]";
}


//-----------------------------------------------------------------------------
template std::ostream& cruft::geom::operator<< (std::ostream&, frustum<float>);
