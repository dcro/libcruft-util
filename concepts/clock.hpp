/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2020, Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../concepts.hpp"

#include <chrono>
#include <ratio>

#include <cstdint>


namespace cruft::concepts {
    namespace detail {
        template <typename ValueT>
        struct is_ratio : public std::false_type {};


        template <std::intmax_t Num, std::intmax_t Denom>
        struct is_ratio<
            std::ratio<Num, Denom>
        > : public std::true_type { };
    };

    template <typename T>
    concept clock =
        numeric<typename T::rep> &&
        detail::is_ratio<typename T::period>::value &&
        same_as<typename T::duration, std::chrono::duration<typename T::rep, typename T::period>> &&
        //same_as<typename T::time_point, std::chrono::time_point<typename T::rep, typename T::period>> &&
        same_as<decltype(T::is_steady), bool const> &&
        requires (T t)
    {
        typename T::rep;
        typename T::period;
        typename T::time_point;
        typename T::duration;

        { T::is_steady  } -> convertible_to<bool>;
        { t.now () } -> same_as<typename T::time_point>;
    };

    namespace detail {
        template <typename T>
        concept trivial_clock_inner_type =
            equality_comparable<T> &&
            is_less_than_comparable_v<T> &&
            default_constructible<T> &&
            copy_constructible<T> &&
            copy_assignable<T> &&
            destructible<T> &&
            numeric<T> &&
            swappable<T>;

        template <typename T>
        concept trivial_clock =
            trivial_clock_inner_type<typename T::rep> &&
            trivial_clock_inner_type<typename T::duration> &&
            trivial_clock_inner_type<typename T::time_point> &&
            noexcept (std::declval<T> ().now());
    }

    template <typename T>
    concept trivial_clock =
        detail::trivial_clock<T> &&
        detail::trivial_clock<typename T::time_point::clock>;
}