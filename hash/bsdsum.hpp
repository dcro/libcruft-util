/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011-2018 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_UTIL_HASH_BSDSUM_HPP
#define CRUFT_UTIL_HASH_BSDSUM_HPP

#include "../view.hpp"

#include <cstdint>
#include <cstdlib>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::hash {
    class bsdsum {
    public:
        using digest_t = uint16_t;

        digest_t operator() (cruft::view<const uint8_t*>) const noexcept;
    };
}

#endif
