/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#include "ops.hpp"

#include "aabb.hpp"
#include "../region.hpp"


///////////////////////////////////////////////////////////////////////////////
// Bounds calculation for a region is effectively just a type conversion
template <>
cruft::geom::aabb<2,float>
cruft::geom::bounds (region<2,float> val)
{
    return aabb2f {
        val.base (),
        val.away ()
    };
}
