/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "windows.hpp"

#include "../posix/fd.hpp"

namespace cruft::win32 {
    struct handle {
        using native_type = HANDLE;

        handle ();
        explicit handle (posix::fd&&);
        explicit handle (HANDLE&&);

        handle (handle&&) noexcept;
        handle& operator= (handle&&) noexcept;

        handle (const handle&) = delete;
        handle& operator= (handle const&) = delete;

        ~handle ();

        operator HANDLE& (void) &; 
        HANDLE& native (void) &;
        const HANDLE& native (void) const &;

        void reset (HANDLE);
        void reset (handle&&);

        static handle current_process (void);

    private:
        HANDLE m_native;
    };
}
