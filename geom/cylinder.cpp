/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include "cylinder.hpp"

using cruft::geom::cylinder;


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
bool
cylinder<S,T>::includes (cruft::point<S,T> p_) const
{
    auto p10 = p1 - p0;
    auto p_0 = p_ - p0;

    auto l = dot (p10, p_0);
    if (l < 0 || l > norm2 (p10))
        return false;

    auto d = dot (p10, p10) - l * l * norm2 (p10);
    if (d > radius * radius)
        return false;

    return true; // d
}


///////////////////////////////////////////////////////////////////////////////
template struct cruft::geom::cylinder<2,float>;
template struct cruft::geom::cylinder<3,float>;
