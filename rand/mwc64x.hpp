/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../std.hpp"

#include <cstdint>
#include <limits>


namespace cruft::rand {
    // multiply-with-carry style generator suitable for rapid seeking and
    // GPU generation.
    //
    // as with all such style generators, the seed value is very important.
    // don't allow either half of the uint64_t to be zero.
    struct mwc64x {
    public:
        using result_type = u32;
        using seed_type   = u64;

        explicit mwc64x (seed_type);

        result_type operator() (void);

        static constexpr auto max (void) noexcept { return std::numeric_limits<result_type>::max (); }
        static constexpr auto min (void) noexcept { return std::numeric_limits<result_type>::min (); }

    private:
        uint64_t m_state;
    };
}
