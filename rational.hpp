/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2014-2015 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_RATIONAL_HPP
#define __UTIL_RATIONAL_HPP

#include <type_traits>
#include <iosfwd>

namespace cruft {
    template <typename T>
    struct rational {
        static_assert (std::is_integral<T>::value, "only defined for integer types");

        rational (const rational&) = default;
        rational (T n, T d);
        explicit rational (T);

        bool operator== (rational) const;
        bool operator!= (rational) const;
        bool operator<  (rational) const;
        bool operator>= (rational) const;

        explicit operator float  (void) const;
        explicit operator double (void) const;
        explicit operator int    (void) const;

        rational<T> reduced (void) const;

        rational<T>  inverse (void) const;
        rational<T>& invert  (void);

        rational<T> operator+ (T) const;
        rational<T> operator- (T) const;
        rational<T> operator* (T) const;
        rational<T> operator/ (T) const;

        T n;
        T d;
    };


    ///////////////////////////////////////////////////////////////////////////
    template <typename T, typename U>
    rational<typename std::common_type<T,U>::type>
    operator/ (U lhs, rational<T> rhs)
    {
        return rhs.inverse () * lhs;
    }


    //-------------------------------------------------------------------------
    template <typename T, typename U>
    rational<typename std::common_type<T,U>::type>
    operator* (U lhs, rational<T> rhs)
    {
        return rhs * lhs;
    }


    ///////////////////////////////////////////////////////////////////////////
    template <typename ValueT>
    std::ostream& operator<< (std::ostream&, rational<ValueT>);
};


#endif
