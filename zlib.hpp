/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2013 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_ZLIB_HPP
#define __UTIL_ZLIB_HPP

#include <stdexcept>
#include <string>

#include <zlib.h>


namespace cruft {
    namespace zlib {
        extern const char* version (void);
        extern const char* code_to_string (int);

        extern void compress (uint8_t       *dst, size_t dst_len,
                              const uint8_t *src, size_t src_len,
                              int level);

        extern size_t compress_bound (size_t len);

        extern size_t uncompress (uint8_t       *dst, size_t dst_len,
                                  const uint8_t *src, size_t src_len);


        class stream {
            public:

            public:
                stream();
                ~stream();

                int deflate (bool flush = false);
                int inflate (bool flush = false);
        };


        class error : public std::runtime_error {
            public:
                static void try_code   (int code);
                static void throw_code (int code);

                explicit error(const std::string&);
        };


        template <int CODE>
        class code_error : public error {
            public:
                code_error(void):
                    error (code_to_string (CODE))
                { ; }
        };
    }
}

#endif
