/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2018 Danny Robson <danny@nerdcruft.net>
 */

#include "tap.hpp"
#include "debug/assert.hpp"

#include <iostream>

using cruft::TAP::logger;


///////////////////////////////////////////////////////////////////////////////
logger::logger ():
    logger (std::cout)
{ ; }


//-----------------------------------------------------------------------------
logger::logger (std::ostream &_output):
    m_output (_output),
    m_status (EXIT_SUCCESS),
    m_size (0)
{ ; }


//-----------------------------------------------------------------------------
logger::~logger ()
{
    CHECK_EQ (m_reported, m_status);
    m_output << "1.." << m_size << '\n';
}


///////////////////////////////////////////////////////////////////////////////
void
logger::skip (const std::string &msg)
{
    m_output << "ok " << ++m_size << " - # SKIP " << msg << '\n';
}


//-----------------------------------------------------------------------------
void
logger::todo (const std::string &msg)
{
    m_output << "not ok " << ++m_size << " - # TODO " << msg << '\n';
}


//-----------------------------------------------------------------------------
void
logger::noop (void)
{
    skip ("noop");
}


///////////////////////////////////////////////////////////////////////////////
int
logger::status (void) const
{
#if !defined(NDEBUG)
    m_reported = m_status;
#endif

    return m_status;
}
