/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_UTIL_COORD_INIT_HPP
#define CRUFT_UTIL_COORD_INIT_HPP

#include "store.hpp"

#include <cstddef>

namespace cruft::coord {
    template <size_t S, typename T, typename SelfT>
    struct init;

    //-------------------------------------------------------------------------
    template <typename T, typename SelfT>
    struct init<1,T,SelfT> : public store<1,T,SelfT>
    {
        using store<1,T,SelfT>::store;
        constexpr init () = default;
        constexpr init (T v0):
            store<1,T,SelfT> ({v0})
        { ; }
    };


    //-------------------------------------------------------------------------
    template <typename T, typename SelfT>
    struct init<2,T,SelfT> : public store<2,T,SelfT>
    {
        using store<2,T,SelfT>::store;
        constexpr init () = default;
        constexpr init (T v0, T v1):
            store<2,T,SelfT> ({ v0, v1 })
        { ; }
    };


    //-------------------------------------------------------------------------
    template <typename T, typename SelfT>
    struct init<3,T,SelfT> : public store<3,T,SelfT>
    {
        using store<3,T,SelfT>::store;
        constexpr init () = default;
        constexpr init (T v0, T v1, T v2):
            store<3,T,SelfT> ({v0, v1, v2})
        { ; }
    };


    //-------------------------------------------------------------------------
    template <typename T, typename SelfT>
    struct init<4,T,SelfT> : public store<4,T,SelfT>
    {
        using store<4,T,SelfT>::store;
        constexpr init () = default;
        constexpr init (T v0, T v1, T v2, T v3):
            store<4,T,SelfT> ({ v0, v1, v2, v3 })
        { ; }
    };


    //-------------------------------------------------------------------------
    template <typename T, typename SelfT>
    struct init<5,T,SelfT> : public store<5,T,SelfT>
    {
        using store<5,T,SelfT>::store;
        constexpr init () = default;
        constexpr init (T v0, T v1, T v2, T v3, T v4):
            store<5,T,SelfT> ({ v0, v1, v2, v3, v4 })
        { ; }
    };
}

#endif
