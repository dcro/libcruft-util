/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2019, Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "hash/murmur/murmur2.hpp"

#include <cstdint>
#include <cstdlib>

namespace cruft::hash {
    constexpr std::uint32_t
    mix (std::uint32_t a, std::uint32_t b)
    {
        return murmur2<std::uint32_t>::mix (a, b);
    }


    constexpr std::uint64_t
    mix (std::uint64_t a, std::uint64_t b)
    {
        return murmur2<std::uint64_t>::mix (a, b);
    }


    template <typename ValueT, typename ...ArgsT>
    constexpr ValueT
    mix (ValueT head, ValueT tail, ArgsT&&...args)
    {
        return mix (
            mix (
                std::forward<ValueT> (head),
                std::forward<ValueT> (tail)
            ),
            std::forward<ArgsT> (args)...
        );
    }
}
