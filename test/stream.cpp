#include "../tap.hpp"

#include "../stream.hpp"

#include <sstream>


///////////////////////////////////////////////////////////////////////////////
template <typename T>
void
test_scoped (cruft::TAP::logger&);


//-----------------------------------------------------------------------------
template <>
void
test_scoped<cruft::stream::scoped::flags> (cruft::TAP::logger &tap)
{
    std::ostringstream os;

    {
        cruft::stream::scoped::flags f (os);
        os << std::hex;
    }

    os << 15;
    
    tap.expect_eq (os.str (), "15", "stream::scoped::flag reverts std::hex");
}


///////////////////////////////////////////////////////////////////////////////
int
main (int, char **)
{
    cruft::TAP::logger tap;

    test_scoped<cruft::stream::scoped::flags> (tap);

    return tap.status ();
}
