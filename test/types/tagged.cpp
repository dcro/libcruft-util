#include "tap.hpp"

#include "types/tagged.hpp"


enum tag_t { A, B, C };

std::ostream& operator<< (std::ostream& os, tag_t val)
{
    switch (val) {
    case A: return os << "A";
    case B: return os << "B";
    case C: return os << "C";
    }

    unreachable ();
}


struct a { static constexpr tag_t tag = A; int value;  };
struct b { static constexpr tag_t tag = B; bool value; };
struct c { static constexpr tag_t tag = C; unsigned value; };

int main ()
{
    cruft::TAP::logger tap;

    // Don't initialise with something that is likely to have a zero tag in
    // case the memory has been pre-emptively or accidentally zeroed.
    cruft::tagged<a,b,c> value (c {.value = 42 });

    tap.expect_eq (value.tag (), c::tag, "initialisation sets the correct tag");
    tap.expect_eq (value.get<c> ().value, 42u, "initialisation saves member variables");

    tap.expect_nothrow ([&] () { value.get<c> (); }, "getter after initialisation succeeds");

    value = a { .value = 7 };
    tap.expect_eq (value.tag (), a::tag, "assignment changes the tag");
    tap.expect_eq (value.get<a> ().value, 7, "assignment saves member variables");

    return tap.status ();
}