/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2016 Danny Robson <danny@nerdcruft.net>
 */

#include "stack.hpp"

#include "../debug/assert.hpp"
#include "../pointer.hpp"
#include "../cast.hpp"

using cruft::alloc::stack;


///////////////////////////////////////////////////////////////////////////////
stack::stack (cruft::view<std::byte*> _data):
    m_begin  (_data.begin ()),
    m_end    (_data.end ()),
    m_cursor (m_begin)
{
    CHECK_LE (m_begin, m_end);
}


//-----------------------------------------------------------------------------
std::byte*
stack::begin (void)
{
    return m_begin;
}


//-----------------------------------------------------------------------------
const std::byte*
stack::begin (void) const
{
    return m_begin;
}


//-----------------------------------------------------------------------------
size_t
stack::offset (const void *_ptr) const
{
    auto ptr = reinterpret_cast<const std::byte*> (_ptr);

    CHECK_GE (ptr, m_begin);
    return ptr - m_begin;
}


//-----------------------------------------------------------------------------
void
stack::reset (void)
{
    m_cursor = m_begin;
}


///////////////////////////////////////////////////////////////////////////////
size_t
stack::capacity (void) const
{
    return m_end - m_begin;
}


//-----------------------------------------------------------------------------
size_t
stack::used (void) const
{
    return m_cursor - m_begin;
}


//-----------------------------------------------------------------------------
size_t
stack::remain (void) const
{
    return capacity () - used ();
}
