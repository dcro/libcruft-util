#include "cmdopt.hpp"
#include "functor.hpp"
#include "geom/aabb.hpp"
#include "geom/sample/surface.hpp"
#include "rand/generic.hpp"

#include <cstdlib>
#include <iostream>
#include <random>


///////////////////////////////////////////////////////////////////////////////
int
main (int argc, char **argv)
{
    cruft::extent2f area {256, 256};
    float distance = 5.f;
    int samples = 15;

    cruft::cmdopt::parser opts;
    opts.add<cruft::cmdopt::option::value<float>> ('w', "width",   "width of the space to fill", area.w);
    opts.add<cruft::cmdopt::option::value<float>> ('h', "height",  "height of the space to fill", area.h);
    opts.add<cruft::cmdopt::option::value<float>> ('d', "distance", "minimum distance between samples", distance);
    opts.add<cruft::cmdopt::option::value<int>>   ('s', "samples", "number of samples per iteration", samples);

    opts.scan (argc, argv);

    auto gen = cruft::random::initialise<cruft::rand::general_generator> ();

    std::cout << "<svg height='" << area.h << "' width='" << area.h << "'>";
    namespace sample = cruft::geom::sample;
    for (auto const &p: sample::poisson (sample::surface {area},
                                         gen,
                                         cruft::functor::constant (distance),
                                         samples))
    {
        std::cout << "<circle cx='" << p.x << "' cy='" << p.y << "' r='1' />";
    }

    std::cout << "</svg>\n";

    return EXIT_SUCCESS;
}
