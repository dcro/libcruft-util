/*  Written in 2015 by Sebastiano Vigna (vigna@acm.org)
 *
 * To the extent possible under law, the author has dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide. This software is distributed without any warranty.
 */

#include "../std.hpp"

#include <limits>


namespace cruft::rand {
    class splitmix64 {
    public:
        using result_type = u64;

        splitmix64 (u64 seed):
            m_state (seed)
        { ; }

        result_type operator() (void)
        {
            u64 z = (m_state += 0x9e3779b97f4a7c15);
            z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
            z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
            return z ^ (z >> 31);
        }

        static constexpr result_type min (void)
        {
            return std::numeric_limits<result_type>::min ();
        }

        static constexpr result_type max (void)
        {
            return std::numeric_limits<result_type>::max ();
        }


    private:
        u64 m_state;
    };
}
