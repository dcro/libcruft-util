/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2018 Danny Robson <danny@nerdcruft.net>
 */

#include "ellipse.hpp"

#include "ops.hpp"
#include "aabb.hpp"
#include "ray.hpp"
#include "sphere.hpp"
#include "quaternion.hpp"

#include "../point.hpp"
#include "../matrix.hpp"
#include "../coord/iostream.hpp"

using cruft::geom::ellipse;


///////////////////////////////////////////////////////////////////////////////
template <>
bool
cruft::geom::intersects (ellipse3f e, cruft::point3f p)
{
    auto transform = cruft::quaternionf::from_to (e.up, {0,1,0}).as_matrix () *
                     cruft::translation (0-e.origin);

    return all (abs (transform * p) <= e.radius);

    //auto mag = (p - e.origin) * (p - e.origin) / (e.radius * e.radius);
    //return std::accumulate (mag.begin (), mag.end (), 0) <= 1;
}


///////////////////////////////////////////////////////////////////////////////
// query a ray-ellipse distance by transforming spaces such that the ellipse is
// a sphere
template <>
float
cruft::geom::distance (ray3f r, ellipse3f e)
{
    // find a transform that puts the ellipse at origin and scales it to a
    // unit sphere.
    auto const from_scaled = cruft::translation (e.origin.template as<vector> ()) *
                             cruft::quaternionf::from_to ({0,1,0}, e.up) *
                             cruft::scale (e.radius);
    auto const to_scaled = inverse (from_scaled);

    // transform the ray into this new space and query against a unit sphere
    auto const scaled_r = to_scaled * r;
    auto const scaled_d = distance (scaled_r, sphere3f {0, 1.f});
    auto const scaled_p = scaled_r.at (scaled_d);

    // transform the result back into the original space
    return distance (r.origin, from_scaled * scaled_p);
}


///////////////////////////////////////////////////////////////////////////////
template <>
cruft::point3f
cruft::geom::project (cruft::geom::ray3f lhs,
                     cruft::geom::ellipse3f rhs)
{
    return lhs.origin + lhs.direction * distance (lhs, rhs);
}


///////////////////////////////////////////////////////////////////////////////
cruft::geom::ellipse3f
cruft::geom::cover (cruft::view<const point3f*> src)
{
    // find our major axis points and vector
    const auto [a,b] = furthest (src);

    auto const diff = b - a;
    auto const dir = normalised (diff);

    // find a transform such that we recentre about the origin
    auto const transform = quaternionf::from_to (dir, cruft::vector3f{1,0,0}).as_matrix () *
                           translation (0 -a -diff*0.5f);

    // find the maximum absolute value in each axis
    cruft::point3f hi {0};

    for (auto const& p: src)
        hi = max (abs (transform * p), hi);

    return ellipse3f {
        .origin = a + diff * 0.5f,
        .radius = hi.as<vector> (),
        .up = rotate ({0,1,0}, cruft::quaternionf::from_to ({1,0,0}, dir))
    };
};


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
static cruft::geom::aabb<S,T>
bounds (ellipse<S,T> e)
{
    return {
        e.origin - e.radius,
        e.origin + e.radius
    };
}


//-----------------------------------------------------------------------------
template <size_t S, typename T, template <size_t,typename> class K>
cruft::geom::aabb<S,T>
cruft::geom::bounds (K<S,T> k)
{
    return ::bounds (k);
}


///////////////////////////////////////////////////////////////////////////////
template <size_t S, typename T>
std::ostream&
cruft::geom::operator<< (std::ostream &os, ellipse<S,T> val)
{
    return os << "{ origin: " << val.origin
              << ", radius: " << val.radius
              << ", up: "     << val.up
              << " }";
}


///////////////////////////////////////////////////////////////////////////////
#define INSTANTIATE_S_T(S,T) \
template cruft::geom::aabb<S,T> cruft::geom::bounds (ellipse<S,T>); \
template std::ostream& cruft::geom::operator<< (std::ostream&, ellipse<S,T>);

//template cruft::point<S,T> cruft::geom::project(ray<S,T>, ellipse<S,T>);
//template bool cruft::geom::intersects (ellipse<S,T>, cruft::point<S,T>);

INSTANTIATE_S_T(2,float)
INSTANTIATE_S_T(3,float)
