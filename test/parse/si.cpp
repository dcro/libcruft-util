#include "tap.hpp"
#include "parse/si.hpp"


int main ()
{
    cruft::TAP::logger tap;

    struct {
        char const *str;
        std::size_t val;
    } const TESTS[] = {
        {  "0",                             0u },
        {  "1",                             1u },
        { "1K",                          1024u },
        { "1M",                  1024u * 1024u },
        { "1G" ,         1024u * 1024u * 1024u },
        { "1T", 1024ul * 1024u * 1024u * 1024u },
        { "3M",             3u * 1024u * 1024u },
        { "3k",                     3u * 1024u },
    };

    for (auto const &t: TESTS) {
        auto res = cruft::parse::si<std::size_t> (t.str);
        if (!res) {
            tap.fail ("SI parsing %!", t.str);
        } else {
            tap.expect_eq (t.val, *res, "SI parsing '%!'", t.str);
        }
    }

    return tap.status ();
}