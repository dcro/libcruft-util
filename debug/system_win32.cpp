#include "./system.hpp"

#include <cruft/util/win32/windows.hpp>


bool cruft::debug::is_interactive_console (void)
{
    HWND console_window = GetConsoleWindow ();

    DWORD dwProcessId;
    GetWindowThreadProcessId (console_window, &dwProcessId);

    return GetCurrentProcessId () == dwProcessId;
}