#include "tap.hpp"

#include "endian.hpp"

#include <cstdint>


///////////////////////////////////////////////////////////////////////////////
int
main (void)
{
    cruft::TAP::logger tap;

    {
        uint32_t a = 0x12345678, b = 0x78563412;
        tap.expect_eq (a, cruft::bswap (b), "u32 byteswap");
    }

    {
        static std::uint8_t const bytes[] = { 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef };
        tap.expect_eq (cruft::readle<std::uint64_t> (bytes), 0xefcdab8967452301, "readle<u64>");
    }


    {
        // try to byte swap the pattern for 1.0f
        //
        // it may not be the most robust test, but it'll catch the most
        // egregious errors for the time being.
        union {
            uint32_t u;
            float f;
        } data { .u = 0x0000803f /* 0x3f800000 == 1.f */ };

        tap.expect_eq (cruft::bswap (data.f), 1.f, "f32 byteswap");
    };

    return tap.status ();
}