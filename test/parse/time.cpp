#include "tap.hpp"
#include "parse/time.hpp"


///////////////////////////////////////////////////////////////////////////////
using days   = std::chrono::duration<std::int_fast32_t, std::ratio<86400>>;
using weeks  = std::chrono::duration<std::int_fast32_t, std::ratio<604800>>;
using months = std::chrono::duration<std::int_fast32_t, std::ratio<2629746>>;
using years  = std::chrono::duration<std::int_fast32_t, std::ratio<31556952>>;


///////////////////////////////////////////////////////////////////////////////
static struct {
    char const *str;
    std::chrono::nanoseconds val;
    char const *msg;
} TESTS[] = {
    { .str = "1", .val = std::chrono::seconds (1), .msg = "bare value" },

    { .str = "1ns",      .val = std::chrono::nanoseconds (1), .msg = "1 nanosecond" },
    { .str = "1us",      .val = std::chrono::microseconds (1), .msg = "1 microsecond" },
    { .str = "1ms",      .val = std::chrono::milliseconds (1), .msg = "1 millisecond" },
    { .str = "1s",       .val = std::chrono::seconds (1), .msg = "1 second" },
    { .str = "1m",       .val = std::chrono::minutes (1), .msg = "1 minute" },
    { .str = "1h",       .val = std::chrono::hours (1), .msg = "1 hour" },
    { .str = "1 days",   .val = days (1), .msg = "1 day" },
    { .str = "1 weeks",  .val = weeks (1), .msg = "1 week" },
    { .str = "1 months", .val = months (1), .msg = "1 month" },
    { .str = "1 years",  .val = years (1), .msg = "1 year" },
};


///////////////////////////////////////////////////////////////////////////////
int main ()
{
    cruft::TAP::logger tap;

    for (auto const &[str,val,msg]: TESTS) {
        auto res = cruft::parse::duration::from (str);
        tap.expect (res && val == *res, "%! %! == %!", msg, val.count (), res->count());
    }

    return tap.status ();
}