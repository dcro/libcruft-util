/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <iosfwd>
#include <string>

namespace cruft::util {
    /// A tag structure that can be passed to an ostream which will print a
    /// collection of system environment values in a JSONish format that
    /// might be suitable for triaging bug reports.
    ///
    /// If you want something specific it would be better to avoid populating
    /// this structure directly and instead write a query that exposes the
    /// value directly.
    struct sysinfo { };

    std::ostream& operator<< (std::ostream &, sysinfo const&);
}
