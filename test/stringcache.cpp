#include <cruft/util/tap.hpp>
#include <cruft/util/stringcache.hpp>
#include <cruft/util/iterator/zip.hpp>


int main ()
{
    cruft::TAP::logger tap;

    {
        static constexpr const char* VALUES[] = {
            "foo",
            "bar",
            "qux",
            "the",
            "quick",
            "brown",
            "fox",
        };

        cruft::stringcache cache;

        std::vector<cruft::stringcache::id_t> indices;
        for (auto const v: VALUES)
            indices.push_back (cache.add (v));

        bool success = true;
        for (auto const [expected, idx]: cruft::iterator::zip (VALUES, indices)) {
            auto const actual = cache[idx];
            if (actual != expected) {
                success = false;
                break;
            }
        }

        tap.expect (success, "cache holds values");
    }

    return tap.status ();
}