/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __CRUFT_UTIL_POSIX_HPP
#define __CRUFT_UTIL_POSIX_HPP

#include "fd.hpp"

#include <sys/mman.h>
#include <cstdint>


namespace cruft::posix {
    class map {
    public:
        map (const map&) = delete;
        map (map&&) noexcept;

        map (size_t size, int prot, int flags);
        map (size_t size, int prot, int flags, const fd&, off_t offset = 0);
        ~map ();

        map& operator= (const map&) = delete;
        map& operator= (map&&) noexcept;

        uint8_t* begin (void);
        uint8_t* end   (void);
        const uint8_t* begin  (void) const;
        const uint8_t* end    (void) const;
        const uint8_t* cbegin (void) const;
        const uint8_t* cend   (void) const;

        void* data (void);
        const void* data (void) const;

        void sync (void *addr, size_t len, int flags);

        bool empty (void) const;
        size_t size (void) const;

        enum class resize_t { MOVE, NOMOVE };
        void resize (size_t newsize, resize_t = resize_t::NOMOVE);

    private:
        void    *m_addr;
        size_t   m_size;
    };
}

#endif
