#include "tap.hpp"

#include "geom/line.hpp"

int
main (int, char**)
{
    cruft::TAP::logger tap;

    static struct {
        cruft::point3f origin;
        cruft::vector3f direction;
        cruft::point3f q;
        float distance;
        const char *message;
    } TESTS[] = {
        { { 0, 0, 0 }, { 1, 0, 0}, { 0, 0, 0 }, 0, "origin line, origin point" } ,
        { { 0, 1, 0 }, { 1, 0, 0}, { 0, 0, 0 }, 1, "+x line, unit distance" },
    };

    for (auto const& t: TESTS) {
        cruft::geom::line3f l { t.origin, t.direction };
        auto d = distance2 (l, t.q);
        tap.expect_eq (d, t.distance, "%!", t.message);
    }

    return tap.status ();
}