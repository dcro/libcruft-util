#include "geom/aabb.hpp"
#include "geom/plane.hpp"
#include "geom/ray.hpp"
#include "geom/iostream.hpp"
#include "tap.hpp"

using cruft::geom::ray2f;
using cruft::geom::ray3f;


//-----------------------------------------------------------------------------
void
test_intersect_plane (cruft::TAP::logger &tap)
{
    // trivial case: origin ray facing z, plane at unit z facing -z.
    const cruft::geom::ray3f l { .origin = 0, .direction = {0, 0, 1} };
    const cruft::geom::plane3f p (cruft::point3f {0,0,1}, cruft::vector3f {0,0,-1});

    tap.expect_eq (distance (l, p), 1.f, "ray-plane intersect");
}


//-----------------------------------------------------------------------------
int
main (void)
{
    cruft::TAP::logger tap;

    test_intersect_plane (tap);

    return tap.status ();
}
