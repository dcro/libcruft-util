#include "./system.hpp"

#include <unistd.h>


bool
cruft::debug::is_interactive_console (void)
{
    return isatty (STDIN_FILENO);
}
