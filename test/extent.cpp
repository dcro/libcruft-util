#include "extent.hpp"
#include "tap.hpp"
#include "types.hpp"

int
main (void)
{
    cruft::TAP::logger tap;

    {
        constexpr const cruft::extent2u hi { 8, 4 };
        constexpr const cruft::extent2u lo { 6, 2 };
        tap.expect_eq (lo, hi.contracted (2), "extent scalar contraction by value");
    }

    {
        static constexpr cruft::point2u EXPECTED[] = {
            { 0, 0 }, { 1, 0 }, { 2, 0 },
            { 0, 1 }, { 1, 1 }, { 2, 1 },
        };

        size_t offset = 0;
        bool success = true;

        for (auto p: cruft::extent2u {3,2}.step ()) {
            if (offset >= std::size (EXPECTED)) {
                success = false;
                break;
            }

            success = success && cruft::all (EXPECTED[offset++] == p);
        }

        tap.expect (success, "extent_range2u iteration");
    }

    return tap.status ();
}
