#include "array/parray.hpp"

#include "tap.hpp"


///////////////////////////////////////////////////////////////////////////////
int
main ()
{
    static_assert (is_container_v<cruft::parray<int>>);

    cruft::TAP::logger tap;

    int raw[3] = { -5, 9, 0 };
    cruft::parray wrapped (raw);

    tap.expect (equal (raw, wrapped), "comparison of array and parray");

    return tap.status ();
}
