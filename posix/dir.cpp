/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include "dir.hpp"

#include "except.hpp"

using cruft::posix::dir;


///////////////////////////////////////////////////////////////////////////////
dir::dir (const std::filesystem::path &p):
    m_handle (::opendir (p.string<char> ().c_str ()))
{
    if (!m_handle)
        error::throw_code ();
}


//-----------------------------------------------------------------------------
dir::~dir ()
{
    error::try_code (closedir (m_handle));
}


///////////////////////////////////////////////////////////////////////////////
dir::operator DIR* (void)
{
    return m_handle;
}


///////////////////////////////////////////////////////////////////////////////
void
dir::rewind (void)
{
    rewinddir (m_handle);
}
