/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2017 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "fwd.hpp"

#include <filesystem>

#include <sys/stat.h>

namespace cruft::posix {
    struct stat stat (char const *path);
    struct stat stat (std::filesystem::path const&);

    struct stat fstat (int);
    struct stat fstat (fd const&);

    struct stat stat (int);
    struct stat stat (fd const&);
}