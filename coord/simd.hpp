/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_UTIL_COORD_SIMD_HPP
#define CRUFT_UTIL_COORD_SIMD_HPP

#ifdef __SSE__
#include "simd_sse.hpp"
#else
#error "Unsupported SIMD architecture"
#endif

#endif
