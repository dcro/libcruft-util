#include "scoped.hpp"
#include "tap.hpp"


///////////////////////////////////////////////////////////////////////////////
void test_restore (cruft::TAP::logger &tap)
{
    int value = 42;

    {
        cruft::scoped::restore raii (value);
        tap.expect_eq (value, 42, "restorer constructor doesn't modifier value");
        value = 7;
    }

    tap.expect_eq (value, 42, "restorer restores value at destruction");


    {
        cruft::scoped::restore raii (value);
        value = 7;
        raii.release ();
    }

    tap.expect_eq (value, 7, "restorer reset doesn't modify value at destruction");
}


///////////////////////////////////////////////////////////////////////////////
int main ()
{
    cruft::TAP::logger tap;
    test_restore (tap);
    return tap.status ();
}