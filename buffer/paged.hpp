/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../std.hpp"
#include "../view.hpp"

#include <cstddef>


namespace cruft::buffer {
    /// Implements a manually paged memory buffer of a fixed length.
    ///
    /// It can be used to ensure a contiguous memory region of bounded size
    /// is available at a later point in execution.
    class paged {
    public:
        using value_type = u08;

        paged (size_t bytes);
        ~paged ();

        paged (const paged&) = delete;
        paged (paged &&);
        paged& operator= (const paged&) = delete;
        paged& operator= (paged &&);

        value_type* begin (void)&;
        value_type* end   (void)&;

        const value_type* cbegin (void)&;
        const value_type* cend   (void)&;

        const value_type* begin (void) const&;
        const value_type* end   (void) const&;

        void commit  (value_type *ptr) { return commit  ({ ptr, 1 }); }
        void release (value_type *ptr) { return release ({ ptr, 1 }); }

        /// Populates the memory range and ensures it is read/write.
        void commit  (cruft::view<value_type*>);
        /// Returns the memory range to the system.
        void release (cruft::view<value_type*>);

        auto size (void) const { return m_data.size (); }
        auto capacity (void) const { return m_data.size (); }

    private:
        /// Applies memory protection flags to the given memory region.
        void apply_prot (cruft::view<value_type*>, int prot);

        cruft::view<value_type*> m_data;
    };
}
