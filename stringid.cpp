/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2014 Danny Robson <danny@nerdcruft.net>
 */

#include "stringid.hpp"

#include <stdexcept>

using cruft::stringid;


///////////////////////////////////////////////////////////////////////////////
stringid::id_t
stringid::operator[] (std::string_view const &key)
{
    if (auto const pos = m_map.find (key); pos != m_map.cend ())
        return pos->second;


    auto const &[pos, success] = m_map.emplace (key, m_map.size ());
    return pos->second;
}


///////////////////////////////////////////////////////////////////////////////
stringid::id_t
stringid::add (std::string key)
{
    auto pos = m_map.find (key);
    if (pos != m_map.end ())
        throw std::invalid_argument ("duplicate stringid key");

    id_t id = m_map.size ();
    m_map.insert ({ std::move (key), id });
    return id;
}


///////////////////////////////////////////////////////////////////////////////
stringid::id_t
stringid::find (std::string_view const &key) const
{
    auto pos = m_map.find (key);
    if (pos == m_map.end ())
        throw std::out_of_range ("invalid stringid key");

    return pos->second;
}


///////////////////////////////////////////////////////////////////////////////
void
stringid::clear (void)
{
    m_map.clear ();
}
