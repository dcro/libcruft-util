/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include "library_posix.hpp"

#include <stdexcept>


using cruft::detail::library_posix;


///////////////////////////////////////////////////////////////////////////////
library_posix::library_posix (const std::filesystem::path &path):
    m_handle (dlopen (path.c_str (), RTLD_NOW))
{
    if (!m_handle)
        throw std::runtime_error (dlerror ());
}


//-----------------------------------------------------------------------------
library_posix::library_posix (library_posix &&rhs) noexcept:
    m_handle (nullptr)
{
    std::swap (m_handle, rhs.m_handle);
}


//-----------------------------------------------------------------------------
library_posix::~library_posix ()
{
    if (m_handle)
        dlclose (m_handle);
}
