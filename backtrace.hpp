/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019, Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <vector>
#include <iosfwd>


///////////////////////////////////////////////////////////////////////////////
namespace cruft {
    class backtrace {
    public:
        backtrace (void);

        const auto& frames(void) const { return m_frames; }

    private:
        std::vector<void *> m_frames;
    };

    std::ostream&
    operator<< (std::ostream&, backtrace const&);
}
