/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_GEOM_CYLINDER_HPP
#define __UTIL_GEOM_CYLINDER_HPP

#include "../point.hpp"

///////////////////////////////////////////////////////////////////////////////
namespace cruft::geom {
    template <size_t S, typename T>
    struct cylinder {
        cruft::point<S,T> p0, p1;
        T radius;

        float distance (cruft::point<S,T>) const;
        bool includes (cruft::point<S,T>) const;
    };

    using cylinder3f = cylinder<3,float>;
}

#endif
