/******************************************************************************
                              _       _
                             | |     | |
                             | | ___ | |__   ___
                             | |/ _ \| '_ \ / _ \
                             | | (_) | |_) | (_) |
                             |_|\___/|_.__/ \___/
    Copyright:
        Danny Robson, 2020
 *****************************************************************************/

#pragma once


namespace cruft::init {
    /// An object that ensures functionality that requires runtime
    /// registration has been setup, and clears these resources at destruction
    /// time.
    ///
    /// Do not operate on instances of this class at the same time across
    /// multiple threads. (It is safe to do so if access is has been locked)
    ///
    /// In particular, this class ensures that:
    ///    * log::level_t has been registered for reflection
    struct cookie {
        cookie ();
        cookie (cookie const&);
        cookie& operator= (cookie const&);
        cookie (cookie &&) noexcept;
        cookie& operator= (cookie &&) noexcept;
        ~cookie ();
    };
}
