/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */


#include "null.hpp"

using cruft::alloc::null;


///////////////////////////////////////////////////////////////////////////////
std::byte*
null::begin (void)
{
    return nullptr;
}


//-----------------------------------------------------------------------------
const std::byte*
null::begin (void) const
{
    return nullptr;
}


//-----------------------------------------------------------------------------
std::byte*
null::end (void)
{ return nullptr; }


//-----------------------------------------------------------------------------
const std::byte*
null::end (void) const
{ return nullptr; }


//-----------------------------------------------------------------------------
size_t
null::offset (const void *ptr) const
{
    return reinterpret_cast<uintptr_t> (ptr);
}


///////////////////////////////////////////////////////////////////////////////
void
null::reset (void)
{
    ;
}


///////////////////////////////////////////////////////////////////////////////
size_t
null::capacity (void) const
{
    return 0;
}


//-----------------------------------------------------------------------------
size_t
null::used (void) const
{
    return 0;
}


//-----------------------------------------------------------------------------
size_t
null::remain (void) const
{
    return 0;
}
