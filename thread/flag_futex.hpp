/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_UTIL_THREAD_FLAG_HPP
#define CRUFT_UTIL_THREAD_FLAG_HPP

#include <atomic>

namespace cruft::thread {
    /// a fire-once event that users can wait upon. if already fired then
    /// waiting will be a noop.
    class flag {
    public:
        flag ();

        /// blocks indefinitely until the flag has been set.
        void wait (void);

        /// wake all the threads waiting on the flag
        void notify_one (void);

        /// wake all threads waiting on the flag
        void notify_all (void);

    private:
        std::atomic<int> value;
    };
};

#endif
