/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010 Danny Robson <danny@nerdcruft.net>
 */


#include "fletcher.hpp"

using cruft::hash::fletcher;


///////////////////////////////////////////////////////////////////////////////
template <typename T>
fletcher<T>::fletcher (part_t _modulus, part_t _a, part_t _b):
    m_modulus { _modulus },
    m_initial { _a, _b }
{ ; }



///////////////////////////////////////////////////////////////////////////////
template <typename DigestT>
typename fletcher<DigestT>::digest_t
fletcher<DigestT>::operator() (cruft::view<const std::uint8_t*> data) const noexcept
{
    state_t accum = m_initial;

    for (const auto i: data) {
        accum.a = (accum.a +       i) % m_modulus;
        accum.b = (accum.a + accum.b) % m_modulus;
    }

    return accum.b << (sizeof(part_t) * 8u) | accum.a;
}


///////////////////////////////////////////////////////////////////////////////
template class cruft::hash::fletcher<uint32_t>;
