/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2014 Danny Robson <danny@nerdcruft.net>
 */

#include "rational.hpp"

#include "maths.hpp"
#include "debug/assert.hpp"

#include <cstdint>

using cruft::rational;


///////////////////////////////////////////////////////////////////////////////
template <typename T>
rational<T>::rational (T _n, T _d):
    n (_n),
    d (_d)
{
    if (n < 0 && d < 0) {
        n *= -1;
        d *= -1;
    }
}


//-----------------------------------------------------------------------------
template <typename T>
rational<T>::rational (T v):
    n (v),
    d (1)
{ ; }


///////////////////////////////////////////////////////////////////////////////
template <typename T>
bool
rational<T>::operator== (const rational<T> rhs) const
{
    return rhs.n == n && rhs.d == d;
}


//-----------------------------------------------------------------------------
template <typename T>
bool
rational<T>::operator!= (const rational<T> rhs) const
{
    return !operator== (rhs);
}


//-----------------------------------------------------------------------------
template <typename T>
bool
rational<T>::operator< (const rational<T> rhs) const
{
    return n * rhs.d < rhs.n * d;
}


//-----------------------------------------------------------------------------
template <typename T>
bool
rational<T>::operator>= (const rational<T> rhs) const
{
    return n * rhs.d >= rhs.n * d;
}


///////////////////////////////////////////////////////////////////////////////
template <typename T>
rational<T>::operator float (void) const
{
    return static_cast<float> (n) / d;
}


//-----------------------------------------------------------------------------
template <typename T>
rational<T>::operator double (void) const
{
    return static_cast<double> (n) / d;
}


//-----------------------------------------------------------------------------
template <typename T>
rational<T>::operator int (void) const
{
    return static_cast<int> (n / d);
}


///////////////////////////////////////////////////////////////////////////////
template <typename T>
rational<T>
rational<T>::reduced (void) const
{
    if (n == 0) {
        return { 0, 1 };
    }

    CHECK_NEZ (d);
    auto x = std::gcd (abs (n), abs (d));
    return { n / x, d / x };
}


///////////////////////////////////////////////////////////////////////////////
template <typename T>
rational<T>
rational<T>::inverse (void) const
{
    return rational<T> { d, n };
}


//-----------------------------------------------------------------------------
template <typename T>
rational<T>
rational<T>::operator+ (const T rhs) const
{
    return { n + rhs * d, d };
}


//-----------------------------------------------------------------------------
template <typename T>
rational<T>
rational<T>::operator- (const T rhs) const
{
    return { n - rhs * d, d };
}


//-----------------------------------------------------------------------------
template <typename T>
rational<T>
rational<T>::operator* (const T rhs) const
{
    return { rhs * n, d };
}


//-----------------------------------------------------------------------------
template <typename T>
rational<T>
rational<T>::operator/ (const T rhs) const
{
    return { n, rhs * d };
}


///////////////////////////////////////////////////////////////////////////////
template <typename ValueT>
std::ostream&
cruft::operator<< (std::ostream &os, cruft::rational<ValueT> val)
{
    return os << val.n << "/" << val.d;
}


///////////////////////////////////////////////////////////////////////////////
#define INSTANTIATE(TYPE) \
template struct cruft::rational<TYPE>; \
template cruft::rational<TYPE> cruft::operator/ (TYPE, cruft::rational<TYPE>); \
template std::ostream& cruft::operator<< (std::ostream&, cruft::rational<TYPE>);

INSTANTIATE(uint32_t)
INSTANTIATE(int32_t)
