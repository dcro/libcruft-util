/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <utility>
#include <tuple>


namespace cruft::functor {
    /// Returns the supplied argument unchanged
    ///
    /// CXX#20: Revisit when C++20 support is available. This is just a
    /// trivial implementation of std::identity.
    struct identity {
        template <typename ValueT>
        ValueT&&
        operator() (ValueT &&val)
        {
            return std::forward<ValueT> (val);
        }
    };


    /// A trivial functor that wraps std::begin without any static typing.
    struct begin {
        template <typename ValueT>
        decltype (auto)
        operator() (ValueT &&value) noexcept (noexcept (std::begin (value)))
        {
            return std::begin (
                std::forward<ValueT> (value)
            );
        }
    };


    /// A trivial functor that wraps std::end without any static typing.
    struct end {
        template <typename ValueT>
        decltype (auto)
        operator() (ValueT &&value) noexcept (noexcept (std::end (value)))
        {
            return std::end (
                std::forward<ValueT> (value)
            );
        }
    };


    ///////////////////////////////////////////////////////////////////////////
    /// returns the value provided at construction time regardless of the
    /// arguments supplied in the call operator.
    template <typename ValueT>
    class constant {
    public:
        constant (const ValueT &_value):
            m_value (_value)
        { ; }


        template <typename ...Args>
        ValueT&
        operator() (Args&&...) noexcept
        { return m_value; }


        template <typename ...Args>
        const ValueT&
        operator() (Args&&...) const noexcept
        { return m_value; }


    private:
        ValueT m_value;
    };


    //-------------------------------------------------------------------------
    template <typename ValueT>
    constant (ValueT) -> constant<std::decay_t<ValueT>>;


    ///////////////////////////////////////////////////////////////////////////
    /// Returns a ValueT constructed from the supplied arguments.
    template <typename ValueT>
    struct construct {
        template <typename ...Args>
        ValueT operator() (Args &&...args)
        {
            return ValueT {
                std::forward<Args> (args)...
            };
        }
    };


    ///------------------------------------------------------------------------
    /// Returns a ValueT constructed from a tuple of arguments.
    template <typename ValueT>
    struct tuple_construct {
        template <typename Args>
        ValueT operator() (Args &&args)
        {
            return std::apply (
                construct<ValueT> {},
                std::forward<Args> (args)
            );
        }
    };
}