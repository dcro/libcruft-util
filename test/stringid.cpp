#include "stringid.hpp"

#include "tap.hpp"

#include <cstdlib>
#include <stdexcept>

int
main (int, char**) {
    cruft::TAP::logger tap;

    cruft::stringid map;

    tap.expect_throw<std::out_of_range> ([&] { map.find ("invalid"); }, "find on empty set throws");

    auto id1 = map.add ("first");
    tap.expect_eq (id1, map.find ("first"), "single entity ID matches");

    tap.expect_throw<std::out_of_range> ([&] { map.find ("invalid"); }, "invalid find throws");

    auto id2 = map.add ("second");
    tap.expect_eq (id1 + 1, id2, "monotonically increasing IDs");
    tap.expect_eq (id1, map.find ("first"), "first element still matches");

    tap.expect_eq (map["second"], id2, "index lookup invokes no change");

    auto id3 = map["third"];
    tap.expect (id3 != id1 && id3 != id2, "index creation adds new value");

    return tap.status ();
}
