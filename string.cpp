/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011-2017 Danny Robson <danny@nerdcruft.net>
 */

#include "string.hpp"

#include <cstring>
#include <codecvt>
#include <locale>

using cruft::tokeniser;


///////////////////////////////////////////////////////////////////////////////
std::string
cruft::to_utf8 (const wchar_t *src)
{
    using convert_t = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_t,wchar_t> converter;
    return converter.to_bytes (src);
}


//-----------------------------------------------------------------------------
std::string
cruft::to_utf8 (const std::wstring &src)
{
    return to_utf8 (src.c_str ());
}


///////////////////////////////////////////////////////////////////////////////
// TODO: Horribly inefficient, but God help you if you're relying on this
// being efficient in the first place.
bool
strbegins (const char *restrict str,
           const char *restrict prefix) {
    return 0 == strncmp (prefix, str, strlen (prefix));
}
