#include "set/dset.hpp"
#include "tap.hpp"


using cruft::set::dset;


int main ()
{

    cruft::TAP::logger tap;

    {
        dset<int, 5> const a { 1, 2, 3,    5, };
        dset<int, 5> const b {    2,    4, 5, };
        dset<int, 5> const c {    2,       5, };

        tap.expect_eq (a & b, c, "set intersection");
    }

    {
        dset<int, 5> val { 3, 4, 5 };
        val.add (2, 4);
        dset<int, 5> const expected { 3, 4, 4, 4, 5 };

        tap.expect_eq (val, expected, "set add_n");
    }

    return tap.status ();
}