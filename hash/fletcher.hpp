/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2016 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../types/sized.hpp"
#include "../view.hpp"

#include <cstdint>
#include <cstdlib>


///////////////////////////////////////////////////////////////////////////////
namespace cruft::hash {
    template <typename DigestT>
    class fletcher {
    public:
        using digest_t = DigestT;
        using part_t = typename types::sized::bytes<sizeof (digest_t) / 2>::uint;

        fletcher (part_t modulus, part_t a, part_t b);

        digest_t
        operator() (cruft::view<const std::uint8_t*>) const noexcept;

    private:
        struct state_t {
            part_t a, b;
        };

        const digest_t m_modulus;
        const state_t m_initial;
    };
}
