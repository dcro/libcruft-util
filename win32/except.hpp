/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010, 2017
 *     Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_UTIL_WIN32_EXCEPT_HPP
#define CRUFT_UTIL_WIN32_EXCEPT_HPP

#include "windows.hpp"

#include <stdexcept>
#include <functional>


namespace cruft::win32 {
    class error : public std::runtime_error {
    public:
        explicit error (DWORD _code);
        error ();

        DWORD code (void) const;
        static DWORD last_code (void);

        template <typename FunctionT, typename ...ArgsT>
        static decltype(auto)
        try_call (FunctionT &&func, ArgsT &&...args)
        {
            // GCC#9.1.0: We'd like to use std::invoke here, but mingw has a
            // spotty record of actually supplying this functionality.
            //
            // It's almost certain that we'll be calling C functions, so
            // pretend that's what we received for the time being.
            return try_value (
                func (
                    std::forward<ArgsT> (args)...
                )
            );
        }

        static HMODULE try_value (HMODULE);
        static HANDLE  try_value (HANDLE);
        static BOOL    try_value (BOOL);

        static void try_code (void);
        static void try_code (DWORD);

        static void throw_code [[gnu::noreturn]] (void);
        static void throw_code [[gnu::noreturn]] (DWORD);

        static std::string code_string (void);
        static std::string code_string (DWORD);

    private:
        DWORD m_code;
    };
}

#endif
