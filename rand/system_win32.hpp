/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <limits>
#include <string>

namespace cruft::rand {
    namespace win32 {
        /// Models std::random_device and is suitable as a drop-in replacement.
        ///
        /// This uses the RtlGenRandom function which is currently deprecated.
        class rtlgenrandom {
        public:
            using result_type = unsigned;

            rtlgenrandom ();
            rtlgenrandom (std::string const&);
            rtlgenrandom (rtlgenrandom const &) = delete;

            rtlgenrandom& operator= (rtlgenrandom const&) = delete;

            result_type operator() (void);

            double entropy (void) const noexcept;

            static constexpr result_type min (void) { return std::numeric_limits<result_type>::min (); }
            static constexpr result_type max (void) { return std::numeric_limits<result_type>::max (); }
        };
    }

    using system = win32::rtlgenrandom;
}
