#include "platform.hpp"

#if defined(PLATFORM_LINUX)
#include "event_futex.hpp"
#else
#include "event_std.hpp"
#endif