/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2019 Danny Robson <danny@nerdcruft.net>
 */


#pragma once

#include "./extent.hpp"
#include "./point.hpp"
#include "./vector.hpp"
#include "./types/traits.hpp"

#include <iosfwd>

namespace cruft {
    /**
     * A two-dimensional rectangle, with size and position.
     */
    template <size_t S, typename T>
    struct region {
        using extent_t = cruft::extent<S,T>;
        using point_t  = cruft::point<S,T>;

        using value_type = T;

        //---------------------------------------------------------------------
        static constexpr size_t dimension = S;
        static constexpr size_t elements = extent_t::elements + point_t::elements;

        point_t  p;
        extent_t e;

        //---------------------------------------------------------------------
        region () = default;
        explicit region (extent_t);
        region (point_t, extent_t);
        region (point_t, point_t);

        //---------------------------------------------------------------------
        template <typename U>
        constexpr region<S,U>
        cast (void) const
        {
            return {
                p.template cast<U> (),
                e.template cast<U> ()
            };
        }

        //---------------------------------------------------------------------
        constexpr T area (void) const { return e.area (); }
        constexpr T diameter (void) const { return e.diameter (); }
        extent_t magnitude (void) const;
        extent_t magnitude (extent_t);

        bool empty (void) const;

        //---------------------------------------------------------------------
        point_t base    (void) const;
        point_t away    (void) const;
        point_t centre  (void) const;
        point_t closest (point_t) const;

        //---------------------------------------------------------------------
        // exclusive of borders
        bool intersects (region<S,T>) const;

        // Compute binary region combinations
        region intersection (region<S,T>) const;

        // Test if a region lies completely within our space
        bool covers (region<S,T>) const noexcept;

        /// Test if a point lies within our space. Inclusive of borders
        constexpr
        bool
        inclusive (point<S,T> q) const noexcept
        {
            return all (p <= q && p + e >= q);
        }

        /// test if a point lies within our space, exclusive of the
        /// bottom-right border
        constexpr bool
        exclusive (point<S,T> q) const noexcept
        {
            return all (p <= q && p + e > q);
        }

        // Move a point to be within the region bounds
        point_t constrain (point_t) const noexcept;

        //---------------------------------------------------------------------
        // Compute a region `mag` units into the region
        region inset (T mag) const;
        region inset (vector<S,T> mag) const;

        region expand (T mag) const;
        region expand (vector<S,T>) const;

        // arithmetic operators
        region operator+ (vector<S,T>) const;
        region operator- (vector<S,T>) const;

        // Logical comparison operators
        bool operator ==(region<S,T> rhs) const;
        bool operator !=(region<S,T> rhs) const
            { return !(*this == rhs); }

        // Utility constants
        static constexpr region<S,T> max  (void)
        {
            return {
                cruft::point <S,T> {std::numeric_limits<T>::lowest () / 2},
                cruft::extent<S,T> {std::numeric_limits<T>::max ()}
            };
        }

        static constexpr region<S,T> unit (void)
        {
            return {
                point_t::origin (),
                extent_t {1}
            };
        }

        static constexpr region<S,T> zero (void)
        { return { point_t {0}, extent_t {0} }; }


        class iterator {
        public:
            using iterator_category = std::forward_iterator_tag;
            using difference_type = std::size_t;
            using value_type = point_t;
            using pointer = value_type*;
            using reference = value_type&;

            iterator (point_t _lo, point_t _hi):
                cursor (_lo),
                lo (_lo),
                hi (_hi)
            { ; }

            iterator ()
                : cursor {}
                , lo {}
                , hi {}
            { ; }


            const point_t& operator* (void) const& { return cursor; }

            iterator&
            operator++ (void)
            {
                cursor[0] += 1;

                for (size_t s = 0; s < S-1; ++s) {
                    if (cursor[s] < hi[s])
                        return *this;

                    cursor[s] = lo[s];
                    cursor[s+1]++;
                }

                return *this;
            }

            bool operator== (const iterator &rhs) const { return cursor == rhs.cursor; }
            bool operator!= (const iterator &rhs) const { return cursor != rhs.cursor; }

        private:
            point_t cursor, lo, hi;
        };


        /// Returns an iterator that provides successive points across the
        /// region.
        ///
        /// The points are in the half open range [p, p+e). ie, the
        /// 'bottom-right' corner will never be returned. If you need this
        /// behaviour then construct a larger range.
        auto step (void) const
        {
            point_t last = p;
            last[S-1] = (p + e)[S-1];

            return cruft::view {
                iterator { p,    p + e },
                iterator { last, p + e }
            };
        };


        void sanity (void) const;
    };


    ///////////////////////////////////////////////////////////////////////////
    template <typename T> using region2 = region<2,T>;
    template <typename T> using region3 = region<3,T>;


    //-------------------------------------------------------------------------
    using region2u = region2<unsigned>;
    using region2i = region2<int>;
    using region2f = region2<float>;
    using region2d = region2<double>;


    //-------------------------------------------------------------------------
    extern template struct region<2, unsigned>;
    extern template struct region<2, int>;
    extern template struct region<2, float>;
    extern template struct region<2, double>;


    ///////////////////////////////////////////////////////////////////////////
    /// constructs the minimal region that encompasses a region and a point.
    template <typename T, size_t S>
    region<S,T>
    operator| (region<S,T> const r, point<S,T> const p)
    {
        const auto p0 = select (r.p < p,       r.p,       p);
        const auto p1 = select (r.away () > p, r.away (), p);
        return { p0, p1 };
    }


    //-------------------------------------------------------------------------
    template <typename T, size_t S>
    auto
    operator| (point<S,T> const p, region<S,T> const r)
    {
        return r | p;
    }


    //-------------------------------------------------------------------------
    // construct a minimal bounding region over two supplied regions
    template <typename T, size_t S>
    region<S,T>
    operator| (region<S,T> const a, region<S,T> const b)
    {
        return a | b.base () | b.away ();
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Construct a region that consists of the overlapping portions of two
    /// supplied regions.
    ///
    /// The behaviour is undefined if there is no overlap. The caller should
    /// test using `intersects` if required beforehand.
    template <typename T, std::size_t S>
    region<S,T>
    intersection (region<S,T> const a, region<S,T> const b)
    {
        // Find the two corners of the new region.
        auto const lo = cruft::max (a.base (), b.base ());
        auto const hi = cruft::min (a.away (), b.away ());
        CHECK (all (lo <= hi));

        return { lo, hi };
    }

    ///////////////////////////////////////////////////////////////////////////
    /// returns the squared minimum distance from a region to a given point
    template <size_t S, typename T>
    T
    distance2 (region<S,T> r, point<S,T> p)
    {
        auto const clamped = cruft::max (
            r.p - p,
            vector<S,T> (0),
            p - (r.p + r.e)
        );

        return sum (clamped * clamped);
    }


    ///------------------------------------------------------------------------
    /// returns the squared minimum distance from a region to a given point
    template <size_t S, typename T>
    T
    distance2 (point<S,T> p, region<S,T> r)
    {
        return distance2 (r, p);
    }


    ///////////////////////////////////////////////////////////////////////////
    /// returns true if the supplied point lies within the supplied region
    /// inclusive of borders.
    template <size_t S, typename T>
    bool
    intersects (cruft::region<S,T> const area,
                cruft::point<S,T> const query)
    {
        return area.inclusive (query);
    }


    ///------------------------------------------------------------------------
    /// returns true if the supplied point lies within the supplied region
    /// inclusive of borders.
    template <size_t S, typename T>
    bool
    intersects (cruft::point<S,T> const query,
                cruft::region<S,T> const area)
    {
        return intersects (area, query);
    }


    ///////////////////////////////////////////////////////////////////////////
    /// returns a uniformly randomly sampled point within the supplied region
    template <size_t S, typename T, typename GeneratorT>
    cruft::point<S,T>
    sample (region<S,T> shape, GeneratorT &&gen)
    {
        return shape.p + sample (
            shape.e, std::forward<GeneratorT> (gen)
        ).template as<cruft::vector> ();
    }


    ///------------------------------------------------------------------------
    /// Returns a uniformly randomly sampled point within the supplied region
    /// using the default thread-local generator.
    template <size_t S, typename T>
    decltype(auto)
    sample (region<S,T> const &shape)
    {
        return sample (shape, cruft::random::generator ());
    }


    ///////////////////////////////////////////////////////////////////////////
    /// Returns a region rotated clockwise about the base point in `steps`
    /// multiples of 90 degrees.
    ///
    /// `steps` must lie in the range [0, 4) so we can avoid an expensive
    /// modulus in the typical case.
    template <typename T>
    cruft::region2<T>
    rotate90 (cruft::region2<T> obj, int steps);


    ///////////////////////////////////////////////////////////////////////////
    template <size_t S, typename T>
    std::ostream& operator<< (std::ostream&, const cruft::region<S,T>&);
}
