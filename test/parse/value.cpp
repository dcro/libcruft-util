#include "parse/value.hpp"
#include "tap.hpp"


///////////////////////////////////////////////////////////////////////////////
int
main (void)
{
    cruft::TAP::logger tap;

    tap.expect_eq (cruft::parse::from_string<long> ("1"), 1L, "parsing long '1'");
    tap.expect_throw<std::invalid_argument> ([] () { cruft::parse::from_string<long> ("a"); }, "parsing long 'a'");

    tap.expect_eq (cruft::parse::from_string<float> ("1"), 1.f, "parsing float '1'");
    tap.expect_throw<std::invalid_argument> ([] () { cruft::parse::from_string<float> ("a"); }, "parsing float 'a'");

    tap.expect_eq (cruft::parse::from_string<bool> ("true"), true, "parsing true");
    tap.expect_eq (cruft::parse::from_string<bool> ("false"), false, "parsing false");

    return tap.status ();
}