/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "cast.hpp"
#include "types/traits.hpp"

#include <type_traits>
#include <iosfwd>


namespace cruft::strongdef {
    /// A transparent wrapper around a (typically lightweight) type for the
    /// purposes of overload disambiguation. It acts like a typesafe typedef.
    template <typename TagT, typename T = typename TagT::index_type>
    class index {
    public:
        using value_type = T;
        using tag_type   = TagT;

        /// We detect underlying_type in a few situations where we emulate
        /// 'enum-like' structures. This lets us opt in to this behaviour.
        using underlying_type = T;

        // TODO: ideally we'd default the constructor, but templated
        // constructors can't be defaulted? So we just stub one out.
        constexpr explicit index () { ; }

        constexpr explicit index (cruft::types::identity_t<T> const &_data): data (_data) { ; }
        constexpr index (index const&) = default;
        constexpr index (index &&) = default;

        index& operator= (T const &) = delete;
        index& operator= (index const &) = default;
        index& operator= (index &&) = default;

        // conversion operators must not be explicit or it defeats the point
        // of this class (ease of use, transparency).
        explicit operator const T& (void) const& { return data; }
        explicit operator       T& (void)      & { return data; }

        constexpr auto operator== (T const &) = delete;
        constexpr auto operator== (index const &rhs) const
        {
            return data == rhs.data;
        }

        constexpr auto operator!= (T const&) = delete;
        constexpr auto operator!= (index const &rhs) const
        {
            return data != rhs.data;
        }

        constexpr auto operator< (T const &) const = delete;
        constexpr auto operator< (index const &rhs) const { return data < rhs.data; }

        constexpr auto operator> (T const &) const = delete;
        constexpr auto operator> (index const &rhs) const { return data > rhs.data; }

        constexpr auto& operator++ (void)& { ++data; return *this; }

        constexpr T      * operator-> ()        &{ return &data; }
        constexpr T const* operator-> ()  const &{ return &data; }

        constexpr T      & operator*  ()        &{ return  data; }
        constexpr T const& operator*  ()  const &{ return  data; }

    private:
        value_type data;
    };


    template <typename>
    struct is_strongdef : public std::false_type {};


    template <typename TagT, typename ValueT>
    struct is_strongdef<strongdef::index<TagT, ValueT>> : public std::true_type {};


    template <typename ValueT>
    constexpr bool is_strongdef_v = is_strongdef<ValueT>::value;


    template <typename TagT, typename T>
    std::ostream&
    operator<< (std::ostream &os, index<TagT,T> const &val)
    {
        return os << +*val;
    }
}


template <typename ContainerT>
static auto indices (ContainerT const &obj)
{
    using index_t = typename ContainerT::index_t;

    struct view {
        view (ContainerT const &_obj):
            m_obj (_obj)
        { ; }

        struct iterator {
            iterator (index_t _idx):
                idx (_idx)
            { ; }

            iterator& operator++ ()
            {
                ++idx;
                return *this;
            }

            index_t const& operator*  () const { return  idx; }
            index_t const* operator-> () const { return &idx; }

            bool operator!= (iterator const &rhs) { return idx != rhs.idx; }

        private:
            index_t idx;
        };

        iterator begin (void) const { return iterator (index_t (0)); }
        iterator end   (void) const
        {
            return index_t (
                cruft::cast::narrow<
                    typename index_t::value_type
                > (
                    m_obj.size ()
                )
            );
        }

    private:
        ContainerT const &m_obj;
    };

    return view {obj};
}


#include "types/description.hpp"
namespace cruft::types {
    template <typename TagT, typename ValueT>
    struct arity_trait<
        ::cruft::strongdef::index<TagT, ValueT>
    > : public arity_trait<ValueT>
    { ; };
};
