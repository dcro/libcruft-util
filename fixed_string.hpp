/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019, Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <algorithm>


///////////////////////////////////////////////////////////////////////////////
namespace cruft {
    /// A simple string-like class that allows constexpr representation of
    /// strings.
    ///
    /// This allows strings to be represented as instances of fixed_string in
    /// template parameters (rather than using vendor specific array
    /// extensions).
    template <std::size_t N>
    struct fixed_string {
        char value[N + 1] {};

        constexpr
        fixed_string (char const _value[N+1]) noexcept
        {
            std::copy_n (_value, N, value);
        }

        constexpr operator char const* () const { return value; }
    };


    template <std::size_t N>
    fixed_string (char const (&)[N]) -> fixed_string<N - 1>;
}
