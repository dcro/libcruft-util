/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010 Danny Robson <danny@nerdcruft.net>
 */

#include "stream.hpp"

#include "debug/assert.hpp"

#include <ostream>

using cruft::stream::null_ostream;
using cruft::stream::bits;


///////////////////////////////////////////////////////////////////////////////
cruft::stream::scoped::flags::flags (std::ios_base &_ios):
    m_ios (_ios),
    m_state (_ios.flags ())
{ ; }


//-----------------------------------------------------------------------------
cruft::stream::scoped::flags::~flags ()
{
    m_ios.flags (m_state);
}


///////////////////////////////////////////////////////////////////////////////
cruft::stream::scoped::precision::precision (std::ios_base &_ios):
    m_ios (_ios),
    m_state (_ios.precision ())
{ ; }


//-----------------------------------------------------------------------------
cruft::stream::scoped::precision::~precision ()
{
    m_ios.precision (m_state);
}


///////////////////////////////////////////////////////////////////////////////
cruft::stream::scoped::width::width (std::ios_base &_ios):
    m_ios (_ios),
    m_state (_ios.width ())
{ ; }


//-----------------------------------------------------------------------------
cruft::stream::scoped::width::~width ()
{
    m_ios.width (m_state);
}


///////////////////////////////////////////////////////////////////////////////
std::ostream&
null_ostream::put (char)
{
    return *this;
}


//-----------------------------------------------------------------------------
bool
null_ostream::good (void) const
{
    return !bad () && !eof () && !fail ();
}


//-----------------------------------------------------------------------------
bool null_ostream::bad  (void) const { return false; }
bool null_ostream::eof  (void) const { return false; }
bool null_ostream::fail (void) const { return false; }


///////////////////////////////////////////////////////////////////////////////
bits::bits (uintmax_t _value, unsigned _count):
    value (_value),
    count (_count)
{
    CHECK_LE (count, sizeof(value) * 8);
}


//-----------------------------------------------------------------------------
std::ostream&
cruft::stream::operator<< (std::ostream &os, cruft::stream::bits b) {
    char digits[sizeof (b.value) * 8 + 1] = { 0 };
    char *cursor = std::end (digits) - 1;

    uintmax_t bits  = b.value;
    unsigned  count = b.count;

    while (count) {
        CHECK_GE (cursor, digits);

        --cursor;
        *cursor = (bits & 0x01) ? '1' : '0';

        bits >>= 1;
        count -= 1;
    }

    os << cursor << "/" << b.count;
    return os;
}
