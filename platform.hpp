/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2012-2015 Danny Robson <danny@nerdcruft.net>
 */

#pragma once


///////////////////////////////////////////////////////////////////////////////
// Determine what compiler we're running
#if defined(__clang__)
    #define COMPILER_CLANG
#elif defined(__GNUC__)
    #define COMPILER_GCC
#else
    #error "Unknown compiler"
#endif


///////////////////////////////////////////////////////////////////////////////
// Determine what operating system we're targetting
#if defined(__ANDROID__)
    #define PLATFORM_ANDROID
    #define PLATFORM_SUFFIX "android"
#elif defined(__linux__) || defined (__linux)
    #define PLATFORM_LINUX
    #define PLATFORM_SUFFIX "linux"
#elif defined(__FreeBSD__)
    #define PLATFORM_FREEBSD
    #define PLATFORM_SUFFIX "freebsd"
#elif defined(__WIN32) || defined (_WIN32)
    #define PLATFORM_WIN32
    #define PLATFORM_SUFFIX "win32"
#else
    #error "Unknown platform"
    #define PLATFORM_SUFFIX "unknown"
#endif


///////////////////////////////////////////////////////////////////////////////
#if defined(COMPILER_GCC) || defined(COMPILER_CLANG)
    #if defined(__x86_64__)
        #define PROCESSOR_AMD64 1
    #elif defined(__arm__)
        #define PROCESSOR_ARM 1
    #else
        #error "Unknown processor"
    #endif
#else
    #error "Unsupported compiler"
#endif