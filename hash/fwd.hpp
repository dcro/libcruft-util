/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016, Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_HASH_FWD_HPP
#define __UTIL_HASH_FWD_HPP

// The single function call hashes are light weight enough to include
// directly, and are unlikely to change given their inherent
// lightweight nature.

#include "fasthash.hpp"
#include "fnv1a.hpp"
#include "murmur.hpp"


// Forward declerations of class based hashes
namespace cruft::hash {
    template <class T> class HMAC;

    // checksums
    class adler32;
    class bsdsum;
    class fletcher;
    class crc;

    class xxhash;
}

#endif
