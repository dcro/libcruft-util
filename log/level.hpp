/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2012-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <cruft/util/preprocessor.hpp>
#include <cruft/util/parse/fwd.hpp>

#include <string>

// Windows.h or one of its friends defines a macro 'ERROR'. Screw Microsoft.
#ifdef ERROR
#undef ERROR
#endif

namespace cruft::log {
    ///////////////////////////////////////////////////////////////////////////
    // rfc5424 log levels. It is assumed they are contiguous to simplify array
    // indexing in logging code.
    enum level_t {
        EMERGENCY,              /** system is unusable */
        ALERT,                  /** action must be taken immediately */
        CRITICAL,               /** critical conditions */
        ERROR,                  /** error conditions */
        WARNING,
        WARN = WARNING,         /** warning conditions */
        NOTICE,                 /** normal but significant condition */
        INFORMATIONAL,
        INFO = INFORMATIONAL,   /** informational messages */
        DEBUG                   /** debug-level messages */
    };

    parse::enumeration::cookie setup_level_reflection (void);

    #define MAP_LEVEL_T(F)  \
    MAP0(F,                 \
        EMERGENCY,          \
        ALERT,              \
        CRITICAL,           \
        ERROR,              \
        WARN,               \
        NOTICE,             \
        INFO,               \
        DEBUG               \
    )

    constexpr auto DEFAULT_LOG_LEVEL = NOTICE;

    //-------------------------------------------------------------------------
    std::string const& to_string (level_t);
    level_t to_level (std::string_view);

    //-------------------------------------------------------------------------
    std::ostream&
    operator<< (std::ostream&, level_t);


    ///////////////////////////////////////////////////////////////////////////
    // Query or set the global logging level filter. If the severity passed to
    // a logging call is less than this then the output is discarded.
    //
    // The initial logging level can be controlled by setting the environment
    // variable LOG_LEVEL to a string corresponding to the names of the values
    // in level_t.
    //
    // Otherwise we fall back to the hard coded default from above.
    //
    // As a special case, a blank value for the environment variable LOG_LEVEL
    // corresponds to the maximum log level. It indicates a preference for
    // minimum output, and may be changed to disable all output in future
    // updates.
    level_t log_level (void);
    level_t log_level (level_t);


    bool needs_break (level_t);


    /// Calculate the maximum width, in characters, of all level names.
    std::size_t level_width (void);
}
