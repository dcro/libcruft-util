/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_HASH_MURMUR_HPP
#define __UTIL_HASH_MURMUR_HPP

#include "murmur/murmur1.hpp"
#include "murmur/murmur2.hpp"
#include "murmur/murmur3.hpp"

#endif
