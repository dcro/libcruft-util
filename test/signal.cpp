#include "signal.hpp"

#include "except.hpp"
#include "tap.hpp"


//-----------------------------------------------------------------------------
void
test_null (cruft::TAP::logger &tap)
{
    tap.expect_nothrow ([] {
        cruft::signal<void(*)(void)> void_signal;
        void_signal ();
    }, "void signal");
}


///////////////////////////////////////////////////////////////////////////////
void
increment_uint (unsigned int &val)
{
    ++val;
}


//-----------------------------------------------------------------------------
void
test_single (cruft::TAP::logger &tap)
{
    unsigned int val = 0;
    cruft::signal<void(*)(unsigned int&)> void_signal;

    auto cookie = void_signal.connect (increment_uint);
    void_signal (val);

    tap.expect_eq (val, 1u, "single listener");
}


//-----------------------------------------------------------------------------
void
test_double (cruft::TAP::logger &tap)
{
    unsigned int val = 0;
    cruft::signal<void(*)(unsigned int&)> void_signal;

    auto cookie0 = void_signal.connect (increment_uint);
    auto cookie1 = void_signal.connect (increment_uint);
    void_signal (val);

    tap.expect_eq (val, 2u, "double listener");
}


///////////////////////////////////////////////////////////////////////////////
void
test_value_signal (cruft::TAP::logger &tap)
{
    cruft::value_signal<unsigned> val;
    unsigned passed = 0;

    auto cookie = val.connect ([&] (unsigned v) { passed = v; });

    val = 42u;
    
    tap.expect_eq (passed, 42u, "value signal, passed value");

    unsigned check = val;
    tap.expect_eq (check, 42u, "value signal, read value");
}


///////////////////////////////////////////////////////////////////////////////
void
test_parallel_release (cruft::TAP::logger &tap)
{
    tap.expect_nothrow ([] {
        using function_t = std::function<void(void)>;
        cruft::signal<function_t> sig;

        cruft::signal<function_t>::cookie a = sig.connect ([&] (void) { a.release (); });
        cruft::signal<function_t>::cookie b = sig.connect ([&] (void) { b.release (); });
        cruft::signal<function_t>::cookie c = sig.connect ([&] (void) { c.release (); });
        cruft::signal<function_t>::cookie d = sig.connect ([&] (void) { d.release (); });

        sig ();
    }, "parallel release in invocation");
}


///////////////////////////////////////////////////////////////////////////////
#include <iostream>


int
main (int, char **)
{
    return cruft::TAP::logger::run ([] (auto &tap) {
        test_null   (tap);
        test_single (tap);
        test_double (tap);
        test_value_signal (tap);
        test_parallel_release (tap);
    });
}
