/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016-2018 Danny Robson <danny@nerdcruft.net>
 */


#include "tap.hpp"

#include "hash/xxhash.hpp"


///////////////////////////////////////////////////////////////////////////////
std::vector<uint8_t>
operator"" _u8s (const char *str, size_t len)
{
    std::vector<uint8_t> res;
    res.resize (len);
    std::copy_n (str, len, std::begin (res));
    return res;
}


///////////////////////////////////////////////////////////////////////////////
int
main (int, char **)
{
    cruft::TAP::logger tap;

    static const struct {
        uint32_t hash32;
        uint64_t hash64;
        unsigned seed;
        std::vector<uint8_t> data;
        const char *msg;
    } TESTS[] = {
        { 0x02cc5d05, 0xef46db3751d8e999, 0, ""_u8s, "empty string" },
        { 0x0b2cb792, 0xd5afba1336a3be4b, 1, ""_u8s, "empty string" },
        { 0x550d7456, 0xd24ec4f1a98c6e5b, 0, "a"_u8s, "single a" },
        { 0xf514706f, 0xdec2bc81c3cd46c6, 1, "a"_u8s, "single a" },
        { 0x32d153ff, 0x44bc2cf5ad770999, 0, "abc"_u8s, "abc" },
        { 0xaa3da8ff, 0xbea9ca8199328908, 1, "abc"_u8s, "abc" },
        { 0x54ca7e46, 0x892a0760a6343391, 0x1234,
          "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+"_u8s,
          "long alphabet" }
    };


    for (const auto &t: TESTS) {
        cruft::hash::xxhash32 h32 (t.seed);
        cruft::hash::xxhash64 h64 (t.seed);

        tap.expect_eq (h32 (t.data), t.hash32, "xxhash32 %s", t.msg);
        tap.expect_eq (h64 (t.data), t.hash64, "xxhash64 %s, seed %!", t.msg, t.seed);
    }

    return tap.status ();
}
