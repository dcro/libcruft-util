/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#include <array>
#include <cstddef>
#include <cstdint>

///////////////////////////////////////////////////////////////////////////////
namespace cruft::hash::murmur {
    template <typename T>
    T
    tail (const uint8_t *restrict data, size_t len);

    template <typename T>
    std::array<T,16/sizeof(T)>
    tail_array (const uint8_t *restrict data, size_t len);
}
