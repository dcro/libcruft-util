/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "fwd.hpp"

#include "../point.hpp"

#include <cstddef>


namespace cruft::geom {
    template <typename ShapeT>
    struct shape_traits { };


    template <std::size_t S, typename T>
    struct shape_traits <::cruft::region<S,T>> {
        static constexpr auto elements = S;
        using value_type = T;

        using point_type = cruft::point<S,T>;
    };
}
