#include "thread/event.hpp"

#include "tap.hpp"

#include <atomic>
#include <thread>


int
main ()
{
    cruft::TAP::logger tap;

    // create an event which a thread will wait on. after it's been woken up
    // it will modify the contents of val. this will be tested at a couple of
    // points for consistency.
    //
    // the test isn't 100% deterministic (because we're attempting to create
    // specific timings by just waiting). but it's a decent first check.
    std::atomic<int> val = 0;
    cruft::thread::event a;

    std::thread t ([&] () {
        a.wait ();
        ++val;
    });

    // block for hopefully long enough to allow the above thread to change
    // the value of the integer.
    std::this_thread::sleep_for (std::chrono::milliseconds (100));

    tap.expect_eq (val, 0, "waiting actually blocks");

    a.notify_all ();
    t.join ();

    tap.expect_eq (val, 1, "notification released the lock");

    return tap.status ();
}
