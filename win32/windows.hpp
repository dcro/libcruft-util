#pragma once

#include "../platform.hpp"

// Include various useful Windows headers in a way that won't break anything
// that isn't aware of Microsoft's propensity to define away large swathes of
// frequently used symbols.

#if defined(_WINDOWS_)
#error "windows headers have already been included"
#endif

// Based off the approach outlined at:
// https://aras-p.info/blog/2018/01/12/Minimizing-windows.h/


// Predefine the hardware platform macros that the headers will expect.
#if defined(PROCESSOR_AMD64)
    #if !defined(_AMD64_)
        #define _AMD64_
    #endif
#elif defined(PROCESSOR_X86)
    #if !defined(_X86_)
        #define _X86_
    #endif
#elif defined(PROCESSOR_ARM)
    #if !defined(_ARM_)
        #define _ARM_
    #endif
#else
    #error "Unsupported processr"
#endif


// Request a smaller header. It probably won't help too much, but it shouldn't
// hurt us either.
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <windef.h>

#include <fileapi.h>
#include <synchapi.h>
#include <debugapi.h>
#include <memoryapi.h>
#include <handleapi.h>
#include <processthreadsapi.h>
#include <errhandlingapi.h>

// GDI isn't used in many locations but it's easier to include it here and
// undefine various pieces of trash than let unsuspecting code get trampled
// later.
#include <wingdi.h>

#undef near
#undef far
#undef NEAR
#undef FAR

#undef RELATIVE
#undef ABSOLUTE

#undef ERROR

#undef TRANSPARENT
#undef OPAQUE

