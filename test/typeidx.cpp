#include "../tap.hpp"

#include "../typeidx.hpp"


///////////////////////////////////////////////////////////////////////////////
int
main (int, char**)
{
    cruft::TAP::logger tap;

    tap.expect_eq (cruft::typeidx<int> (), cruft::typeidx<int> (), "equality for int");
    tap.expect_eq (cruft::typeidx<float> (), cruft::typeidx<float> (), "equality for float");
    tap.expect_neq (cruft::typeidx<int> (), cruft::typeidx<float> (), "inequality for int/float");

    struct tag_a {};
    struct tag_b {};

    // Register an unequal number of types (that don't include f32) to each
    // tag before we get to testing in the expectation that will force f32 to
    // be different in the following tests.
    cruft::typeidx<i16, tag_a> ();
    cruft::typeidx<i32, tag_a> ();
    cruft::typeidx<i08, tag_b> ();
    cruft::typeidx<i16, tag_b> ();
    cruft::typeidx<i32, tag_b> ();

    // We test using f32 rather than int because we're pretty sure it's not
    // going to have the first index (and hence give us false results) given
    // int was registered first just above here.
    tap.expect_neq (
        cruft::typeidx<f32,tag_a> (),
        cruft::typeidx<f32> (),
        "inequality for f32/f32, tag_a"
    );

    tap.expect_neq (
        cruft::typeidx<f32,tag_a> (),
        cruft::typeidx<f32,tag_b> (),
        "inequality for f32/f32, tag_a/tag_b"
    );

    tap.expect_neq (
        cruft::typeidx<f32,tag_a> (),
        cruft::typeidx<f32, tag_b> (),
        "equality for f32/f32, tag_a/tag_a"
    );

    return tap.status ();
}