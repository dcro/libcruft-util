/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../std.hpp"

#include <cstddef>
#include <memory>


namespace cruft::buffer {
    /// Implements a trivial statically allocated memory buffer with a fixed
    /// construction time and no other fancy tricks (just something trivial
    /// like new/delete).
    ///
    /// The actual allocation size is guaranteed to be _at least_ the
    /// specified size. It may be larger if the underlying implementation
    /// requires page alignment or other system specific details.
    ///
    /// Useful as a trivial universal implementation of the buffer concept.
    class simple {
    public:
        using value_type = u08;

        explicit simple (size_t _size);

        simple (const simple&) = delete;
        simple (simple &&) = default;
        simple& operator= (const simple&) = delete;
        simple& operator= (simple &&);

        value_type* begin (void)&;
        value_type* end   (void)&;

        value_type const* cbegin (void)&;
        value_type const* cend   (void)&;

        value_type const* begin (void) const&;
        value_type const* end   (void) const&;

        std::size_t size (void) const;
        std::size_t capacity (void) const;

    private:
        std::unique_ptr<u08[]> m_base;
        std::size_t m_size;
    };
}
