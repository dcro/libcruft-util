/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_TERM_HPP
#define __UTIL_TERM_HPP

#include <iosfwd>

namespace cruft::term {
    bool has_csi_support (void);

    namespace csi {
        struct code {
            static constexpr char CSI = '\x1B';
        };


        struct graphics : public code {
            static constexpr char terminator = 'm';

            enum layer : char {
                FOREGROUND = 30,
                BACKGROUND = 40
            };

            enum hue : char {
                BLACK   = 0,
                RED     = 1,
                GREEN   = 2,
                YELLOW  = 3,
                BLUE    = 4,
                MAGENTA = 5,
                CYAN    = 6,
                WHITE   = 7,
            };

            graphics (layer, hue);
            explicit graphics (char value);

            char value (void) const;
            char value (char);

            static const graphics RESET;

        private:
            char m_value;
        };


        std::ostream& operator<< (std::ostream&, code);
        std::ostream& operator<< (std::ostream&, graphics);
    }
}

#endif
