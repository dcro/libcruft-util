#include "coord/simd.hpp"
#include "tap.hpp"


int
main ()
{
    using simd_t = cruft::coord::simd<4,float>;

    cruft::TAP::logger tap;

    {
        const simd_t a (1,2,3,4);
        const simd_t b (4,1,3,2);
        const float res = dot (a, b)[0];
        tap.expect_eq (res, 4+2+9+8, "trivial dot product");
    }

    {
        const simd_t a (1, 2, 3, 4);
        const simd_t b (0, 3, 3, 9);

        const auto lo = min (a, b);
        const auto hi = max (a, b);

        tap.expect_eq (lo, simd_t {0,2,3,4}, "vector minimum");
        tap.expect_eq (hi, simd_t {1,3,3,9}, "vector maximum");
    }

    {
        const simd_t val { -INFINITY, INFINITY, 0, -9 };
        tap.expect_eq (abs (val), simd_t {INFINITY,INFINITY,0,9}, "absolute value");
    }

    {
        const simd_t test { -1.25f,  1.25f, 0.f, 1.f };
        const auto lo = floor (test);
        const auto hi = ceil  (test);

        tap.expect_eq (lo, simd_t { -2, 1, 0, 1 }, "floor");
        tap.expect_eq (hi, simd_t { -1, 2, 0, 1 }, "ceil");
    };

    tap.expect_eq (simd_t {1,2,3,4}.x, 1.f, "named accessor, x");
    tap.expect_eq (simd_t {1,2,3,4}.y, 2.f, "named accessor, y");
    tap.expect_eq (simd_t {1,2,3,4}.z, 3.f, "named accessor, z");
    tap.expect_eq (simd_t {1,2,3,4}.w, 4.f, "named accessor, w");

    return tap.status ();
}