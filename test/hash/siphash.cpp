
#include "tap.hpp"

#include "hash/siphash.hpp"


///////////////////////////////////////////////////////////////////////////////
static const struct {
    std::array<uint64_t,2> key;
    std::vector<uint8_t> data;
    uint64_t digest;
    const char *message;
} TESTS[] = {
    {
        { 0x0706050403020100, 0x0f0e0d0c0b0a0908 },
        { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
          0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e
        },
        0xa129ca6149be45e5,
        "byte sequence",
    }
};


///////////////////////////////////////////////////////////////////////////////
int
main ()
{
    cruft::TAP::logger tap;

    for (const auto &t: TESTS) {
        cruft::hash::siphash<2,4> h (t.key);
        tap.expect_eq (h (t.data), t.digest, "%s", t.message);
    }

    return tap.status ();
}