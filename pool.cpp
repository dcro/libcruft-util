/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011 Danny Robson <danny@nerdcruft.net>
 */


#include "pool.hpp"


// Explicitly instance a possibly useful specialisation so that we can more easily catch linker errors.
template class cruft::pool<std::string>;
