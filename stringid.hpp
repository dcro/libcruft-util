/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2014-2018 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_UTIL_STRINGID_HPP
#define CRUFT_UTIL_STRINGID_HPP

#include "view.hpp"

#include <map>
#include <string>

namespace cruft {
    class stringid {
    public:
        typedef size_t id_t;

        ///////////////////////////////////////////////////////////////////////
        /// Returns the ID for the supplied string, or creates and returns a
        /// new ID if the string is not present.
        id_t operator[] (std::string_view const&);

        // provided for symmetry with std member names
        id_t at (std::string_view const &name) const { return find (name); }


        ///////////////////////////////////////////////////////////////////////
        id_t add  (std::string);


        //---------------------------------------------------------------------
        template <typename T>
        id_t add (cruft::view<T> key)
        {
            return add (
                std::string{
                    std::cbegin (key),
                    std::cend (key)
                }
            );
        }


        ///////////////////////////////////////////////////////////////////////
        id_t find (std::string_view const&) const;


        //---------------------------------------------------------------------
        template <typename T>
        id_t find (cruft::view<T> key) const
        {
            return find (
                std::string_view {
                    std::cbegin (key),
                    std::cend (key)
                }
            );
        }


        ///////////////////////////////////////////////////////////////////////
        void clear (void);


    private:
        std::map<std::string const, id_t, std::less<>> m_map;
    };
}

#endif
