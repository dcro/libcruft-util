/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2016 Danny Robson <danny@nerdcruft.net>
 */

#include "exe.hpp"

#include "win32/except.hpp"
#include "win32/windows.hpp"

#include <filesystem>
#include <vector>

#include <libloaderapi.h>


///////////////////////////////////////////////////////////////////////////////
std::filesystem::path
cruft::image_path (void)
{
        std::vector<char> resolved (256);

retry:
        const auto written = GetModuleFileName (
            nullptr,
            resolved.data (),
            static_cast<DWORD> (resolved.size ())
        );
        if (written == 0)
            win32::error::throw_code ();

        if (written == resolved.size ()) {
            resolved.resize (resolved.size () * 2);
            goto retry;
        }

        return std::filesystem::path (resolved.data (), resolved.data () + written);
}
