#include "tap.hpp"

#include "array/darray.hpp"

int
main (int, char**)
{
    cruft::TAP::logger tap;

    {
        cruft::darray<8,int> val;
        tap.expect (val.empty (), "default initialisation is empty");
    }

    {
        cruft::darray<8,int> val ({ 0, 1, 2, 3, 4, 5, 6, 7});

        tap.expect_eq (val.size (), 8u, "query for size");

        tap.expect_eq (val.capacity (), decltype(val)::elements, "static vs dynamic query for capacity");

        {
            auto const sum = std::accumulate (val.cbegin (), val.cend (), 0);
            tap.expect_eq (sum, 28, "accumulate over initializer_list range");
        }

        {
            val.erase (val.begin () + 2);
            tap.expect_eq (val.size (), 7u, "erasing removes one element");

            auto const sum = std::accumulate (val.cbegin (), val.cend (), 0);
            tap.expect_eq (sum, 26, "erase one element");
        }

        {
            // Try inserting a value and see if it sticks.
            //
            // Ensure that a unique value is used here.
            // Perform an accumulate to check the rest of the values are there too.
            val.insert (val.begin () + 4, 13);
            tap.expect_eq (val[4], 13, "inserted values matches indexed value");
            auto const sum = std::accumulate (val.cbegin (), val.cend (), 0);
            tap.expect_eq (sum, 39, "accumulate over new array suggests all still present");
        }
    }


    {
        cruft::darray<8,int>       val ({ 0, 1,             2, 3 });
        cruft::darray<8,int> const res ({ 0, 1, 13, 13, 13, 2, 3 });

        val.insert (val.begin () + 2, 3, 13);
        tap.expect_eq (val, res, "trivial insert_n");
    }


    return tap.status ();
}