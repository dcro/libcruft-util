/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2016 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_TIME_HPP
#define __UTIL_TIME_HPP

#include "stats.hpp"

#include <chrono>
#include <cstdint>
#include <string>


namespace cruft {
    ///////////////////////////////////////////////////////////////////////////
    uintmax_t nanoseconds (void);


    //-------------------------------------------------------------------------
    template <typename T>
    void sleep (std::chrono::duration<T,std::nano> dt)
    {
        auto nano = std::chrono::duration_cast<std::chrono::nanoseconds> (dt);
        sleep (nano.count ());
    }


    //-------------------------------------------------------------------------
    void sleep (uint64_t ns);


    ///////////////////////////////////////////////////////////////////////////
    class delta_clock {
        public:
            delta_clock ();
            std::chrono::nanoseconds dt(void);

        protected:
            struct {
                uint64_t prev;
                uint64_t curr;
            } time;
    };


    ///////////////////////////////////////////////////////////////////////////
    class period_query {
        public:
            explicit period_query (float seconds);

            bool poll (void);

        protected:
            struct {
                uint64_t start;
                uint64_t period;
            } m_time;
    };


    ///////////////////////////////////////////////////////////////////////////
    class rate_limiter {
        public:
            explicit rate_limiter (unsigned rate);

            void poll (void);

        protected:
            uint64_t m_last;
            unsigned m_target;
    };


    ///////////////////////////////////////////////////////////////////////////
    class polled_duration {
        public:
            polled_duration (std::string name, uint64_t interval);

            void start (void);
            void stop  (void);

        protected:
            std::string m_name;
            uint64_t    m_last;
            uint64_t    m_interval;
            uint64_t    m_next;

            stats::accumulator<uint64_t> m_series;
    };
}

#endif
