/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#include "si.hpp"

#include "value.hpp"

#include "../std.hpp"

#include "preprocessor.hpp"


///////////////////////////////////////////////////////////////////////////////
template <typename ValueT>
cruft::expected<ValueT, std::errc>
cruft::parse::si (cruft::view<char const*> const src)
{
    auto remain = src;
    ValueT dst = cruft::parse::value<ValueT> (remain);

    switch (remain.size ()) {
    case 0:
        return dst;

    case 1:
        switch (remain[0]) {
            case 'K': case 'k': return dst * 1024;
            case 'M': case 'm': return dst * 1024 * 1024;
            case 'G': case 'g': return dst * 1024 * 1024 * 1024;
            case 'T': case 't': return dst * 1024 * 1024 * 1024 * 1024;
        }

    default:
        return cruft::unexpected (std::errc::invalid_argument);
    }
}


///////////////////////////////////////////////////////////////////////////////
#define INSTANTIATE(KLASS) template cruft::expected<KLASS,std::errc> cruft::parse::si (cruft::view<char const*>);


MAP0 (INSTANTIATE,
    u16, u32, u64,
    i16, i32, i64,
         f32, f64
)
