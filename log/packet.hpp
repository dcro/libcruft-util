#pragma once

#include "level.hpp"
#include "../format.hpp"

#include <chrono>

#include <cstddef>


namespace cruft::log {
    struct packet {
        using clock = std::chrono::high_resolution_clock;

        packet (level_t _level, std::string _message)
            : level (_level)
            , message (std::move (_message))
            , timestamp (clock::now ())
        { ; }


        template <typename FormatT, typename ...ArgsT>
        packet (
            level_t _level,
            FormatT _format,
            ArgsT &&..._args
        ) : packet (
            _level,
            cruft::format::to_string (
                cruft::format::printf (
                    std::forward<FormatT> (_format)
                ) (
                    std::forward<ArgsT> (_args)...
                )
            )
        )
        { ; }

        level_t level;
        std::string message;
        clock::time_point timestamp;
    };
}