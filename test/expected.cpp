#include "../expected.hpp"

#include "../tap.hpp"



int main ()
{
    cruft::TAP::logger tap;

    tap.expect_nothrow (
        [] () { (void)cruft::expected<std::string,int> { "foo" }; },
        "value construction succeeds"
    );

    tap.expect_nothrow (
        [] () { (void)cruft::expected<std::string,int> { EPERM }; },
        "error construction succeeds"
    );

    tap.expect_eq (cruft::expected<std::string,int> { "foo" }.value (), "foo", "value matches");
    tap.expect_eq (cruft::expected<std::string,int> { EPERM }.error (), EPERM, "error matches");

    tap.expect_throw<cruft::bad_expected_access> (
        [] () {
            cruft::expected<std::string,int> val { EPERM };
            val.value ();
        },
        "value access fails when holding an error"
    );

    tap.expect_throw<cruft::bad_expected_access> (
        [] () {
            cruft::expected<std::string,int> val { "foo" };
            val.error ();
        },
        "error access fails when holding a value"
    );

    return tap.status ();
}