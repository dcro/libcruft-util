/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#include "frustum.hpp"

using cruft::geom::frustum;


///////////////////////////////////////////////////////////////////////////////
template <typename T>
frustum<T>::frustum (const matrix4<T> &transform)
{
    // left
    planes[X_NEG].coefficients[0] = transform[3][0] + transform[0][0];
    planes[X_NEG].coefficients[1] = transform[3][1] + transform[0][1];
    planes[X_NEG].coefficients[2] = transform[3][2] + transform[0][2];
    planes[X_NEG].coefficients[3] = transform[3][3] + transform[0][3];

    // Right clipping plane
    planes[X_POS].coefficients[0] = transform[3][0] - transform[0][0];
    planes[X_POS].coefficients[1] = transform[3][1] - transform[0][1];
    planes[X_POS].coefficients[2] = transform[3][2] - transform[0][2];
    planes[X_POS].coefficients[3] = transform[3][3] - transform[0][3];

    // Top clipping plane
    planes[Y_POS].coefficients[0] = transform[3][0] - transform[1][0];
    planes[Y_POS].coefficients[1] = transform[3][1] - transform[1][1];
    planes[Y_POS].coefficients[2] = transform[3][2] - transform[1][2];
    planes[Y_POS].coefficients[3] = transform[3][3] - transform[1][3];

    // Bottom clipping plane
    planes[Y_NEG].coefficients[0] = transform[3][0] + transform[1][0];
    planes[Y_NEG].coefficients[1] = transform[3][1] + transform[1][1];
    planes[Y_NEG].coefficients[2] = transform[3][2] + transform[1][2];
    planes[Y_NEG].coefficients[3] = transform[3][3] + transform[1][3];

    // Near clipping plane
    planes[Z_POS].coefficients[0] = transform[3][0] + transform[2][0];
    planes[Z_POS].coefficients[1] = transform[3][1] + transform[2][1];
    planes[Z_POS].coefficients[2] = transform[3][2] + transform[2][2];
    planes[Z_POS].coefficients[3] = transform[3][3] + transform[2][3];

    // Far clipping plane
    planes[Z_NEG].coefficients[0] = transform[3][0] - transform[2][0];
    planes[Z_NEG].coefficients[1] = transform[3][1] - transform[2][1];
    planes[Z_NEG].coefficients[2] = transform[3][2] - transform[2][2];
    planes[Z_NEG].coefficients[3] = transform[3][3] - transform[2][3];

    for (auto &p: planes)
        p = normalised (p);
}


///////////////////////////////////////////////////////////////////////////////
template struct cruft::geom::frustum<float>;
