/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <cstddef>

namespace cruft::alloc {
    template <typename ValueT, typename AllocatorT>
    struct std {
        using value_type = ValueT;
        using size_type = std::size_t;
        using difference_type = std::ptrdiff_t;

        [[nodiscard]] ValueT* allocate (std::size_t n)
        {
            return m_allocator.allocate (n * sizeof (ValueT), alignof (ValueT));
        }

        void
        deallocate (ValueT *ptr, std::size_t n)
        {
            m_allocator.deallocate (ptr, n * sizeof (ValueT), alignof (ValueT));
        }

    private:
        AllocatorT m_allocator;
    };
}