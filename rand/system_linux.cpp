/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#include "system_linux.hpp"

#include "../posix/except.hpp"
#include "../io.hpp"

#include <sys/ioctl.h>
#include <linux/random.h>


using cruft::rand::file;


///////////////////////////////////////////////////////////////////////////////
static std::string const& default_source (void)
{
    static std::string const s_path = "/dev/urandom";
    return s_path;
}


//-----------------------------------------------------------------------------
file::file ():
    file (default_source ())
{ ; }


//-----------------------------------------------------------------------------
file::file (std::string const &token)
    : m_fd (token, O_RDONLY | O_BINARY)
{ ; }


///////////////////////////////////////////////////////////////////////////////
file::result_type
file::operator() (void)
{
    result_type res;
    cruft::read (m_fd, cruft::make_byte_view<u08> (res));
    return res;
}


///////////////////////////////////////////////////////////////////////////////
double
file::entropy () const noexcept
{
    int val;

    cruft::posix::error::try_code (
        ioctl (m_fd.native (), RNDGETENTCNT, &val)
    );

    return val < 0 ? 0. : double (val);
}
