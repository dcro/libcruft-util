/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_STATS_HPP
#define __UTIL_STATS_HPP

#include <iosfwd>


namespace cruft {
    namespace stats {
        // Undefined until at least one value has been added.

        template <typename T>
        struct accumulator {
            accumulator ();

            void reset (void);

            void add (T);
            void add (const accumulator<T> &);

            size_t count;

            T min;
            T max;
            T sum;

            T range (void) const;
            T mean  (void) const;
        };

        template <typename T>
        std::ostream& operator<< (std::ostream&, const accumulator<T>&);
    }
}

#endif
