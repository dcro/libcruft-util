#include "../platform.hpp"

#if defined(PLATFORM_LINUX)
#include "semaphore_linux.hpp"
#elif defined(PLATFORM_WIN32)
#include "semaphore_win32.hpp"
#else
#error "Unsupported threading platform"
#endif
