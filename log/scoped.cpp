/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2012-2019 Danny Robson <danny@nerdcruft.net>
 */

#include "scoped.hpp"

#include "../time.hpp"

using cruft::log::scoped_logger;
using cruft::log::scoped_timer;


///////////////////////////////////////////////////////////////////////////////
cruft::log::scoped_logger::scoped_logger (
    level_t _level,
    std::string   _message
):
    m_level   (_level),
    m_message (std::move (_message))
{ ; }


//-----------------------------------------------------------------------------
cruft::log::scoped_logger::~scoped_logger ()
{
    write (m_level, m_message);
}


///////////////////////////////////////////////////////////////////////////////
cruft::log::scoped_timer::scoped_timer (
    cruft::log::level_t _level,
    std::string   _message
):
    m_level (_level),
    m_message (std::move (_message)),
    m_start (cruft::nanoseconds ())
{ ; }


//-----------------------------------------------------------------------------
cruft::log::scoped_timer::~scoped_timer ()
{
    auto finish = cruft::nanoseconds ();
    auto duration = finish - m_start;

    write (
        m_level,
        "%fs, %s",
        float (duration) / 1'000'000'000.f,
        m_message
    );
}
