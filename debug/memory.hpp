/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../view.hpp"

namespace cruft::debug::memory {
    /// Returns the required alignment for instrumented memory accesses.
    std::align_val_t instrumentation_alignment (void);


    /// Label the contents of this memory region as undefined.
    void mark_undefined (void const volatile*, std::size_t);


    //-------------------------------------------------------------------------
    template <typename ValueT>
    void mark_undefined (cruft::view<ValueT const *> mem)
    {
        return mark_undefined (mem.data (), mem.size () * sizeof (ValueT));
    }


    /// Label the contents of this memory region as defined
    void mark_defined (void const volatile*, std::size_t);


    //-------------------------------------------------------------------------
    template <typename ValueT>
    void mark_defined (cruft::view<ValueT const *> mem)
    {
        return mark_defined (mem.data (), mem.size () * sizeof (ValueT));
    }
}
