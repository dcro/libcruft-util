/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016-2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "./traits.hpp"
#include "../iterator/infix.hpp"

#include <cstddef>
#include <ostream>
#include <algorithm>

namespace cruft {
    template <
        typename K,
        typename = std::enable_if_t<is_coord_v<K>,void>
    >
    std::ostream&
    operator<< (std::ostream &os, const K &k)
    {
        os << "[";
        std::transform (std::cbegin (k),
                        std::cend   (k),
                        iterator::infix_iterator<typename K::value_type> (os, ", "),
                        [] (auto i) { return +i; });
        os << "]";

        return os;
    }
}
