/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2010-2016 Danny Robson <danny@nerdcruft.net>
 */

#include "time.hpp"

#include <chrono>
#include <thread>


///////////////////////////////////////////////////////////////////////////////
void
cruft::sleep (uint64_t ns)
{
    std::this_thread::sleep_for (std::chrono::nanoseconds (ns));
}
