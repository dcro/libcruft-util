#include "ostream.hpp"

#include "../platform.hpp"

#include <ostream>
#include <iomanip>

#include <sys/stat.h>


///////////////////////////////////////////////////////////////////////////////
struct type_printer {
    type_printer (struct stat const &_data)
        : data (_data)
    { ; }

    struct stat const &data;
};


//-----------------------------------------------------------------------------
std::ostream& operator<< (std::ostream &os, type_printer const &val)
{
    os << "[ ";
#ifndef PLATFORM_WIN32
    if (val.data.st_mode & S_ISUID) os << "SUID, ";
    if (val.data.st_mode & S_ISGID) os << "SGID, ";
    if (val.data.st_mode & S_ISVTX) os << "STICKY, ";
#endif

    os << "{ user: "  << (val.data.st_mode & S_IRUSR ? 'r' : '_')
        << (val.data.st_mode & S_IWUSR ? 'w' : '_')
        << (val.data.st_mode & S_IXUSR ? 'x' : '_')
        << " }, "
        << "{ group: " << (val.data.st_mode & S_IRGRP ? 'r' : '_')
        << (val.data.st_mode & S_IWGRP ? 'w' : '_')
        << (val.data.st_mode & S_IXGRP ? 'x' : '_')
        << " }, "
        << "{ group: " << (val.data.st_mode & S_IROTH ? 'r' : '_')
        << (val.data.st_mode & S_IWOTH ? 'w' : '_')
        << (val.data.st_mode & S_IXOTH ? 'x' : '_')
        << " }";

    return os;
}


///////////////////////////////////////////////////////////////////////////////
struct mode_printer {
    mode_printer (struct stat const &_data)
        : data (_data)
    { ; }

    struct stat const &data;
};


//-----------------------------------------------------------------------------
std::ostream& operator<< (std::ostream &os, mode_printer const &val)
{
    return S_ISREG  (val.data.st_mode) ? os << "REGULAR"
         : S_ISDIR  (val.data.st_mode) ? os << "DIRECTORY"
         : S_ISCHR  (val.data.st_mode) ? os << "CHARACTER"
         : S_ISBLK  (val.data.st_mode) ? os << "BLOCK"
         : S_ISFIFO (val.data.st_mode) ? os << "FIFO"
#ifndef PLATFORM_WIN32
         : S_ISLNK  (val.data.st_mode) ? os << "SYMLINK"
         : S_ISSOCK (val.data.st_mode) ? os << "SOCKET"
#endif
         : (throw std::invalid_argument ("Unhandled mode_t"), os << "_error");
}


//-----------------------------------------------------------------------------
std::ostream& operator<< (std::ostream &os, struct stat const &val)
{
    return os << "{ dev: "  << val.st_dev
        << ", ino: "  << val.st_ino
        << ", type: " << type_printer (val)
        << ", mode: " << mode_printer (val)
        << ", uid: "  << val.st_uid
        << ", gid: "  << val.st_gid
        << ", size: " << val.st_size
#ifndef PLATFORM_WIN32
        << ", blocksize: " << val.st_blksize
#endif
        << " }";
}
