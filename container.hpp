/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2020, Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <algorithm>
#include <utility>

namespace cruft {
    /// Tests if a value is present in a container.
    ///
    /// Defaults to a naive search of the container.
    ///
    /// \return true if the value is present in the container, otherwise false.
    template <typename ContainerT, typename ValueT>
    bool
    contains (ValueT &&value, ContainerT &&container)
    {
        auto const pos = std::find (
            std::cbegin (container),
            std::cend   (container),
            std::move (value)
        );

        return pos != std::cend (container);
    }
}