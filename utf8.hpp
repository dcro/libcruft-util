/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2017 Danny Robson <danny@nerdcruft.net>
 */


#ifndef CRUFT_UTIL_UTF8_HPP
#define CRUFT_UTIL_UTF8_HPP

#include "./view.hpp"

#include <cstddef>
#include <cstdint>
#include <vector>


namespace cruft::utf8 {
    ///////////////////////////////////////////////////////////////////////////
    using codepoint_t = uint32_t;


    ///////////////////////////////////////////////////////////////////////////
    std::vector<codepoint_t>
    decode (cruft::view<const std::byte*>);


    //-------------------------------------------------------------------------
    inline auto
    decode (cruft::view<const char*> data)
    {
        return decode ({
            reinterpret_cast<const std::byte*> (data.cbegin ()),
            reinterpret_cast<const std::byte*> (data.cend   ())
        });
    }

    inline auto
    decode (cruft::view<const uint8_t*> data)
    {
        return decode ({
            reinterpret_cast<const char*> (data.cbegin ()),
            reinterpret_cast<const char*> (data.cend   ())
        });
    }


    ///////////////////////////////////////////////////////////////////////////
    std::vector<std::byte>
    encode (cruft::view<const char*>);


    ///////////////////////////////////////////////////////////////////////////
    struct error : public std::exception {};

    struct malformed_error : public error { };
    struct illegal_codepoint : public malformed_error {};

    struct overlong_error  : public error { };
}

#endif
