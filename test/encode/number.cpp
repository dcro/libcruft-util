#include "encode/number.hpp"
#include "tap.hpp"


int main ()
{
    static struct {
        char const *str;
        unsigned value;
    } const TESTS[] = {
        {      "0",        0 },
        {      "a",       10 },
        {      "A",       10 },
        {     "10",       36 },
        {     "11",       37 },
        {   "bfi0",   533304 },
        { "15bfi0", 69397560 },
    };

    cruft::TAP::logger tap;

    for (auto const &t: TESTS) {
        // decode the truth string and test against the truth number
        cruft::view src (t.str);
        tap.expect_eq (cruft::encode::decode36<unsigned> (src), t.value, "base36 decode '%!'", t.str);

        // encode the truth number
        std::ostringstream dst;
        cruft::encode::encode36<unsigned> (std::ostream_iterator<char> (dst), t.value);

        // covert computed and truth strings to lower case
        std::string encoded_computed = dst.str ();
        std::string encoded_truth    = t.str;
        std::transform (encoded_computed.begin (), encoded_computed.end (), encoded_computed.begin (), ::tolower);
        std::transform (encoded_truth.begin (), encoded_truth.end (), encoded_truth.begin (), ::tolower);

        // compare lower case encoded strings
        tap.expect_eq (encoded_computed, encoded_truth, "base36 encode '%!'", t.value);
    }

    return tap.status ();
}