/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#include "system_win32.hpp"

#include "win32/except.hpp"

#include <windows.h>
#include <ntsecapi.h>

using cruft::rand::win32::rtlgenrandom;


///////////////////////////////////////////////////////////////////////////////
rtlgenrandom::rtlgenrandom ()
{ ; }


//-----------------------------------------------------------------------------
rtlgenrandom::rtlgenrandom (std::string const &)
{ ; }


///////////////////////////////////////////////////////////////////////////////
rtlgenrandom::result_type
rtlgenrandom::operator() ()
{
    result_type res;
    if (RtlGenRandom (&res, sizeof (res)) != TRUE)
        cruft::win32::error::throw_code ();
    return res;
}
