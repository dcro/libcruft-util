#include <iostream>

#include "json/tree.hpp"

enum {
    ARG_SELF,
    ARG_A,
    ARG_B,

    NUM_ARGS
};


int
main (int argc, char **argv)
{
    if (NUM_ARGS != argc) {
        std::cerr << "usage: " << argv[ARG_SELF] << " <a> <b>\n";
        return -1;
    }

    auto const &a = json::tree::parse (argv[ARG_A]);
    auto const &b = json::tree::parse (argv[ARG_B]);

    return *a == *b ? 0 : 1;
}