/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016-2018 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_UTIL_HASH_SIPHASH_HPP
#define CRUFT_UTIL_HASH_SIPHASH_HPP

#include "../view.hpp"

#include <cstdint>
#include <type_traits>

namespace cruft::hash {
    template <int CompressionsV = 2, int FinalisationsV = 4>
    class siphash {
    public:
        using digest_t = std::uint64_t;

        static constexpr auto compression_rounds = CompressionsV;
        static constexpr auto finalisation_rounds = FinalisationsV;

        siphash (std::array<uint64_t,2>) noexcept;

        digest_t operator() (cruft::view<const std::uint8_t*> data) const noexcept;

    private:
        std::array<uint64_t,2> m_key;
    };
};

#endif
