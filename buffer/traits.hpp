/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <type_traits>


namespace cruft::buffer {
    ///////////////////////////////////////////////////////////////////////////
    /// A trait that evaluates to true if the queried type models cruft::allocator
    template <
        typename BufferT,
        typename = std::void_t<>
    >
    struct is_buffer
        : public std::false_type
    { };


    //-------------------------------------------------------------------------
    template <typename BufferT>
    struct is_buffer<BufferT,
        std::void_t<
            // Provides aligned and unaligned allocation
            decltype(std::declval<BufferT> ().begin ()),
            decltype(std::declval<BufferT> ().end   ()),
            decltype(std::declval<BufferT> ().size  ()),
            void
        >
    > : public std::true_type
    { };


    //-------------------------------------------------------------------------
    template <typename BufferT>
    constexpr auto is_buffer_v = is_buffer<BufferT>::value;
}
