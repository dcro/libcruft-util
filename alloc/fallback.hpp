/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2017 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../view.hpp"

#include <cstddef>
#include <tuple>

namespace cruft::alloc {
    /// A raw memory allocator that allocates memory series of child
    /// allocators, preferring earlier allocators.
    template <typename ...ChildT>
    class fallback {
    public:
        fallback (ChildT &..._children):
            m_children (_children...)
        { ; }

        template <typename T>
        cruft::view<T*>
        allocate (size_t bytes);

        template <typename T>
        cruft::view<T*>
        allocate (size_t bytes, size_t align);

        template <typename T>
        void deallocate (cruft::view<std::byte*> ptr);

    private:
        std::tuple<ChildT&...> m_children;
    };
}
