/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../platform.hpp"

#if !defined(PROCESSOR_AMD64)
#error "Wrong architecture"
#endif

#include <array>
#include <iosfwd>

namespace cruft::cpu {
    /// Queries x86 CPU features using the CPUID instruction.
    struct x86 {
        x86 ();

        struct {
            int logical;

            // currently unused because we lack APIC enumeration code
            // required for physical:logical mappings.
            int physical = 0;

            bool hyper_threading;
        } cores;

        struct {
            bool sse;
            bool sse2;
            bool sse3;
            bool ssse3;
            bool sse41;
            bool sse42;
            bool avx;
        } simd;

        std::array<char,12> vendor_name;
        std::array<char,48> product_name;
    };

    std::ostream&
    operator<< (std::ostream&, const cruft::cpu::x86&);
};
