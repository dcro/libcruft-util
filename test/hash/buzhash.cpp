#include "tap.hpp"
#include "hash/buzhash.hpp"
#include "std.hpp"


int main ()
{
    cruft::TAP::logger tap;

    // Use buzhash to find the string 'needle' inside a larger string.
    static constexpr u08 needle[] = "needle";
    static constexpr std::size_t WIDTH = std::size (needle) - 1;

    // Compute the hash of the needle
    auto const key = cruft::hash::buzhash<u16> (WIDTH, needle).digest ();

    // Find the point at which the hash object equals the key's digest.
    static constexpr u08 haystack[] = "there is a needle here somewhere";
    cruft::hash::buzhash<u16> h (WIDTH, haystack);
    auto pos = std::find_if (
        std::begin (haystack) + WIDTH,
        std::end   (haystack),
        [&h, key] (auto const &i) { return h (&i) == key; }
    );

    tap.expect_eq (
        key,
        h.digest (),
        "needle/haystack digests match"
    );

    tap.expect (
        pos == haystack + strlen ("there is a needle") - 1,
        "buzhash finds the haystack"
    );

    return tap.status ();
}