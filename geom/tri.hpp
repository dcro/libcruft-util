/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016-2018 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_GEOM_TRI_HPP
#define CRUFT_GEOM_TRI_HPP

#include "../point.hpp"
#include "sample/fwd.hpp"

#include <cstddef>
#include <random>

namespace cruft::geom {
    ///////////////////////////////////////////////////////////////////////////
    template <size_t S, typename T>
    struct tri {
        tri () = default;
        tri (point<S,T> _a, point<S,T> _b, point<S,T> _c):
            a (_a), b (_b), c (_c)
        { ; }

        cruft::point<S,T> a, b, c;
    };

    template <size_t S, typename T>
    tri (point<S,T>, point<S,T>, point<S,T>) -> tri<S,T>;

    using tri3f = tri<3,float>;


    namespace sample {
        template <size_t S, typename T>
        class surface<tri<S,T>> {
        public:
            using shape_type = tri<S,T>;

            explicit surface (tri<S,T> _target):
                base (_target.a),
                v0 (_target.b - _target.a),
                v1 (_target.c - _target.a)
            { ; }

            template <typename GeneratorT>
            cruft::point<S,T>
            operator() (GeneratorT &&gen) const noexcept
            {
                std::uniform_real_distribution<float> dist (0, 1);
                return base + dist (gen) * v0 + dist (gen) * v1;
            }

        private:
            cruft::point<S,T> base;
            cruft::vector<S,T> v0;
            cruft::vector<S,T> v1;
        };
    };


    ///////////////////////////////////////////////////////////////////////////
    // n-dimensional triangle area
    template <std::size_t DimensionV, typename ValueT>
    ValueT
    area (tri<DimensionV,ValueT> obj)
    {
        // heron's formula
        const auto ab = cruft::distance (obj.a, obj.b);
        const auto bc = cruft::distance (obj.b, obj.c);
        const auto ca = cruft::distance (obj.c, obj.a);

        const auto s = (ab + bc + ca) / 2;

        return std::sqrt (s * (s - ab) * (s - bc) * (s - ca));
    }


    //-------------------------------------------------------------------------
    // 2-dimension triangle area
    template <typename T>
    T
    area (tri<2,T> obj)
    {
        //                | x1 y1 1 |
        // area = 0.5 det | x2 y2 1 |
        //                | x3 y3 1 |

        return std::abs (
            -obj.b.x * obj.a.y
            +obj.c.x * obj.a.y
            +obj.a.x * obj.b.y
            -obj.c.x * obj.b.y
            -obj.a.x * obj.c.y
            +obj.b.x * obj.c.y
        ) / 2;
    }


    //-------------------------------------------------------------------------
    // 3-dimension triangle area
    template <typename T>
    T
    area (tri<3,T> obj)
    {
        const auto ab = obj.a - obj.b;
        const auto ac = obj.a - obj.c;

        return norm (cross (ab, ac)) / 2;
    }


    //-------------------------------------------------------------------------
    // convenience forwarder
    template <size_t S, typename T>
    T
    area (point<S,T> a, point<S,T> b, point<S,T> c)
    {
        return area (tri (a, b, c));
    }
};

#endif
