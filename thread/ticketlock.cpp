/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018-2019 Danny Robson <danny@nerdcruft.net>
 */

#include "ticketlock.hpp"
#include "primitive.hpp"

using cruft::thread::ticketlock;


///////////////////////////////////////////////////////////////////////////////
ticketlock::ticketlock ():
    current (0),
    next (0)
{ }


///////////////////////////////////////////////////////////////////////////////
void
ticketlock::lock (void)
{
    auto self = next++;

    while (current != self)
        pause ();
}


//-----------------------------------------------------------------------------
void
ticketlock::unlock (void)
{
    ++current;
}
