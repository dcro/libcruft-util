/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015-2018 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include <cstddef>

namespace cruft {
    template <size_t S, typename T> struct region;
}

namespace cruft::geom {
    template <size_t S, typename T> struct aabb;
    template <size_t S, typename T> struct cylinder;
    template <size_t S, typename T> struct ellipse;
    template <size_t S, typename T> struct plane;
    template <size_t S, typename T> struct ray;
    template <size_t S, typename T> struct rect;
    template <size_t S, typename T> struct segment;
    template <size_t S, typename T> struct sphere;
    template <size_t S, typename T> struct tri;

    template <typename T> struct frustum;
}
