/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2017 Danny Robson <danny@nerdcruft.net>
 */

#include "util.hpp"

#include "fd.hpp"

#include "except.hpp"


///////////////////////////////////////////////////////////////////////////////
struct ::stat
cruft::posix::stat (char const *path)
{
    struct stat buffer;
    error::try_call (::stat, path, &buffer);
    return buffer;
}


//-----------------------------------------------------------------------------
struct ::stat
cruft::posix::stat (std::filesystem::path const &path)
{
    return stat (path.u8string ().c_str ());
}


///////////////////////////////////////////////////////////////////////////////
struct ::stat
cruft::posix::fstat (int src)
{
    struct stat buffer;
    error::try_call (::fstat, src, &buffer);
    return buffer;
}


//-----------------------------------------------------------------------------
struct ::stat
cruft::posix::fstat (fd const &src)
{
    return fstat (src.native ());
}


//-----------------------------------------------------------------------------
struct ::stat
cruft::posix::stat (int src)
{
    return fstat (src);
}


//-----------------------------------------------------------------------------
struct ::stat
cruft::posix::stat (fd const &src)
{
    return fstat (src);
}
