#include "json/tree.hpp"
#include "json/pointer.hpp"

#include <iostream>


enum {
    ARG_SELF,
    ARG_QUERY,
    ARG_INPUT,

    NUM_ARGS,
};


int
main (int argc, char **argv)
{
    if (argc != NUM_ARGS) {
        std::cerr << argv[ARG_SELF] << " <query> <input>\n";
        return EXIT_FAILURE;
    }

    auto const root = json::tree::parse (argv[ARG_INPUT]);
    std::string const query = argv[ARG_QUERY];

    try {
        std::cout << cruft::json::pointer::resolve (query, *root) << '\n';
    } catch (std::exception const &x) {
        std::cerr << "error" << x.what () << '\n';
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}