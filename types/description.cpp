/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2018 Danny Robson <danny@nerdcruft.net>
 */

#include "description.hpp"

#include "../debug/assert.hpp"


///////////////////////////////////////////////////////////////////////////////
std::ostream&
cruft::types::operator<< (std::ostream &os, cruft::types::category val)
{
    switch (val) {
    case category::NONE:        return os << "NONE";
    case category::INTEGER:     return os << "INTEGER";
    case category::REAL:        return os << "REAL";
    case category::BOOL:        return os << "BOOL";
    case category::ENUM:        return os << "ENUM";
    }

    unreachable ();
}


///////////////////////////////////////////////////////////////////////////////
std::ostream&
cruft::types::operator<< (std::ostream &os, cruft::types::description val)
{
    return os << "{ category: "   << val.category
              << ", signedness: " << val.signedness
              << ", width: "      << val.width
              << ", arity: "      << val.arity
              << ", alignment: "  << val.alignment
              << ", index: "      << val.index
              << " }";
}


///////////////////////////////////////////////////////////////////////////////
#include "../debug/validate.hpp"
#include <iostream>

//-----------------------------------------------------------------------------
template <>
struct cruft::debug::validator<cruft::types::description> {
    static bool is_valid (cruft::types::description const&);
};


//-----------------------------------------------------------------------------
bool
cruft::debug::validator<cruft::types::description>::is_valid (
    cruft::types::description const &val
) {
    switch (val.category) {
    // Ensure types that look fundamental might have a chance of being
    // represented in hardware.
    case cruft::types::category::INTEGER:
    case cruft::types::category::REAL:
    case cruft::types::category::BOOL:
    case cruft::types::category::ENUM:
        // Most numbers are only realistic if they're powers of two; it's not
        // a requirement, it's just stupid suspicious.
        //
        // Though arity of 3 is common for 3D points so we make an exception
        // here.
        WARN_RETURN (!is_pow2 (val.width), false);
        WARN_RETURN (!is_pow2 (val.arity) && val.arity != 3, false);
        WARN_RETURN (!is_pow2 (val.alignment), false);

        // Restrict bounds of native type constants to values that correspond
        // to the hardware we tend to work on.
        //
        // Again, this is mostly for plausibilty rather than anything else.
        // Feel free to make this less restrictive.
        WARN_RETURN (val.width == 0 || val.width > 8, false);
        WARN_RETURN (val.arity == 0 || val.arity > 16, false);
        WARN_RETURN (val.alignment == 0 || val.alignment > 64, false);

    // All bets are off. Have fun.
    case cruft::types::category::NONE:
        return true;
    }

    WARN ("Unknown value category");
    return false;
}
