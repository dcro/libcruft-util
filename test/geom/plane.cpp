#include "geom/plane.hpp"
#include "point.hpp"
#include "tap.hpp"


void
test_distances (cruft::TAP::logger &tap)
{
    static struct {
        cruft::point3f a, b, c;
        cruft::point3f query;
        float distance;
        const char *message;
    } const TESTS[] = {
        { { 0, 0, 0 }, { 0, 1, 0 }, { 1, 0, 0 }, { 0, 0, 0 },  0, "colinear with plane about -z" },
        { { 0, 0, 0 }, { 0, 1, 0 }, { 1, 0, 0 }, { 0, 0,-1 },  1, "in front of plane about -z" },
        { { 0, 0, 0 }, { 0, 1, 0 }, { 1, 0, 0 }, { 0, 0, 1 }, -1, "behind plane about -z" },
    };

    for (auto const &t: TESTS) {
        auto const p = cruft::geom::make_plane (t.a, t.b, t.c);
        const auto d = distance (p, t.query);
        tap.expect_eq (d, t.distance, "%!", t.message);
    }
};


int
main (int, char**)
{
    cruft::TAP::logger tap;

    test_distances (tap);

    return tap.status ();
}