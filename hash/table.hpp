/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../std.hpp"

///////////////////////////////////////////////////////////////////////////////
namespace cruft::hash {
    /// Provide a trivial static mapping from a ValueT to a ResultT.
    ///
    /// We guarantee that all outputs are:
    ///   * unique
    ///   * are unbiased at every bit offset
    ///
    /// Specialisations are provided for u08, u16, u32, and u64 outputs with
    /// u08 inputs. If further flexibility is required we can move the
    /// implementation into the header.
    ///
    /// \tparam ResultT  The output of the 'hash' function.
    /// \tparam WordT    The input to the 'hash' function.
    template <typename ResultT, typename WordT = u08>
    class table {
    public:
        ResultT operator() (WordT const) const noexcept;
    };
}
