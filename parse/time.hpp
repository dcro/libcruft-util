/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2019 Danny Robson <danny@nerdcruft.net>
 */

#pragma once

#include "../expected.hpp"
#include "../view.hpp"

#include <chrono>
#include <system_error>

namespace cruft::parse::duration {
    /// Parse a number that represents a duration. eg, "1s", "5 minutes".
    ///
    /// The data view is updated to indicate the unused data.
    ///
    /// When there is no suffix it is assumed the quantity is in second.
    ///
    /// Note: The quantities are as defined by the standard std::chrono
    /// durations. This means that "1 month" will equal just under 30.5 days.
    /// Thus the utility is not universally useful for offsetting.
    expected<std::chrono::nanoseconds, std::errc>
    consume (cruft::view<char const*> &);


    /// Parse a number that represent a duration (as with `consume`).
    ///
    /// The operation will fail if the entire input is not consumed.
    expected<std::chrono::nanoseconds, std::errc>
    from (cruft::view<char const*> const&);
}
