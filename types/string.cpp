/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2011 Danny Robson <danny@nerdcruft.net>
 */

#include "string.hpp"

#define do_type_to_string(T)                                                    \
template <> std::string type_to_string <T>       (void) { return #T; }          \
template <> std::string type_to_string <const T> (void) { return "const " #T; }

do_type_to_string (float)
do_type_to_string (double)

do_type_to_string (  int8_t)
do_type_to_string ( int16_t)
do_type_to_string ( int32_t)
do_type_to_string ( int64_t)

do_type_to_string ( uint8_t)
do_type_to_string (uint16_t)
do_type_to_string (uint32_t)
do_type_to_string (uint64_t)

