/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_HASH_MURMUR_MURMUR1_HPP
#define __UTIL_HASH_MURMUR_MURMUR1_HPP

#include "../../view.hpp"

#include <cstdint>
#include <cstddef>

// Austin Appleby's MumurHash1
namespace cruft::hash {
    class murmur1 {
    public:
        using digest_t = uint32_t;
        using seed_t = uint32_t;

        murmur1 (seed_t _seed):
            m_seed (_seed)
        { ; }

        static constexpr uint32_t mix (uint32_t h, uint32_t k)
        {
            constexpr uint32_t m = 0xc6a4a793;

            h += k;
            h *= m;
            h ^= h >> 16;

            return h;
        }

        digest_t operator() (cruft::view<const uint8_t*> data) const noexcept;

    private:
        seed_t m_seed;
    };
}

#endif
