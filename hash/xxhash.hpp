/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016-2018 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_UTIL_HASH_XXHASH_HPP
#define CRUFT_UTIL_HASH_XXHASH_HPP

#include "../view.hpp"

#include <cstdint>
#include <type_traits>

namespace cruft::hash {
    template <typename WordT>
    class xxhash {
    public:
        static_assert (std::is_same<WordT,std::uint32_t>::value || std::is_same<WordT,std::uint64_t>::value);
        using digest_t = WordT;
        using word_t = WordT;
        static constexpr int block_bytes = 4 * sizeof (word_t);

        static constexpr word_t DEFAULT_SEED = 0;

        xxhash (word_t seed = DEFAULT_SEED);

        digest_t operator() (const cruft::view<const uint8_t*> data);

    private:
        word_t m_seed;
    };

    using xxhash32 = xxhash<uint32_t>;
    using xxhash64 = xxhash<uint64_t>;
}

#endif
