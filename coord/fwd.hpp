/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2016-2017 Danny Robson <danny@nerdcruft.net>
 */

#ifndef CRUFT_UTIL_COORD_FWD_HPP
#define CRUFT_UTIL_COORD_FWD_HPP

#include <cstddef>

namespace cruft {
    namespace coord {
        template <size_t S,typename T,typename ParentT> struct store;
        template <size_t,typename,typename> struct init;
        template <size_t,typename,typename> struct base;
    }

    template <size_t,typename> struct srgba;
    template <size_t,typename> struct hsva;
    template <size_t,typename> struct extent;
    template <size_t,typename> struct point;
    template <size_t,typename> struct vector;
}

#endif
