/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright 2015 Danny Robson <danny@nerdcruft.net>
 */

#ifndef __UTIL_HASH_FASTHASH_HPP
#define __UTIL_HASH_FASTHASH_HPP

#include "../view.hpp"

#include <cstddef>
#include <cstdint>


// Zilong Tan's FastHash, via George Marsaglia's "Xorshift RNGs"
namespace cruft::hash {
    template <typename ValueT>
    struct fasthash {
        using digest_t = ValueT;

        static constexpr
        uint64_t mix (uint64_t v)
        {
            v ^= v >> 23;
            v *= 0x2127599bf4325c37;
            v ^= v >> 47;

            return v;
        }


        digest_t operator() (uint64_t seed, cruft::view<const uint8_t*>) const;
    };
}

#endif
